package id.ac.tazkia.smilemahasiswa.dto.ploting;

public interface DataPlotingDto {
    String getIdKelas();
    String getKelas();
    String getMkkur();
    String getKode();
    String getMatkul();
    String getCourse();
    Integer getSks();
    String getValidasi();
    String getIdDos();
    String getDosen();
    Integer getTotsks();
    String getJadwal();
    String getHari();
    String getMulai();
}
