package id.ac.tazkia.smilemahasiswa.dao;

import id.ac.tazkia.smilemahasiswa.dto.assesment.BobotDto;
import id.ac.tazkia.smilemahasiswa.dto.assesment.BobotScoreDto;
import id.ac.tazkia.smilemahasiswa.entity.*;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface BobotTugasDao extends PagingAndSortingRepository<BobotTugas, String> {
    List<BobotTugas> findByJadwalAndStatus(Jadwal jadwal, StatusRecord aktif);
    List<BobotTugas> findByJadwalAndStatusOrderByPertemuanAsc(Jadwal jadwal, StatusRecord aktif);

    BobotTugas findByJadwalAndJadwalTugasAndStatus(Jadwal jadwal, JadwalTugas pertemuan, StatusRecord status);

    @Query("select new id.ac.tazkia.smilemahasiswa.dto.assesment.BobotScoreDto(bt.id,bt.bobot) from BobotTugas bt where bt.jadwal.id= :jadwal and bt.status = :status")
    List<BobotScoreDto> bobotTugas(@Param("jadwal")String jadwal, @Param("status")StatusRecord statusRecord);

    @Query("select new id.ac.tazkia.smilemahasiswa.dto.assesment.BobotDto(bt.id,bt.pertemuan,bt.bobot) from BobotTugas bt where bt.jadwal.id= :jadwal and bt.kategoriTugas = :kategori and bt.status = :status order by bt.pertemuan asc")
    List<BobotDto> Tugas(@Param("jadwal")String jadwal, @Param("kategori")KategoriTugas kategoriTugas, @Param("status")StatusRecord statusRecord);

    @Query(value = "select sum(bobot) as bobot from jadwal_bobot_tugas where id_jadwal=?1 and status='AKTIF'", nativeQuery = true)
    BigDecimal totalBobotTugas(String idJadwal);

}
