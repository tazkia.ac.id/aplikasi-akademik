package id.ac.tazkia.smilemahasiswa.dto.human;

public interface DataJabatan {
    String getId();
    String getNama();
}
