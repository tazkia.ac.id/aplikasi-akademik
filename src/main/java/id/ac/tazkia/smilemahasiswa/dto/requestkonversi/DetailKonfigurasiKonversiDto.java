package id.ac.tazkia.smilemahasiswa.dto.requestkonversi;

public interface DetailKonfigurasiKonversiDto {
    String getNama();
    String getKategori();
    String getLevel1();
    String getLevel2();
    String getLevel3();
    String getLevel4();
    String getLevel5();
    Integer getNumber();
}
