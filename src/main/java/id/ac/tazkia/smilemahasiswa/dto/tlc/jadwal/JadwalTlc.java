package id.ac.tazkia.smilemahasiswa.dto.tlc.jadwal;

public interface JadwalTlc {
     String getJadwal();
     String getMatakuliah();
     String getDosen();
     String getHari();
     String getKelas();
     String getRuangan();
     String getMulai();
     String getSelesai();
}
