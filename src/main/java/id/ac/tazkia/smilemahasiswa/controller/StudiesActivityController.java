package id.ac.tazkia.smilemahasiswa.controller;

import com.github.mustachejava.MustacheFactory;
import id.ac.tazkia.smilemahasiswa.dao.*;
import id.ac.tazkia.smilemahasiswa.dto.BaseResponse;
import id.ac.tazkia.smilemahasiswa.dto.BaseResponseDto;
import id.ac.tazkia.smilemahasiswa.dto.KrsNilaiTugasDto;
import id.ac.tazkia.smilemahasiswa.dto.assesment.*;
import id.ac.tazkia.smilemahasiswa.dto.attendance.*;
import id.ac.tazkia.smilemahasiswa.dto.krs.GeneratedKrsDto;
import id.ac.tazkia.smilemahasiswa.dto.krs.MatkulMengulangDto;
import id.ac.tazkia.smilemahasiswa.dto.krs.SpDto;
import id.ac.tazkia.smilemahasiswa.dto.report.DataKhsDto;
import id.ac.tazkia.smilemahasiswa.dto.room.KelasMahasiswaDto;
import id.ac.tazkia.smilemahasiswa.dto.transkript.DataTranskript;
import id.ac.tazkia.smilemahasiswa.dto.transkript.TranskriptDto;
import id.ac.tazkia.smilemahasiswa.dto.user.IpkDto;
import id.ac.tazkia.smilemahasiswa.entity.*;
import id.ac.tazkia.smilemahasiswa.entity.SesiKuliahFoto;
import id.ac.tazkia.smilemahasiswa.service.*;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.PropertyTemplate;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.ValidationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.chrono.HijrahDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@Slf4j
public class StudiesActivityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudiesActivityController.class);

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private SesiKuliahDao sesiKuliahDao;



    @Autowired
    private TahunAkademikDao tahunAkademikDao;

    @Autowired
    private HariDao hariDao;

    @Autowired
    private JenjangDao jenjangDao;

    @Autowired
    private JadwalDao jadwalDao;

    @Autowired
    private MustacheFactory mustacheFactory;

    @Autowired
    private SesiKuliahFotoDao sesiKuliahFotoDao;

    @Autowired
    private JadwalDosenDao jadwalDosenDao;

    @Autowired
    private DetailPertemuanDao detailPertemuanDao;

    @Autowired
    private PresensiService presensiService;

    @Autowired
    private PresensiMahasiswaDao presensiMahasiswaDao;

    @Autowired
    private PresensiDosenDao presensiDosenDao;

    @Autowired
    private MahasiswaDao mahasiswaDao;

    @Autowired
    private KrsDao krsDao;

    @Autowired
    private NilaiTugasDao nilaiTugasDao;

    @Autowired
    private KrsDetailDao krsDetailDao;

    @Autowired
    private KelasMahasiswaDao kelasMahasiswaDao;

    @Autowired
    private BobotTugasDao bobotTugasDao;

    @Autowired
    private CurrentUserService currentUserService;

    @Autowired
    private KaryawanDao karyawanDao;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private SoalDao soalDao;

    @Autowired
    private RpsDao rpsDao;

    @Autowired
    private ProgramDao programDao;

    @Autowired
    private JadwalTugasDao jadwalTugasDao;

    @Autowired
    private ScoreService scoreService;

    @Autowired
    private PraKrsSpDao praKrsSpDao;

    @Autowired
    private MatakuliahKurikulumDao matakuliahKurikulumDao;

    @Autowired
    private KelasDao kelasDao;

    @Autowired
    private TahunProdiDao tahunProdiDao;

    @Autowired
    private EdomMahasiswaDao edomMahasiswaDao;

    @Autowired
    private EdomQuestionDao edomQuestionDao;

    @Autowired
    private MateriMengajarDao materiMengajarDao;

//    @Autowired
//    private JadwalTugasDao jadwalTugasDao;

    @Autowired
    private JadwalTugasJawabanDao tugasJawabanDao;

    @Autowired
    private MailService mailService;

    @Autowired
    private TranscriptService transcriptService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private KrsApprovalDao krsApprovalDao;

    @Autowired
    private PendaftaranMbkmDao pendaftaranMbkmDao;

    @Autowired
    private TugasMateriService tugasMateriService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private KrsService krsService;

    @Autowired
    @Value("${upload.fotokuliah}")
    private String uploadFotoKuliah;


    @Value("classpath:sample/soal.doc")
    private Resource contohSoal;

    @Value("classpath:/sample/khs.xlsx")
    private Resource contohExcelKhs;

    @Value("classpath:/sample/transkript.xlsx")
    private Resource contohExcelTranskript;

    @Value("classpath:/sample/transkriptIndo.xlsx")
    private Resource contohExcelTranskriptIndo;

    @Value("classpath:/sample/Transkript-baru.xlsx")
    private Resource getContohExcelTranskript;

    @Value("classpath:sample/uas.doc")
    private Resource contohSoalUas;

    @Value("classpath:static/images/tazkia.png")
    private Resource logoTazkia;

    @Value("classpath:tazkia1.png")
    private Resource logoTazkia1;

    @Value("classpath:tazkia2.png")
    private Resource logoTazkia2;

    @Value("${upload.soal}")
    private String uploadFolder;

        @Value("${upload.materi.dosen}")
        private String uploadMateri;

        @Value("${upload.tugas.mhs}")
        private String uploadJawaban;

        @Value("${upload.rps}")
        private String uploadRps;

    @Value("classpath:sample/contohRps.xlsx")
    private Resource exampleRps;

    // Attribute

    @ModelAttribute("dosen")
    public Iterable<Dosen> dosen() {
        return dosenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS));
    }

    @ModelAttribute("prodi")
    public Iterable<Prodi> prodi() {
        return prodiDao.findByStatus(StatusRecord.AKTIF);
    }

    @ModelAttribute("tahunAkademik")
    public Iterable<TahunAkademik> tahunAkademik() {
        return tahunAkademikDao.findByStatusNotInOrderByTahunDesc(Arrays.asList(StatusRecord.HAPUS));
    }

    @ModelAttribute("hari")
    public Iterable<Hari> hari() {
        return hariDao.findAll();
    }

    @ModelAttribute("angkatan")
    public Iterable<Mahasiswa> angkatan() {
        return mahasiswaDao.cariAngkatan();
    }

    // API
    @GetMapping("/api/nilai")
    @ResponseBody
    public KrsDetail findByJadwal(@RequestParam(required = false) KrsDetail krsDetail, Model model) {
        model.addAttribute("otomatisNilai", krsDetail);
        return krsDetail;
    }

    @GetMapping("/api/mahasiswa")
    @ResponseBody
    public Mahasiswa findByNim(@RequestParam(required = false) String nim, Model model) {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        model.addAttribute("mahasiswa", mahasiswa);
        return mahasiswa;
    }

    // Attendance

    @GetMapping("/studiesActivity/attendance/listdosen")
    public void listLectureAttendance(Model model, Authentication authentication,
                                      @RequestParam(required = false) TahunAkademik tahunAkademik) {
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        Dosen dosen = dosenDao.findByKaryawan(karyawan);

        if (tahunAkademik != null) {
            model.addAttribute("selectedTahun", tahunAkademik);
            Iterable<JadwalDosen> jadwal = jadwalDosenDao
                    .findByJadwalStatusNotInAndJadwalTahunAkademikAndDosenAndJadwalHariNotNullAndJadwalKelasNotNullOrderByJadwalHariAscJadwalJamMulaiAsc(
                            Arrays.asList(StatusRecord.HAPUS), tahunAkademik, dosen);
            model.addAttribute("jadwal", jadwal);

        } else {
            TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
            Iterable<JadwalDosen> jadwal = jadwalDosenDao
                    .findByJadwalStatusNotInAndJadwalTahunAkademikAndDosenAndJadwalHariNotNullAndJadwalKelasNotNullOrderByJadwalHariAscJadwalJamMulaiAsc(
                            Arrays.asList(StatusRecord.HAPUS), tahun, dosen);
            model.addAttribute("jadwal", jadwal);
        }

    }

    @GetMapping("/studiesActivity/attendance/list")
    public void listAttendance(Model model, @RequestParam(required = false) Prodi prodi,
                               @RequestParam(required = false) TahunAkademik tahunAkademik,
                               @RequestParam(required = false) Hari hari) {
        model.addAttribute("selectedTahun", tahunAkademik);
        model.addAttribute("selectedHari", hari);
        model.addAttribute("selectedProdi", prodi);

        if (prodi != null && tahunAkademik != null && hari != null) {
            model.addAttribute("ploting", jadwalDao.ploting(prodi, tahunAkademik));
            model.addAttribute("jadwal",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik, hari));
        }

        if (prodi != null && tahunAkademik != null && hari == null) {
            model.addAttribute("ploting", jadwalDao.ploting(prodi, tahunAkademik));
            model.addAttribute("minggu",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("0").get()));
            model.addAttribute("senin",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("1").get()));
            model.addAttribute("selasa",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("2").get()));
            model.addAttribute("rabu",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("3").get()));
            model.addAttribute("kamis",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("4").get()));
            model.addAttribute("jumat",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("5").get()));
            model.addAttribute("sabtu",
                    jadwalDao.schedule(prodi, StatusRecord.AKTIF, tahunAkademik,
                            hariDao.findById("6").get()));
        }
    }

    @GetMapping("/studiesActivity/attendance/detail")
    public void detailAttendance(Model model, @RequestParam Jadwal jadwal) {
        List<SesiKuliah> cekRps = sesiKuliahDao.findByJadwalAndPresensiDosenStatusNotInAndRpsIsNull(jadwal, Arrays.asList(StatusRecord.HAPUS));
        if (!cekRps.isEmpty()) {
            model.addAttribute("gagal", "Unggah Rencana Pembelajaran terlebih dahulu!");
        }

        List<SesiKuliah> cekSesi = sesiKuliahDao.findByJadwalAndPresensiDosenStatusNotIn(jadwal, Arrays.asList(StatusRecord.HAPUS));
        if (!cekSesi.isEmpty()) {
            model.addAttribute("valid", "sudah tergenerate");
        }

        model.addAttribute("detail", presensiDosenDao.listPresensiDosen(jadwal));
        model.addAttribute("validasiMateri", sesiKuliahDao.countByJadwalAndPertemuanNotAndLinkVideoIsNull(jadwal, "Belum Mengajar"));
        model.addAttribute("dosenUtama", jadwal.getDosen());
        model.addAttribute("teamTeaching", jadwalDosenDao.findByJadwal(jadwal));
        model.addAttribute("jadwalTugas", jadwalTugasDao.findByStatusAndSesiKuliahJadwalOrderBySesiKuliahPertemuanKe(StatusRecord.AKTIF,jadwal));
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("sesiKuliahFoto", sesiKuliahFotoDao.findByStatusAndJadwalOrderBySesiKuliahPertemuan(StatusRecord.AKTIF,jadwal));
        model.addAttribute("date", LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

    }

    @PostMapping("/studiesActivity/attendance/edit-sesi")
    public String editSesi(PresensiDto presensiDto) {
        PresensiDosen presensiDosen = presensiDosenDao.findById(presensiDto.getId()).get();
         LocalDateTime masuk = LocalDateTime.of(presensiDto.getTanggal(), LocalTime.parse(presensiDto.getWaktuMulai()));
         LocalDateTime keluar = LocalDateTime.of(presensiDto.getTanggal(), LocalTime.parse(presensiDto.getWaktuSelesai()));
         presensiDosen.setWaktuMasuk(masuk);
         presensiDosen.setWaktuSelesai(keluar);
         presensiDosen.setStatusPresensi(StatusPresensi.BELUM_MENGAJAR);
         presensiDosen.setTanggalInput(LocalDateTime.now().plusHours(7));
         presensiDosenDao.save(presensiDosen);

         SesiKuliah sesiKuliah = sesiKuliahDao.findByPresensiDosen(presensiDosen);
         sesiKuliah.setWaktuMulai(masuk);
         sesiKuliah.setWaktuSelesai(keluar);
         sesiKuliah.setIsEdit(Boolean.TRUE);
         sesiKuliahDao.save(sesiKuliah);
        return "redirect:detail?jadwal=" + presensiDosen.getJadwal().getId();
    }


    @GetMapping("/sample/rps")
    public void downloadContohFileInputRPS(HttpServletResponse response) throws IOException {

        response.setContentType("application/vns.ms-excel");
        response.setHeader("Content-Disposition", "attahment; filename=Example-RPS.xlsx");
        FileCopyUtils.copy(exampleRps.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();

    }

    @Transactional
    @PostMapping("/studiesActivity/attendance/rps/upload")
    public String prosesInputRpsFromExcel(MultipartFile file, RedirectAttributes attributes,
                                          @RequestParam Jadwal jadwal) {

        log.debug("Nama file : {}", file.getOriginalFilename());
        log.debug("Ukuran file : {} bytes", file.getSize());

        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheet1 = workbook.getSheetAt(0);

            int row = 5;

            for (int n = 0; n <= 15; n++) {
                Row baris = sheet1.getRow(row + n);

                Cell num = baris.getCell(0);
                num.setCellType(CellType.STRING);
                Cell data = baris.getCell(1);
                data.setCellType(CellType.STRING);

                System.out.println("data no. : " + num);
                System.out.println("data isi : " + data);

                Optional<SesiKuliah> optSesi = sesiKuliahDao.findByJadwalAndPertemuanKe(jadwal,
                        Integer.parseInt(num.getStringCellValue()));
                if (optSesi.isEmpty()) {
                    log.debug("Sesi kuliah tidak di temukan {}", jadwal.getId());
                }
                SesiKuliah sesiKuliah = optSesi.get();
                sesiKuliah.setRps(data.getStringCellValue());
                sesiKuliahDao.save(sesiKuliah);

            }

            List<SesiKuliah> cekRps = sesiKuliahDao.findByJadwalAndPresensiDosenStatusAndRps(jadwal,
                    StatusRecord.AKTIF, "");
            log.info("RPS : {}", cekRps);
            if (!cekRps.isEmpty()) {
                List<String> kosong = new ArrayList<>();
                for (SesiKuliah list : cekRps) {
                    log.info("sesi kuliah : {}", list.toString());
                    kosong.add(list.getPertemuanKe().toString());
                }
                log.info("Kosong : {}", kosong);
                String set = kosong.toString().replace("[", "").replace("]", "");
                attributes.addFlashAttribute("gagal", "Keterangan pada pertemuan ke " + set
                        + " kosong, harap isi terlebih dahulu!");
                sesiKuliahDao.updateRps(jadwal.getId());
                return "redirect:/studiesActivity/attendance/detail?jadwal=" + jadwal.getId();
            }

        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }

        return "redirect:/studiesActivity/attendance/detail?jadwal=" + jadwal.getId();

    }

    @GetMapping("/detail-sesikuliah/{id}")
    @ResponseBody
    public List<PresensiDosenDto> getSesiKuliah(@PathVariable String id) {
        Jadwal jadwal = jadwalDao.findById(id).get();

        return presensiDosenDao.listPresensiDosen(jadwal);
    }

    @GetMapping("/studiesActivity/attendance/download")
    public void downloadAttendance(@RequestParam(required = false) Jadwal jadwal, HttpServletResponse response)
            throws IOException {

        String[] column = {"No", "Nim", "Nama", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
                "13",
                "14", "15", "16"};

        List<Object[]> listAbsen = presensiMahasiswaDao.bkdAttendance(jadwal);

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Absensi");

        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 20);
        titleFont.setColor(IndexedColors.BLACK.getIndex());
        titleFont.setFontName("Cambria");

        Font detailFont = workbook.createFont();
        detailFont.setFontHeightInPoints((short) 12);
        detailFont.setFontName("Arial");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        headerFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        CellStyle dataCellStyle = workbook.createCellStyle();
        dataCellStyle.setFont(dataFont);

        CellStyle detailCellStyle = workbook.createCellStyle();
        detailCellStyle.setFont(detailFont);

        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setFont(titleFont);
        titleCellStyle.setAlignment(HorizontalAlignment.LEFT);
        titleCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        sheet.addMergedRegion(CellRangeAddress.valueOf("A1:C2"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A4:C4"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A5:C5"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A6:C6"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A7:C7"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:C8"));

        Row titleRow = sheet.createRow(0);
        titleRow.createCell(0).setCellValue("DAFTAR HADIR ");
        titleRow.getCell(0).setCellStyle(titleCellStyle);

        Row matkul = sheet.createRow(3);
        matkul.createCell(0).setCellValue("Nama Mata Kuliah ");
        matkul.createCell(3).setCellValue(": ");
        matkul.createCell(4).setCellValue(jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah());
        matkul.getCell(0).setCellStyle(detailCellStyle);
        matkul.getCell(3).setCellStyle(detailCellStyle);
        matkul.getCell(4).setCellStyle(detailCellStyle);

        Row kelas = sheet.createRow(4);
        kelas.createCell(0).setCellValue("Kelas ");
        kelas.createCell(3).setCellValue(": ");
        kelas.createCell(4).setCellValue(jadwal.getKelas().getNamaKelas());
        kelas.getCell(0).setCellStyle(detailCellStyle);
        kelas.getCell(3).setCellStyle(detailCellStyle);
        kelas.getCell(4).setCellStyle(detailCellStyle);

        Row dosen = sheet.createRow(5);
        dosen.createCell(0).setCellValue("Dosen ");
        dosen.createCell(3).setCellValue(": ");
        dosen.createCell(4).setCellValue(jadwal.getDosen().getKaryawan().getNamaKaryawan());
        dosen.getCell(0).setCellStyle(detailCellStyle);
        dosen.getCell(3).setCellStyle(detailCellStyle);
        dosen.getCell(4).setCellStyle(detailCellStyle);

        Row ruangan = sheet.createRow(6);
        ruangan.createCell(0).setCellValue("Ruangan ");
        ruangan.createCell(3).setCellValue(": ");
        ruangan.createCell(4).setCellValue(jadwal.getRuangan().getNamaRuangan());
        ruangan.getCell(0).setCellStyle(detailCellStyle);
        ruangan.getCell(3).setCellStyle(detailCellStyle);
        ruangan.getCell(4).setCellStyle(detailCellStyle);

        Row jam = sheet.createRow(7);
        jam.createCell(0).setCellValue("Jam ");
        jam.createCell(3).setCellValue(": ");
        jam.createCell(4).setCellValue(
                jadwal.getJamMulai().toString() + " - " + jadwal.getJamSelesai().toString());
        jam.getCell(0).setCellStyle(detailCellStyle);
        jam.getCell(3).setCellStyle(detailCellStyle);
        jam.getCell(4).setCellStyle(detailCellStyle);

        Row headerRow = sheet.createRow(9);

        for (int i = 0; i < column.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(column[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 10;
        int baris = 1;

        for (Object[] list : listAbsen) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(list[2].toString());
            row.createCell(2).setCellValue(list[3].toString());
            row.createCell(3).setCellValue(list[4].toString());
            row.createCell(4).setCellValue(list[5].toString());
            row.createCell(5).setCellValue(list[6].toString());
            row.createCell(6).setCellValue(list[7].toString());
            row.createCell(7).setCellValue(list[8].toString());
            row.createCell(8).setCellValue(list[9].toString());
            row.createCell(9).setCellValue(list[10].toString());
            row.createCell(10).setCellValue(list[11].toString());
            row.createCell(11).setCellValue(list[12].toString());
            row.createCell(12).setCellValue(list[13].toString());
            row.createCell(13).setCellValue(list[14].toString());
            row.createCell(14).setCellValue(list[15].toString());
            row.createCell(15).setCellValue(list[16].toString());
            row.createCell(16).setCellValue(list[17].toString());
            row.createCell(17).setCellValue(list[18].toString());
            row.createCell(18).setCellValue(list[19].toString());
            row.getCell(0).setCellStyle(dataCellStyle);
            row.getCell(1).setCellStyle(dataCellStyle);
            row.getCell(2).setCellStyle(dataCellStyle);
            row.getCell(3).setCellStyle(dataCellStyle);
            row.getCell(4).setCellStyle(dataCellStyle);
            row.getCell(5).setCellStyle(dataCellStyle);
            row.getCell(6).setCellStyle(dataCellStyle);
            row.getCell(7).setCellStyle(dataCellStyle);
            row.getCell(8).setCellStyle(dataCellStyle);
            row.getCell(9).setCellStyle(dataCellStyle);
            row.getCell(10).setCellStyle(dataCellStyle);
            row.getCell(11).setCellStyle(dataCellStyle);
            row.getCell(12).setCellStyle(dataCellStyle);
            row.getCell(13).setCellStyle(dataCellStyle);
            row.getCell(14).setCellStyle(dataCellStyle);
            row.getCell(15).setCellStyle(dataCellStyle);
            row.getCell(16).setCellStyle(dataCellStyle);
            row.getCell(17).setCellStyle(dataCellStyle);
            row.getCell(18).setCellStyle(dataCellStyle);
        }

        for (int i = 0; i < column.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=Daftar Hadir Kelas.xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @PostMapping("/studiesActivity/attendance/detail")
    public String createPresensi(@ModelAttribute @Valid JadwalDto jadwalDto) throws Exception {

        LocalTime jamMulai1 = LocalTime.now().plusHours(7);
        DateTimeFormatter format2 = DateTimeFormatter.ofPattern("hh:mm");

        presensiService.inputPresensi(jadwalDto.getDosen(),
                jadwalDto.getJadwal(), jadwalDto.getBeritaAcara(),
                LocalDateTime.of(jadwalDto.getTanggal(), jamMulai1), jadwalDto.getPertemuan());

        return "redirect:detail2?jadwal=" + jadwalDto.getJadwal().getId();

    }

    @GetMapping("/studiesActivity/attendance/mahasiswa")
    public void attendance(@RequestParam(name = "id", value = "id") SesiKuliah sesiKuliah, Model model) {
        model.addAttribute("jadwal", sesiKuliah.getJadwal().getId());
        model.addAttribute("sesi", sesiKuliah.getId());
    }

    @GetMapping("/data-presensi-mahasiswa")
    @ResponseBody
    public List<PresensiMahasiswaDto> listPresensiMahasiswaBarcode(@RequestParam String id) {
        PresensiDosen presensiDosen = presensiDosenDao.findById(id).get();
        SesiKuliah sesiKuliah = sesiKuliahDao.findByPresensiDosen(presensiDosen);
        return presensiMahasiswaDao.listPresensiMahasiswaBarcode(sesiKuliah);
    }

    @PostMapping("/update-presensi-mahasiswa/{id}")
    @ResponseBody
    public BaseResponse listPresensiMahasiswa(@PathVariable String id, @RequestParam String status,@RequestParam String sesi) {
        Mahasiswa mahasiswa = mahasiswaDao.findById(id).get();
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(sesi).get();
        PresensiMahasiswa presensiMahasiswa = presensiMahasiswaDao.findByMahasiswaAndSesiKuliahAndStatus(mahasiswa,sesiKuliah,StatusRecord.AKTIF);
        if (presensiMahasiswa != null) {
            presensiMahasiswa.setStatusPresensi(StatusPresensi.valueOf(status));
            presensiMahasiswa.setCatatan("MANUAL");
            presensiMahasiswaDao.save(presensiMahasiswa);
            presensiService.updateNilaiPresensi(presensiMahasiswa.getKrsDetail());

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setId(presensiMahasiswa.getId());
            baseResponse.setNim(presensiMahasiswa.getMahasiswa().getNim());
            baseResponse.setNama(presensiMahasiswa.getMahasiswa().getNama());
            baseResponse.setMessage("Presensi " + mahasiswa.getNim() + " - " + mahasiswa.getNama() + " Telah Diperbarui");
            return baseResponse;
        }else {
            KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(mahasiswa,sesiKuliah.getJadwal(),StatusRecord.AKTIF);
            PresensiMahasiswa newPresensi = new PresensiMahasiswa();
            newPresensi.setStatusPresensi(StatusPresensi.valueOf(status));
            newPresensi.setSesiKuliah(sesiKuliah);
            newPresensi.setKrsDetail(krsDetail);
            newPresensi.setCatatan("Manual");
            newPresensi.setWaktuMasuk(LocalDateTime.now().plusHours(7));
            newPresensi.setWaktuKeluar(sesiKuliah.getWaktuSelesai());
            newPresensi.setMahasiswa(mahasiswa);
            newPresensi.setRating(0);
            presensiMahasiswaDao.save(newPresensi);
            presensiService.updateNilaiPresensi(newPresensi.getKrsDetail());

            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setId(newPresensi.getId());
            baseResponse.setNim(newPresensi.getMahasiswa().getNim());
            baseResponse.setNama(newPresensi.getMahasiswa().getNama());
            baseResponse.setMessage("Presensi " + mahasiswa.getNim() + " - " + mahasiswa.getNama() + " Telah Diperbarui");
            return baseResponse;
        }
    }

    @GetMapping("/detail-presensi-mahasiswa")
    @ResponseBody
    public List<PresensiMahasiswaDto> listPresensiMahasiswa(@RequestParam String id) {
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(id).get();
        return presensiMahasiswaDao.listDataPresensiMahasiswa(sesiKuliah);
    }

    @PostMapping("/studiesActivity/attendance/mahasiswa")
    public String prosesAttendance(@RequestParam String jadwal, @RequestParam String sesi,
                                   HttpServletRequest request) {
        Jadwal j = jadwalDao.findById(jadwal).get();
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(sesi).get();

        for (PresensiMahasiswa presensiMahasiswa : presensiMahasiswaDao.findBySesiKuliahAndStatus(sesiKuliah,
                StatusRecord.AKTIF)) {
            String pilihan = request.getParameter(presensiMahasiswa.getMahasiswa().getNim() + "nim");
            if (pilihan == null || pilihan.isEmpty()) {
                System.out.println("gaada");
            } else {
                presensiMahasiswa.setMahasiswa(presensiMahasiswa.getMahasiswa());
                StatusPresensi statusPresensi = StatusPresensi.valueOf(pilihan);
                presensiMahasiswa.setStatusPresensi(statusPresensi);
                presensiMahasiswa.setCatatan("Manual");
                presensiMahasiswa.setKrsDetail(presensiMahasiswa.getKrsDetail());
                presensiMahasiswa.setWaktuKeluar(LocalDateTime.of(LocalDate.now(), j.getJamSelesai()));
                presensiMahasiswa.setWaktuMasuk(LocalDateTime.now().plusHours(7));
                presensiMahasiswa.setSesiKuliah(sesiKuliah);
                presensiMahasiswaDao.save(presensiMahasiswa);
                presensiService.updateNilaiPresensi(presensiMahasiswa.getKrsDetail());
                System.out.println(presensiMahasiswa.getId());
            }

        }

        return "redirect:detail?jadwal=" + j.getId();
    }

    @PostMapping("/studiesActivity/attendance/sesi")
    public String saveSesiBaru(@RequestParam Jadwal jadwal, @RequestParam SesiKuliah sesi,
                               HttpServletRequest request) {
        List<KrsDetail> krsDetail = krsDetailDao.findByJadwalAndStatus(sesi.getJadwal(), StatusRecord.AKTIF);
        for (KrsDetail kd : krsDetail) {
            String pilihan = request.getParameter(kd.getMahasiswa().getNim() + "nim");
            if (pilihan == null || pilihan.isEmpty()) {
                System.out.println("gaada");
            } else {
                PresensiMahasiswa pm = presensiMahasiswaDao.findByMahasiswaAndSesiKuliahAndStatus(kd.getMahasiswa(), sesi, StatusRecord.AKTIF);
                if (pm == null) {
                    PresensiMahasiswa presensiMahasiswa = new PresensiMahasiswa();
                    presensiMahasiswa.setMahasiswa(kd.getMahasiswa());
                    StatusPresensi statusPresensi = StatusPresensi.valueOf(pilihan);
                    presensiMahasiswa.setStatusPresensi(statusPresensi);
                    presensiMahasiswa.setCatatan("Manual");
                    presensiMahasiswa.setKrsDetail(kd);
                    presensiMahasiswa.setWaktuKeluar(sesi.getWaktuSelesai());
                    presensiMahasiswa.setWaktuMasuk(sesi.getWaktuMulai());
                    presensiMahasiswa.setSesiKuliah(sesi);
                    presensiMahasiswaDao.save(presensiMahasiswa);
                    presensiService.updateNilaiPresensi(presensiMahasiswa.getKrsDetail());
                }else{
                    StatusPresensi statusPresensi = StatusPresensi.valueOf(pilihan);
                    pm.setStatusPresensi(statusPresensi);
                    pm.setCatatan("Manual");
                    pm.setWaktuKeluar(sesi.getWaktuSelesai());
                    pm.setWaktuMasuk(sesi.getWaktuMulai());
                    presensiMahasiswaDao.save(pm);
                    presensiService.updateNilaiPresensi(pm.getKrsDetail());
                }
            }
        }
        return "redirect:mahasiswa?id=" + sesi.getId();
    }

    @PostMapping("/studiesActivity/attendance/save")
    public String savePresensi(@RequestParam(required = false) String sesi,
                               @RequestParam(required = false) StatusPresensi statusPresensi) {
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(sesi).get();
        List<KrsDetail> mahasiswas = krsDetailDao.findByJadwalAndStatus(sesiKuliah.getJadwal(), StatusRecord.AKTIF);

        presensiMahasiswaDao.insertNewPresensiMahasiswa(sesiKuliah.getId(), statusPresensi.name());
        presensiMahasiswaDao.updatePresensiMahasiswa(sesiKuliah.getId(),statusPresensi.name());

        return "redirect:detail?jadwal=" + sesiKuliah.getJadwal().getId();
    }

    @GetMapping("/studiesActivity/attendance/form")
    public void editAttendance(Model model, @RequestParam(name = "id", value = "id") SesiKuliah sesiKuliah) {
        JadwalDto jadwalDto = new JadwalDto();
        jadwalDto.setId(sesiKuliah.getId());
        jadwalDto.setId(sesiKuliah.getPertemuan());
        jadwalDto.setJadwal(sesiKuliah.getJadwal());
        jadwalDto.setDetailPertemuan(sesiKuliah.getDetailPertemuan());
        jadwalDto.setBeritaAcara(sesiKuliah.getBeritaAcara());
        jadwalDto.setRps(sesiKuliah.getRps());
        jadwalDto.setDosen(sesiKuliah.getPresensiDosen().getDosen());
        jadwalDto.setPertemuan(sesiKuliah.getPertemuan());
        jadwalDto.setPresensiDosen(sesiKuliah.getPresensiDosen());
        jadwalDto.setTanggal(sesiKuliah.getWaktuMulai().toLocalDate());
        jadwalDto.setId(sesiKuliah.getId());
        jadwalDto.setJamMulai(sesiKuliah.getWaktuMulai().toLocalTime());
        jadwalDto.setJamSelesai(sesiKuliah.getWaktuSelesai().toLocalTime());
        jadwalDto.setJamMulaiString(sesiKuliah.getWaktuMulai().toLocalTime());
        jadwalDto.setJamSelesaiString(sesiKuliah.getWaktuSelesai().toLocalTime());
        jadwalDto.setCeramah(sesiKuliah.getCeramah());
        jadwalDto.setDiskusi(sesiKuliah.getDiskusi());
        jadwalDto.setPresentasi(sesiKuliah.getPresentasi());
        jadwalDto.setTanyajawab(sesiKuliah.getTanyajawab());
        jadwalDto.setKuis(sesiKuliah.getKuis());
        jadwalDto.setPraktikum(sesiKuliah.getPraktikum());
        jadwalDto.setLinkOnline(sesiKuliah.getLinkOnline());

        if (sesiKuliah.getPertemuanKe().equals(8) || sesiKuliah.getPertemuanKe().equals(16)) {
            model.addAttribute("validPertemuan", sesiKuliah.getPertemuanKe());

        }

        model.addAttribute("sesi", jadwalDto);
        model.addAttribute("teamTeaching", jadwalDosenDao.findByJadwal(sesiKuliah.getJadwal()));
        model.addAttribute("detailPertemuans", detailPertemuanDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("sesikuliah", sesiKuliah);
        model.addAttribute("materi", materiMengajarDao.findByStatusAndSesiKuliah(StatusRecord.AKTIF, sesiKuliah));

    }

    @GetMapping("/materi/{id}/sesi")
    public ResponseEntity<byte[]> lihatMateri(@PathVariable MateriMengajar id){
        String folder = id.getSesiKuliah().getJadwal().getDosen().getKaryawan().getNamaKaryawan() + " - " + id.getSesiKuliah().getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah() + " - " + id.getSesiKuliah().getPertemuanKe();
        String lokasi = uploadMateri + File.separator + folder + File.separator + id.getFile();
        log.debug("lokasi file : {}" + lokasi);
        try {
            HttpHeaders headers = new HttpHeaders();
            if (id.getFile().toLowerCase().endsWith("jpeg") || id.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (id.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (id.getFile().toLowerCase().endsWith("pdf")){
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasi));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/studiesActivity/attendance/materi/upload")
    public String tambahMateriDiawal(@RequestParam String idSesi, @RequestParam String materi, RedirectAttributes attributes){

        SesiKuliah sesi = sesiKuliahDao.findById(idSesi).get();
        if (sesi != null) {

            try {
                tugasMateriService.saveMateri(sesi, materi);
            }catch (ValidationException e){
                attributes.addFlashAttribute("pertemuanId", sesi.getId());
                attributes.addFlashAttribute("duplicateLink", e.getMessage());
                return "redirect:../detail?jadwal=" + sesi.getJadwal().getId();
            }
        }
        return "redirect:../detail?jadwal=" + sesi.getJadwal().getId();

    }

    @GetMapping("/get-materi/{id}")
    @ResponseBody
    public List<MateriMengajar> getMateriMengajar(@PathVariable String id){

        SesiKuliah sesiKuliah = sesiKuliahDao.findById(id).get();

        return materiMengajarDao.findByStatusAndSesiKuliah(StatusRecord.AKTIF, sesiKuliah);
    }

    @PostMapping("/studiesActivity/attendance/materi/input")
    public String addMateri(MultipartFile[] materi, @RequestParam SesiKuliah sesiKuliah){
        if (materi != null){
            tugasMateriService.uploadMateri(materi, sesiKuliah);
        }
        return "redirect:../form?id=" + sesiKuliah.getId();
    }

    @PostMapping("/studiesActivity/attendance/materi/delete")
    public String deleteMateri(@RequestParam MateriMengajar materi){
        if (materi != null) {
            materi.setStatus(StatusRecord.HAPUS);
            materiMengajarDao.save(materi);
        }

        return "redirect:../form?id="+materi.getSesiKuliah().getId();
    }

    @GetMapping("/studiesActivity/attendance/mulai")
    public void mulaiAttendance(Model model, @RequestParam(name = "id", value = "id") SesiKuliah sesiKuliah, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Dosen dosen = dosenDao.findByKaryawanIdUser(user);

        PresensiDto jadwalDto = new PresensiDto();
        jadwalDto.setId(sesiKuliah.getId());
        jadwalDto.setJadwal(sesiKuliah.getJadwal());
        jadwalDto.setBeritaAcara(sesiKuliah.getBeritaAcara());
        jadwalDto.setDosen(dosen);
        jadwalDto.setPresensiDosen(sesiKuliah.getPresensiDosen());
        jadwalDto.setTanggal(sesiKuliah.getWaktuMulai().toLocalDate());
        jadwalDto.setId(sesiKuliah.getId());
        jadwalDto.setJenisPertemuan(sesiKuliah.getJenisPertemuan());
        jadwalDto.setDetailPertemuan(sesiKuliah.getDetailPertemuan());
        jadwalDto.setJamMulai(sesiKuliah.getWaktuMulai().toLocalTime());
        jadwalDto.setJamSelesai(sesiKuliah.getWaktuSelesai().toLocalTime());
        jadwalDto.setIsEdit(sesiKuliah.getIsEdit());
        if (sesiKuliah.getPertemuanKe().equals(8) || sesiKuliah.getPertemuanKe().equals(16)) {
            model.addAttribute("validPertemuan", sesiKuliah.getPertemuanKe());

        }

        model.addAttribute("detailPertemuans", detailPertemuanDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("sesi", jadwalDto);
        model.addAttribute("now", LocalDate.now());
        model.addAttribute("sesiKuliah", sesiKuliah);
        model.addAttribute("teamTeaching", jadwalDosenDao.findByJadwal(sesiKuliah.getJadwal()));

    }

    @PostMapping("/studiesActivity/attendance/form")
    public String prosesEdit(@ModelAttribute JadwalDto jadwalDto) {

        SesiKuliah sesiKuliah = sesiKuliahDao.findById(jadwalDto.getId()).get();
        sesiKuliah.setBeritaAcara(jadwalDto.getBeritaAcara());
        sesiKuliah.setJenisPertemuan(jadwalDto.getJenisPertemuan());
        sesiKuliah.setDetailPertemuan(jadwalDto.getDetailPertemuan());
        sesiKuliah.setCeramah(jadwalDto.getCeramah());
        sesiKuliah.setDiskusi(jadwalDto.getDiskusi());
        sesiKuliah.setPresentasi(jadwalDto.getPresentasi());
        sesiKuliah.setTanyajawab(jadwalDto.getTanyajawab());
        sesiKuliah.setKuis(jadwalDto.getKuis());
        sesiKuliah.setPraktikum(jadwalDto.getPraktikum());

        LocalTime jamMulai = jadwalDto.getJamMulaiString().plusSeconds(30);
        LocalTime jamSelesai = jadwalDto.getJamSelesaiString().plusSeconds(30);
        LocalTime modifJamMulai = jamMulai.withSecond(0);
        LocalTime modifJamSelesai = jamSelesai.withSecond(0);

        sesiKuliah.setWaktuMulai(LocalDateTime.of(jadwalDto.getTanggal(), modifJamMulai));
        sesiKuliah.setWaktuSelesai(LocalDateTime.of(jadwalDto.getTanggal(), modifJamSelesai));
        PresensiDosen presensiDosen = presensiDosenDao.findById(sesiKuliah.getPresensiDosen().getId()).get();
        presensiDosen.setDosen(jadwalDto.getDosen());
        presensiDosen.setWaktuMasuk(sesiKuliah.getWaktuMulai());
        presensiDosen.setWaktuSelesai(sesiKuliah.getWaktuSelesai());
        presensiDosen.setTanggalInput(LocalDateTime.now().plusHours(7));
        sesiKuliah.setPertemuan(jadwalDto.getPertemuan());
        if ("Offline".equalsIgnoreCase(jadwalDto.getPertemuan()) || "Pengganti Offline".equalsIgnoreCase(jadwalDto.getPertemuan())) {
            sesiKuliah.setLinkOnline(null);
        }else{
            sesiKuliah.setLinkOnline(jadwalDto.getLinkOnline());
        }
        sesiKuliahDao.save(sesiKuliah);
        presensiDosenDao.save(presensiDosen);

        return "redirect:detail?jadwal=" + sesiKuliah.getJadwal().getId();

    }

    @PostMapping("/studiesActivity/attendance/mulai")
    public String prosesMulai(@ModelAttribute PresensiDto jadwalDto) throws Exception {
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(jadwalDto.getId()).get();
        LocalDateTime waktuMasukSebelum = sesiKuliah.getWaktuMulai();
        presensiService.mulaiMengajar(jadwalDto, sesiKuliah, waktuMasukSebelum);

        return "redirect:detail?jadwal=" + sesiKuliah.getJadwal().getId();

    }

    @PostMapping("/studiesActivity/attendance/delete")
    public String deletePresensi(@RequestParam(value = "id", name = "id") SesiKuliah sesiKuliah) {

        PresensiDosen presensiDosen = presensiDosenDao.findById(sesiKuliah.getPresensiDosen().getId()).get();
        presensiDosen.setStatus(StatusRecord.HAPUS);
        presensiDosenDao.save(presensiDosen);

        List<PresensiMahasiswa> presensiMahasiswa = presensiMahasiswaDao.findBySesiKuliah(sesiKuliah);
        for (PresensiMahasiswa pm : presensiMahasiswa) {
            pm.setStatus(StatusRecord.HAPUS);
            presensiMahasiswaDao.save(pm);
        }

        return "redirect:detail?jadwal=" + sesiKuliah.getJadwal().getId();

    }

    // KRS
    @GetMapping("/studiesActivity/krs/paket")
    public void main(Model model, @RequestParam(required = false) Kelas kelas) {
        model.addAttribute("kelas", kelasMahasiswaDao.carikelasMahasiswa());
        model.addAttribute("selected", kelas);
        List<KelasMahasiswaDto> kelasMahasiswaDtos = new ArrayList<>();

//                if (kelas != null) {
//                        Iterable<KelasMahasiswa> kelasMahasiswa = kelasMahasiswaDao.findByKelasAndStatus(kelas,
//                                        StatusRecord.AKTIF);
//                        if (IterableUtils.size(kelasMahasiswa) == 0) {
//                                model.addAttribute("kosong", "gada mahasiswa");
//                        }
//                        if (IterableUtils.size(kelasMahasiswa) > 0) {
//                                for (KelasMahasiswa km : kelasMahasiswa) {
//                                        KelasMahasiswaDto kelasMahasiswaDto = new KelasMahasiswaDto();
//                                        kelasMahasiswaDto.setNim(km.getMahasiswa().getNim());
//                                        kelasMahasiswaDto.setNama(km.getMahasiswa().getNama());
//                                        kelasMahasiswaDto.setKelas(km.getKelas().getNamaKelas());
//                                        kelasMahasiswaDtos.add(kelasMahasiswaDto);
//
//                                }
//                        }
//                        model.addAttribute("mahasiswaList", kelasMahasiswaDtos);
//                }
    }

    @PostMapping("/studiesActivity/krs/paket")
    public String paketKrs(@RequestParam(required = false) Kelas kelas, RedirectAttributes attributes) {
        if (kelas != null) {
            List<GeneratedKrsDto> listGenerated = new ArrayList<>();
            List<GeneratedKrsDto> listNoKrs = new ArrayList<>();
            Iterable<KelasMahasiswa> kelasMahasiswa = kelasMahasiswaDao.findByKelasAndStatus(kelas,
                    StatusRecord.AKTIF);
            if (kelasMahasiswa == null) {
                // model.addAttribute("empty","tidak ada mahasiswa");
            }

            if (kelasMahasiswa != null) {
                for (KelasMahasiswa km : kelasMahasiswa) {
                    Krs krs = krsDao.findByTahunAkademikAndMahasiswaAndStatus(tahunAkademikDao.findByStatus(StatusRecord.AKTIF), km.getMahasiswa(), StatusRecord.AKTIF);
                    if (krs != null) {
                        if ("AKTIF".equalsIgnoreCase(krs.getMahasiswa().getStatusAktif())) {
                            krsService.cekKrsApproval(km.getMahasiswa(), tahunAkademikDao.findByStatus(StatusRecord.AKTIF));
                            GeneratedKrsDto krsDto = new GeneratedKrsDto();
                            krsDto.setId(km.getMahasiswa().getId());
                            krsDto.setNim(km.getMahasiswa().getNim());
                            krsDto.setNama(km.getMahasiswa().getNama());
                            krsDto.setKelas(km.getKelas().getNamaKelas());
                            List<Jadwal> jadwal = jadwalDao.findByStatusAndTahunAkademikAndKelasAndHariNotNull(StatusRecord.AKTIF, tahunAkademikDao.findByStatus(StatusRecord.AKTIF), kelas);
                            List<Jadwal> listJadwal = new ArrayList<>();
                            for (Jadwal j : jadwal) {
                                KrsDetail kd = krsDetailDao.findByJadwalAndStatusAndKrs(j, StatusRecord.AKTIF, krs);
                                if (kd == null) {
                                    KrsDetail krsDetail = new KrsDetail();
                                    krsDetail.setJadwal(j);
                                    krsDetail.setKrs(krs);
                                    krsDetail.setMahasiswa(krs.getMahasiswa());
                                    krsDetail.setMatakuliahKurikulum(j.getMatakuliahKurikulum());
                                    krsDetail.setNilaiPresensi(BigDecimal.ZERO);
                                    krsDetail.setNilaiUas(BigDecimal.ZERO);
                                    krsDetail.setNilaiUts(BigDecimal.ZERO);
                                    krsDetail.setNilaiTugas(BigDecimal.ZERO);
                                    krsDetail.setFinalisasi("N");
                                    krsDetail.setJumlahKehadiran(0);
                                    krsDetail.setJumlahMangkir(0);
                                    krsDetail.setJumlahTerlambat(0);
                                    krsDetail.setJumlahSakit(0);
                                    krsDetail.setTahunAkademik(tahunAkademikDao.findByStatus(StatusRecord.AKTIF));
                                    krsDetail.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                                    krsDetail.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                                    krsDetail.setKeterangan("Generated");
                                    krsDetail.setJumlahIzin(0);
                                    krsDetailDao.save(krsDetail);

                                    log.info("generate jadwal {} nim {} nama {}", j.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah(), krs.getNim(), krs.getMahasiswa().getNama());
                                    listJadwal.add(krsDetail.getJadwal());
                                }
                            }
                            krsDto.setJadwals(listJadwal);
                            if (krsDto.getJadwals() != null && !krsDto.getJadwals().isEmpty()) {
                                listGenerated.add(krsDto);
                            }
                        }
                    } else {
                        log.info("Mahasiswa dengan nim {} dan nama {} dari prodi {} belum melakukan daftar ulang / pembayaran krs", km.getMahasiswa().getNim(), km.getMahasiswa().getNama(), km.getMahasiswa().getIdProdi().getNamaProdi());
                        GeneratedKrsDto krsDto = new GeneratedKrsDto();
                        krsDto.setNim(km.getMahasiswa().getNim());
                        krsDto.setNama(km.getMahasiswa().getNama());
                        krsDto.setKelas(km.getKelas().getNamaKelas());
                        listNoKrs.add(krsDto);
                    }
                }
            }
            attributes.addFlashAttribute("mahasiswaNoKrs", listNoKrs);
            attributes.addFlashAttribute("mahasiswaList", listGenerated);
        }
        return "redirect:paket?kelas=" + kelas.getId();
    }

    @GetMapping("/studiesActivity/krs/list")
    public void listKrs(@RequestParam(required = false) String nim,
                        @RequestParam(required = false) TahunAkademik tahunAkademik, Model model) {
        model.addAttribute("tahunAkademik",
                tahunAkademikDao.findByStatusNotInOrderByTahunDesc(Arrays.asList(StatusRecord.HAPUS)));

        if (nim != null && tahunAkademik != null) {

            // Mahasiswa dataMahasiswa = mahasiswaDao.findByNimAndStatus(nim,
            // StatusRecord.AKTIF);
            model.addAttribute("nim", nim);
            model.addAttribute("tahun", tahunAkademik);
            Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

            TahunAkademik ta = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);

            if (ta.getTanggalMulaiKrs().compareTo(LocalDate.now()) <= 0) {
                model.addAttribute("validasi", ta);
            }

            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mahasiswa, tahunAkademik,
                    StatusRecord.AKTIF);
            if (k == null) {
                model.addAttribute("validation", "Krs belum aktif, Silahkan aktifasi terlebih dahulu ");
            }

            model.addAttribute("listKrs", krsDetailDao.findByStatusAndKrsAndMahasiswaOrderByJadwalHariAscJadwalJamMulaiAsc(StatusRecord.AKTIF, k, mahasiswa));
            model.addAttribute("mahasiswa", mahasiswa);
            model.addAttribute("jumlahSks",
                    krsDetailDao.jumlahSksMahasiswa(mahasiswa.getId(), tahunAkademik.getId()));
            model.addAttribute("status", krsDao.findByTahunAkademikAndMahasiswaAndStatus(tahunAkademik,
                    mahasiswa, StatusRecord.AKTIF));

        }

    }

    @GetMapping("/detail/presensi/{id}")
    @ResponseBody
    public List<DetailPresensiDto> detailPresensiMahasiswa(@PathVariable String id) {
        KrsDetail krsDetail = krsDetailDao.findById(id).get();

        return presensiMahasiswaDao.detailPresensi(krsDetail.getMahasiswa(), krsDetail.getTahunAkademik(),
                krsDetail.getJadwal());

    }

    @GetMapping("/studiesActivity/krs/form")
    public void formKrs(@RequestParam(required = false) String nim,
                        @RequestParam(required = false) String tahunAkademik,
                        Model model, @RequestParam(required = false) String lebih,
                        @RequestParam(required = false) String kosong) {
        model.addAttribute("nim", nim);
        model.addAttribute("tahun", tahunAkademik);

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

        KelasMahasiswa kelasMahasiswa = kelasMahasiswaDao.findByMahasiswaAndStatus(mahasiswa,
                StatusRecord.AKTIF);

        TahunAkademik ta = tahunAkademikDao.findById(tahunAkademik).get();
        String firstFourChars = ta.getKodeTahunAkademik().substring(0, 4);
        System.out.println(firstFourChars);

        if (ta.getJenis() == StatusRecord.GENAP) {
            String kode = firstFourChars + "1";
            System.out.println("kode : " + kode);
            TahunAkademik tahun = tahunAkademikDao.findByKodeTahunAkademikAndJenis(kode,
                    StatusRecord.GANJIL);
            IpkDto ipk = krsDetailDao.ip(mahasiswa, tahun);
            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mahasiswa, ta, StatusRecord.AKTIF);

            Long sks = krsDetailDao.jumlahSks(StatusRecord.AKTIF, k);

            if (ipk == null) {
                model.addAttribute("kosong", "24");
            } else {

                if (ipk.getIpk().compareTo(new BigDecimal(3.00)) >= 0) {
                    model.addAttribute("full", "24");
                }
            }
            model.addAttribute("sks", sks);
        }

        if (ta.getJenis() == StatusRecord.GANJIL) {
            Integer prosesKode = Integer.valueOf(firstFourChars) - 1;
            String kode = prosesKode.toString() + "2";

            TahunAkademik tahun = tahunAkademikDao.findByKodeTahunAkademikAndJenis(kode,
                    StatusRecord.GENAP);
            IpkDto ipk = krsDetailDao.ip(mahasiswa, tahun);
            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mahasiswa, ta, StatusRecord.AKTIF);
            Long sks = krsDetailDao.jumlahSks(StatusRecord.AKTIF, k);

            if (ipk == null) {
                model.addAttribute("kosong", "24");
            } else {

                if (ipk.getIpk().compareTo(new BigDecimal(3.00)) >= 0) {
                    model.addAttribute("full", "24");
                }
            }
            model.addAttribute("sks", sks);
        }

        List<Object[]> krsDetail = krsDetailDao.newPilihKrs(ta, kelasMahasiswa.getKelas(),
                mahasiswa.getIdProdi(),
                mahasiswa, mahasiswa.getIdProdi().getIdJenjang());
        model.addAttribute("pilihanKrs", krsDetail);
        model.addAttribute("tahun", ta);
        model.addAttribute("lebih", lebih);
        model.addAttribute("kosong", kosong);

    }

    @GetMapping("/studiesActivity/krs/enable")
    public void listEnableKrs(Model model, @PageableDefault(size = 10) Pageable page) {

        model.addAttribute("listMahasiswa", krsService.mhsKrs());
        model.addAttribute("listKrs", krsService.listEnableKrs(page));
        model.addAttribute("tahun", tahunAkademikDao.findByStatus(StatusRecord.AKTIF));

    }

    @PostMapping("/studiesActivity/krs/enable/save")
    public String saveEnableKrs(@RequestParam(required = false) String[] mahasiswa, @RequestParam(required = false) String tanggalKadaluarsa) {
        LocalDate kadaluarsa = LocalDate.parse(tanggalKadaluarsa);
        krsService.saveEnabelKrs(mahasiswa, kadaluarsa);
        return "redirect:";
    }

    @PostMapping("/studiesActivity/krs/enable/edit")
    public String editEnableKrs(@RequestParam(required = false) EnableKrs enableKrs, @RequestParam(required = false) String tanggalKadaluarsa) {
        LocalDate kadaluarsa = LocalDate.parse(tanggalKadaluarsa);
        krsService.updateEnabelKrs(enableKrs, kadaluarsa);
        return "redirect:";
    }

    @PostMapping("/studiesActivity/krs/form")
    public String prosesKrs(Authentication authentication, @RequestParam String jumlah,
                            @RequestParam(required = false) String[] selected,
                            @RequestParam TahunAkademik tahunAkademik, @RequestParam String nim) {

        Mahasiswa mahasiswa = mahasiswaDao.findByNimAndStatus(nim, StatusRecord.AKTIF);
        Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mahasiswa, tahunAkademik, StatusRecord.AKTIF);

        if (k != null) {

            // Long krsDetail = krsDetailDao.jumlahSks(StatusRecord.AKTIF, k);

            Long krsDetail = krsDetailDao.cariJumlahSks(k.getId());

            if (selected == null) {

            } else {
                Long jadwal = jadwalDao.totalSks(selected);
                if (krsDetail == null) {
                    if (jadwal >= Integer.valueOf(jumlah)) {
                        return "redirect:form?lebih=true";
                    } else {
                        for (String idJadwal : selected) {
                            Jadwal j = jadwalDao.findById(idJadwal).get();
                            if (krsDetailDao.cariKrs(j, tahunAkademik, mahasiswa) == null) {
                                KrsDetail kd = new KrsDetail();
                                kd.setJadwal(j);
                                kd.setKrs(k);
                                kd.setMahasiswa(mahasiswa);
                                kd.setMatakuliahKurikulum(j.getMatakuliahKurikulum());
                                kd.setNilaiPresensi(BigDecimal.ZERO);
                                kd.setNilaiUas(BigDecimal.ZERO);
                                kd.setNilaiTugas(BigDecimal.ZERO);
                                kd.setNilaiUts(BigDecimal.ZERO);
                                kd.setFinalisasi("N");
                                kd.setJumlahMangkir(0);
                                kd.setJumlahKehadiran(0);
                                kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                                kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                                kd.setJumlahTerlambat(0);
                                kd.setJumlahIzin(0);
                                kd.setJumlahSakit(0);
                                kd.setTahunAkademik(tahunAkademik);
                                kd.setStatusEdom(StatusRecord.UNDONE);
                                krsDetailDao.save(kd);
                            }
                        }
                    }
                }

                if (krsDetail != null) {
                    if (jadwal + krsDetail > Integer.valueOf(jumlah)) {
                        return "redirect:list?nim=" + nim + "&tahunAkademik="
                                + tahunAkademik.getId() + "&lebih=true";
                    } else {
                        for (String idJadwal : selected) {
                            Jadwal j = jadwalDao.findById(idJadwal).get();
                            if (krsDetailDao.cariKrs(j, tahunAkademik, mahasiswa) == null) {

                                KrsDetail kd = new KrsDetail();
                                kd.setJadwal(j);
                                kd.setKrs(k);
                                kd.setMahasiswa(mahasiswa);
                                kd.setMatakuliahKurikulum(j.getMatakuliahKurikulum());
                                kd.setNilaiTugas(BigDecimal.ZERO);
                                kd.setNilaiPresensi(BigDecimal.ZERO);
                                kd.setFinalisasi("N");
                                kd.setNilaiUas(BigDecimal.ZERO);
                                kd.setNilaiUts(BigDecimal.ZERO);
                                kd.setJumlahMangkir(0);
                                kd.setTahunAkademik(tahunAkademik);
                                kd.setJumlahKehadiran(0);
                                kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                                kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                                kd.setJumlahTerlambat(0);
                                kd.setJumlahIzin(0);
                                kd.setJumlahSakit(0);
                                kd.setStatusEdom(StatusRecord.UNDONE);
                                krsDetailDao.save(kd);
                            }
                        }
                    }
                }

            }
        } else {
            return "redirect:list?nim=" + nim + "&tahunAkademik=" + tahunAkademik.getId();
        }

        return "redirect:list?nim=" + nim + "&tahunAkademik=" + tahunAkademik.getId();

    }

    @PostMapping("/studiesActivity/krs/delete")
    public String deleteKrs(@RequestParam(name = "id", value = "id") KrsDetail krsDetail) {
        krsDetail.setStatus(StatusRecord.HAPUS);
        krsDetailDao.save(krsDetail);

        return "redirect:list?nim=" + krsDetail.getMahasiswa().getNim() + "&tahunAkademik="
                + krsDetail.getTahunAkademik().getId();

    }

    @GetMapping("/studiesActivity/krs/approval/list")
    public void krsApprovalDosenWali(Model model, Authentication authentication,
                                     @PageableDefault(size = 10) Pageable page) {
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        Dosen dosen = dosenDao.findByKaryawan(karyawan);
        TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
        model.addAttribute("tahun", tahun);

        String jadi;
        if (tahun.getJenis() == StatusRecord.GANJIL) {
            Integer ta = new Integer(tahun.getTahun());
            Integer k = ta - 1;
            jadi = k + "2";
        } else {
            Integer ta = new Integer(tahun.getKodeTahunAkademik());
            Integer j = ta - 1;
            jadi = j.toString();
        }
        TahunAkademik tahunBefore = tahunAkademikDao.findByKodeTahunAkademik(jadi);
        //
        Page<Object[]> krs = krsApprovalDao.krsApproval(tahun, tahunBefore, dosen.getId(),
                tahun.getKodeTahunAkademik(), page);
        //
        // model.addAttribute("listKrsDetail",
        // krsDetailDao.findByStatusAndTahunAkademik(StatusRecord.AKTIF, tahun));
        // model.addAttribute("listMatkulMengulang",
        // krsApprovalDao.listMatkulMengulang(jadi));
        model.addAttribute("listKrs", krs);

        // list MBKM
        List<PendaftaranMbkm> listRequest = pendaftaranMbkmDao.findByDosenAndStatusOrderByStatusApprovalDesc(dosen, StatusRecord.AKTIF);
        model.addAttribute("list", listRequest);
        model.addAttribute("tahun", tahun);

    }

    @GetMapping("/get-matkul-mengulang/{id}")
    @ResponseBody
    public List<MatkulMengulangDto> getMatulMengulang(@PathVariable String id) {
        Krs krs = krsDao.findById(id).get();
        TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
        String jadi;
        if (tahun.getJenis() == StatusRecord.GANJIL) {
            Integer ta = new Integer(tahun.getTahun());
            Integer k = ta - 1;
            jadi = k + "2";
        } else {
            Integer ta = new Integer(tahun.getKodeTahunAkademik());
            Integer j = ta - 1;
            jadi = j.toString();
        }
        return krsApprovalDao.listMatkulMengulang(jadi, krs.getMahasiswa().getId());
    }

    @GetMapping("/get-request-matkul/{id}")
    @ResponseBody
    public List<Object[]> getRequestMatkulKrs(@PathVariable String id) {
        TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
        Krs krs = krsDao.findById(id).get();

        return krsDetailDao.cariMatkurKrs(krs.getId());
    }

    @PostMapping("/studiesActivity/krs/approval/approve")
    public String approvedKrs(@RequestParam KrsApproval krsApproval,
                              @RequestParam(required = false) String keterangan) {

        krsApproval.setKomentar(keterangan);
        krsApproval.setStatus(StatusApprove.APPROVED);
        krsApprovalDao.save(krsApproval);

        // KelasMahasiswa kelas =
        // kelasMahasiswaDao.findByMahasiswaAndStatus(krsApproval.getMahasiswa(),
        // StatusRecord.AKTIF);
        // if (kelas != null) {
        // mailService.suksesKrs(krsApproval, kelas);
        // }

        return "redirect:list";
    }

    @PostMapping("/studiesActivity/krs/approval/reject")
    public String rejectedKrs(@RequestParam KrsApproval krsApproval,
                              @RequestParam(required = false) String keterangan) {

        krsApproval.setKomentar(keterangan);
        krsApproval.setStatus(StatusApprove.REJECTED);
        krsApprovalDao.save(krsApproval);

        List<KrsDetail> setDetail = krsDetailDao.findByStatusAndKrsAndMahasiswa(StatusRecord.AKTIF,
                krsApproval.getKrs(), krsApproval.getMahasiswa());
        for (KrsDetail krs : setDetail) {
            krs.setStatus(StatusRecord.REJECTED);
            krsDetailDao.save(krs);
        }
        return "redirect:list";
    }

    // Assesment

    @GetMapping("/studiesActivity/assesment/list")
    public void assesmentList(Model model, @RequestParam(required = false) TahunAkademik tahunAkademik,
                              @RequestParam(required = false) Prodi prodi, @RequestParam(required = false) String search) {
        if (tahunAkademik != null) {
            model.addAttribute("selectedTahun", tahunAkademik);
            model.addAttribute("selectedProdi", prodi);
            if (!StringUtils.isEmpty(search)) {
                model.addAttribute("search", search);
                model.addAttribute("list",
                        jadwalDao.assesmentSearch(prodi, Arrays.asList(StatusRecord.HAPUS),
                                tahunAkademik, search));
            } else {
                model.addAttribute("list",
                        jadwalDao.assesment(prodi, Arrays.asList(StatusRecord.HAPUS),
                                tahunAkademik));
            }
        }
    }

    @GetMapping("/studiesActivity/assesment/listdosen")
    public void listPenilaianDosen(Model model, Authentication authentication,
                                   @RequestParam(required = false) TahunAkademik tahunAkademik) {
        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        Dosen dosen = dosenDao.findByKaryawan(karyawan);

        if (tahunAkademik != null) {
            model.addAttribute("selectedTahun", tahunAkademik);

            TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
            model.addAttribute("jadwal",
                    jadwalDosenDao
                            .findByJadwalStatusNotInAndJadwalTahunAkademikAndDosenAndJadwalHariNotNullAndJadwalKelasNotNullOrderByJadwalHariAscJadwalJamMulaiAsc(
                                    Arrays.asList(StatusRecord.HAPUS), tahunAkademik, dosen));
            model.addAttribute("dosenAkses", jadwalDosenDao.findByJadwalTahunAkademik(tahunAkademik));
        } else {
            TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);
            model.addAttribute("jadwal", jadwalDosenDao
                    .findByJadwalStatusNotInAndJadwalTahunAkademikAndDosenAndJadwalHariNotNullAndJadwalKelasNotNullOrderByJadwalHariAscJadwalJamMulaiAsc(
                            Arrays.asList(StatusRecord.HAPUS), tahun, dosen));
        }

    }

    @PostMapping("/berkas/uts/setting")
    public String saveAksesUts(@RequestParam Jadwal jadwal, @RequestParam(required = false) String uts) {

        log.info("jadwal: {}", jadwal.getId());
        log.info("uts: {}", uts);

        Dosen Duts = dosenDao.findById(uts).get();

        jadwal.setAksesUts(Duts);
        jadwalDao.save(jadwal);

        return "redirect:../uploadSoal/list";
    }

    @PostMapping("/berkas/uas/setting")
    public String saveAksesUas(@RequestParam Jadwal jadwal, @RequestParam(required = false) String uas) {

        log.info("jadwal: {}", jadwal.getId());
        log.info("uas: {}", uas);

        Dosen Duas = dosenDao.findById(uas).get();

        jadwal.setAksesUas(Duas);
        jadwalDao.save(jadwal);

        return "redirect:../uploadSoal/list";
    }

    @GetMapping("/studiesActivity/assesment/tugas")
    public void uploadTugas(@RequestParam Jadwal jadwal, Model model, @RequestParam(required = false) String sesi){
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("now", LocalDate.now());
        model.addAttribute("sesi", sesi);
        BigDecimal bobotMaks = jadwal.getBobotTugas().subtract(jadwalTugasDao.totalBobot(jadwal));
        model.addAttribute("bobotMaksTugas", bobotMaks);
        BigDecimal bobotMaksUTS = jadwal.getBobotUts().subtract(jadwalTugasDao.totalBobotUTS(jadwal));
        model.addAttribute("bobotMaksUTS", bobotMaksUTS);
        BigDecimal bobotMaksUAS = jadwal.getBobotUas().subtract(jadwalTugasDao.totalBobotUAS(jadwal));
        model.addAttribute("bobotMaksUAS", bobotMaksUAS);
    }

    @PostMapping("/studiesActivity/assesment/tugas")
    public String simpanTugas(@Valid JadwalTugas jadwalTugas, @RequestParam Jadwal jadwal, MultipartFile fileTugas,
                              @RequestParam(required = false) String online, @RequestParam(required = false) String upload,
                              @RequestParam(required = false) String presentasi, @RequestParam(required = false) String ujian,
                              RedirectAttributes attributes) throws IOException {
        log.info("Online : {}, Upload : {}, Presentasi : {}, Ujian : {}", online, upload, presentasi, ujian);
        SesiKuliah sesiKuliah = sesiKuliahDao.findByJadwalAndPertemuanKe(jadwal, jadwalTugas.getPertemuanKe()).get();
        if (online == null && upload == null && presentasi == null && ujian == null) {
            attributes.addFlashAttribute("gagal", "Wajib pilih salah satu jenis pengiriman");
            return "redirect:tugas?jadwal="+jadwal.getId();
        }else{
            jadwalTugas.setJenisPengiriman(tugasMateriService.setPengiriman(online, upload, presentasi, ujian));
        }
        jadwalTugas.setSesiKuliah(sesiKuliah);
        if (fileTugas != null && !fileTugas.isEmpty()){
            try {
                jadwalTugas.setFile(tugasMateriService.uploadFile(fileTugas, jadwalTugas));
                jadwalTugas.setNamaFile(fileTugas.getOriginalFilename());
            }catch (IOException e){
                throw new  RuntimeException("Gagal memproses file", e);
            }
            log.info("File : {}", fileTugas);
        }else{
            jadwalTugas.setFile(null);
            jadwalTugas.setNamaFile(null);
        }
        if (jadwalTugas.getWaktuMulai() == null) {
            jadwalTugas.setWaktuMulai(LocalDateTime.now());
        }
        jadwalTugasDao.save(jadwalTugas);
        tugasMateriService.saveBobotTugas(jadwal, jadwalTugas);

        if(!KategoriTugas.TUGAS.equals(jadwalTugas.getKategoriTugas())){
            SesiKuliah sk = jadwalTugas.getSesiKuliah();
            sk.setJenisPertemuan(jadwalTugas.getKategoriTugas().toString());
            sesiKuliahDao.save(sk);
        }

        return "redirect:weight?jadwal="+sesiKuliah.getJadwal().getId();
    }

    @GetMapping("/studiesActivity/assesment/edit")
    public void editTugas(@RequestParam(name = "id", value = "id") JadwalTugas jadwalTugas, Model model) {
        switch (jadwalTugas.getJenisPengiriman()){
            case ONLINE_TEKS:
                model.addAttribute("onlineteks", "checked");
                break;
            case UPLOAD_FILE:
                model.addAttribute("uploadfile", "checked");
                break;
            case PRESENTASI:
                model.addAttribute("presentasi", "checked");
                break;
            case UJIAN:
                model.addAttribute("ujian", "checked");
                break;
            case ONLINE_TEKS_UPLOAD_FILE:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("uploadfile", "checked");
                break;
            case ONLINE_TEKS_PRESENTASI:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("presentasi", "checked");
                break;
            case ONLINE_TEKS_UJIAN:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("ujian", "checked");
                break;
            case UPLOAD_FILE_PRESENTASI:
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("presentasi", "checked");
                break;
            case UPLOAD_FILE_UJIAN:
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("ujian", "checked");
                break;
            case PRESENTASI_UJIAN:
                model.addAttribute("presentasi", "checked");
                model.addAttribute("ujian", "checked");
                break;
            case ONLINE_TEKS_UPLOAD_FILE_PRESENTASI:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("presentasi", "checked");
                break;
            case ONLINE_TEKS_UPLOAD_FILE_UJIAN:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("ujian", "checked");
                break;
            case UPLOAD_FILE_PRESENTASI_UJIAN:
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("presentasi", "checked");
                model.addAttribute("ujian", "checked");
                break;
            default:
                model.addAttribute("onlineteks", "checked");
                model.addAttribute("uploadfile", "checked");
                model.addAttribute("presentasi", "checked");
                model.addAttribute("ujian", "checked");
                break;
        }
        Jadwal jadwal = jadwalTugas.getSesiKuliah().getJadwal();
        BigDecimal bobotMaks = jadwal.getBobotTugas().subtract(jadwalTugasDao.totalBobot(jadwal));
        BigDecimal bobotMaksUTS = jadwal.getBobotUts().subtract(jadwalTugasDao.totalBobotUTS(jadwal));
        BigDecimal bobotMaksUAS = jadwal.getBobotUas().subtract(jadwalTugasDao.totalBobotUAS(jadwal));
        BigDecimal akumulasiBobot = BigDecimal.ZERO;
        BigDecimal akumulasiBobotUTS = BigDecimal.ZERO;
        BigDecimal akumulasiBobotUAS = BigDecimal.ZERO;
        if (jadwalTugas.getBobot() != null) {
            if (KategoriTugas.TUGAS.equals(jadwalTugas.getKategoriTugas())) {
                akumulasiBobot = bobotMaks.add(jadwalTugas.getBobot());
            }else{
                akumulasiBobot = bobotMaks;
            }
            if (KategoriTugas.UTS.equals(jadwalTugas.getKategoriTugas())) {
                akumulasiBobotUTS = bobotMaksUTS.add(jadwalTugas.getBobot());
            }else{
                akumulasiBobotUTS = bobotMaksUTS;
            }
            if (KategoriTugas.UAS.equals(jadwalTugas.getKategoriTugas())) {
                akumulasiBobotUAS = bobotMaksUAS.add(jadwalTugas.getBobot());
            }else{
                akumulasiBobotUAS = bobotMaksUAS;
            }
        }else{
            akumulasiBobot = bobotMaks;
            akumulasiBobotUTS = bobotMaksUTS;
            akumulasiBobotUAS = bobotMaksUAS;
        }

        model.addAttribute("bobotMaksTugas", akumulasiBobot);
        model.addAttribute("bobotMaksUTS", akumulasiBobotUTS);
        model.addAttribute("bobotMaksUAS", akumulasiBobotUAS);
        model.addAttribute("now", LocalDate.now());
        model.addAttribute("jadwalTugas", jadwalTugas);
    }

    @PostMapping("/studiesActivity/assesment/edit")
    public String prosesEditTugas(@ModelAttribute @Valid JadwalTugas jadwalTugas, MultipartFile fileTugas,
                                  @RequestParam(required = false) String online, @RequestParam(required = false) String upload,
                                  @RequestParam(required = false) String presentasi, @RequestParam(required = false) String ujian,
                                  RedirectAttributes attributes) throws IOException {

        log.info("edit tugas : {}", jadwalTugas);
        SesiKuliah sesiKuliah = sesiKuliahDao.findByJadwalAndPertemuanKe(jadwalTugas.getSesiKuliah().getJadwal(), jadwalTugas.getPertemuanKe()).get();
        if (online == null && upload == null && presentasi == null && ujian == null) {
            attributes.addFlashAttribute("gagal", "Wajib pilih salah satu jenis pengiriman");
            return "redirect:tugas?jadwal="+jadwalTugas.getSesiKuliah().getJadwal().getId();
        }else{
            jadwalTugas.setJenisPengiriman(tugasMateriService.setPengiriman(online, upload, presentasi, ujian));
        }
        jadwalTugas.setSesiKuliah(sesiKuliah);
        if (fileTugas != null && !fileTugas.isEmpty()){
            try {
                jadwalTugas.setFile(tugasMateriService.uploadFile(fileTugas, jadwalTugas));
                jadwalTugas.setNamaFile(fileTugas.getOriginalFilename());
            }catch (IOException e){
                throw new  RuntimeException("Gagal memproses file", e);
            }
            log.info("File : {}", fileTugas);
        }else{
            jadwalTugas.setFile(null);
            jadwalTugas.setNamaFile(null);
        }
        jadwalTugasDao.save(jadwalTugas);
        tugasMateriService.saveBobotTugas(jadwalTugas.getSesiKuliah().getJadwal(), jadwalTugas);
        return "redirect:weight?jadwal=" + jadwalTugas.getSesiKuliah().getJadwal().getId();
    }

    @PostMapping("/studiesActivity/assesment/delete")
    public String deleteBobot(@RequestParam(name = "id", value = "id") String tugas) {
        JadwalTugas jadwalTugas = jadwalTugasDao.findById(tugas).get();
        jadwalTugas.setStatus(StatusRecord.HAPUS);
        jadwalTugasDao.save(jadwalTugas);
        tugasMateriService.deleteBobotTugas(jadwalTugas);
        return "redirect:weight?jadwal=" + jadwalTugas.getSesiKuliah().getJadwal().getId();
    }

    @GetMapping("/studiesActivity/assesment/collection")
    public void pengumpulanTugas(Model model, @RequestParam JadwalTugas jadwalTugas){
        model.addAttribute("jadwaltugas", jadwalTugas);
        model.addAttribute("tugasmahasiswa", tugasJawabanDao.findByJadwalTugasAndStatusOrderByWaktuPengumpulan(jadwalTugas, StatusRecord.AKTIF));
    }

//    @GetMapping("/studiesActivity/assesment/collection/download")
//    public void downloadAllSubmission(@RequestParam JadwalTugas jadwalTugas, HttpServletRequest request, HttpServletResponse response) throws IOException {
//        tugasMateriService.downloadAllSubmission(jadwalTugas, request, response);
//    }

    @PostMapping("/studiesActivity/assesment/collection")
    public String nilaiTugas(@RequestParam JadwalTugas jadwalTugas, HttpServletRequest request){
        List<JadwalTugasJawaban> listJawaban = tugasJawabanDao.findByJadwalTugasAndStatusOrderByWaktuPengumpulan(jadwalTugas, StatusRecord.AKTIF);
        for (JadwalTugasJawaban j : listJawaban){
            String pilihan = request.getParameter("jawab-"+j.getId());
            if (pilihan != null && !pilihan.trim().isEmpty()){
                j.setNilai(new BigDecimal(pilihan));
                tugasJawabanDao.save(j);
                tugasMateriService.saveNilaiJawaban(pilihan, j, jadwalTugas);
            }
        }
        return "redirect:weight?jadwal=" + jadwalTugas.getSesiKuliah().getJadwal().getId();
    }

    @GetMapping("/collection/{jawaban}/tugas")
    public ResponseEntity<byte[]> lihatMateri(@PathVariable JadwalTugasJawaban jawaban){
//        String folder = jawaban.getMahasiswa().getNim() + " - " + jawaban.getJadwalTugas().getSesiKuliah().getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah();
        String folder = jawaban.getJadwalTugas().getSesiKuliah().getJadwal().getTahunAkademik().getNamaTahunAkademik().replace("/", "-") + File.separator +
                jawaban.getJadwalTugas().getSesiKuliah().getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah().replace("/", "-") + " - " +
                jawaban.getJadwalTugas().getSesiKuliah().getJadwal().getDosen().getKaryawan().getNamaKaryawan() + File.separator +
                "Pertemuan " + jawaban.getJadwalTugas().getPertemuanKe() + File.separator + jawaban.getMahasiswa().getNim() + " - " +
                jawaban.getMahasiswa().getNama();
        String lokasi = uploadJawaban + File.separator + folder + File.separator + jawaban.getFile();
        log.debug("lokasi file : {}" + lokasi);
        try {
            HttpHeaders headers = new HttpHeaders();
            if (jawaban.getFile().toLowerCase().endsWith("jpeg") || jawaban.getFile().toLowerCase().endsWith("jpg")){
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (jawaban.getFile().toLowerCase().endsWith("png")){
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (jawaban.getFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else if (jawaban.getFile().toLowerCase().endsWith("doc") || jawaban.getFile().toLowerCase().endsWith("docx")) {
                headers.setContentType(MediaType.parseMediaType("application/msword"));
            } else if (jawaban.getFile().toLowerCase().endsWith("xls") || jawaban.getFile().toLowerCase().endsWith("xlsx")) {
                headers.setContentType(MediaType.parseMediaType("application/vnd.ms-excel"));
            }else if (jawaban.getFile().toLowerCase().endsWith("ppt") || jawaban.getFile().toLowerCase().endsWith("pptx")){
                headers.setContentType(MediaType.parseMediaType("application/vnd.ms-powerpoint"));
            }else if (jawaban.getFile().toLowerCase().endsWith("txt")){
                headers.setContentType(MediaType.TEXT_PLAIN);
            } else{
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasi));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        }catch (Exception err){
            log.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/studiesActivity/assesment/detail")
    public void detailJawaban(Model model, @RequestParam JadwalTugasJawaban jawaban){
        model.addAttribute("tugasjawaban", jawaban);
    }

    @PostMapping("/studiesActivity/assesment/collection/detail")
    public String nilaiTugasDetail(@RequestParam JadwalTugasJawaban jawaban, @RequestParam(required = false) String masukan, @RequestParam String nilai){
        if (nilai != null && !nilai.trim().isEmpty()){
            jawaban.setNilai(new BigDecimal(nilai));
            jawaban.setMasukan(masukan);
            tugasJawabanDao.save(jawaban);
            tugasMateriService.saveNilaiJawaban(nilai, jawaban, jawaban.getJadwalTugas());
        }
        return "redirect:../collection?jadwalTugas=" + jawaban.getJadwalTugas().getId();
    }

    @GetMapping("/studiesActivity/assesment/upload/soal")
    public void listUas(@RequestParam Jadwal jadwal, @RequestParam StatusRecord status,
                        Authentication authentication,
                        Model model) {

        if (StatusRecord.UTS == status) {
            Iterable<Soal> soal = soalDao.findByJadwalAndStatusAndStatusSoal(jadwal, StatusRecord.AKTIF,
                    StatusRecord.UTS);
            User user = currentUserService.currentUser(authentication);
            Karyawan karyawan = karyawanDao.findByIdUser(user);
            Soal s = soalDao.findByJadwalAndStatusAndStatusApproveAndStatusSoal(jadwal, StatusRecord.AKTIF,
                    StatusApprove.APPROVED, StatusRecord.UTS);
            Dosen dosen = dosenDao.findByKaryawan(karyawan);
            Soal validasi = soalDao.findByJadwalAndStatusSoalAndStatusAndStatusApproveIn(jadwal,
                    StatusRecord.UTS,
                    StatusRecord.AKTIF,
                    Arrays.asList(StatusApprove.WAITING, StatusApprove.APPROVED));
            model.addAttribute("cek", validasi);
            model.addAttribute("jadwal", jadwal);
            model.addAttribute("soal", soal);
            model.addAttribute("dosen", dosen);
            model.addAttribute("approve", s);
            model.addAttribute("status", status);
        }

        if (StatusRecord.UAS == status) {
            Iterable<Soal> soal = soalDao.findByJadwalAndStatusAndStatusSoal(jadwal, StatusRecord.AKTIF,
                    StatusRecord.UAS);
            User user = currentUserService.currentUser(authentication);
            Karyawan karyawan = karyawanDao.findByIdUser(user);
            Soal s = soalDao.findByJadwalAndStatusAndStatusApproveAndStatusSoal(jadwal, StatusRecord.AKTIF,
                    StatusApprove.APPROVED, StatusRecord.UAS);
            Dosen dosen = dosenDao.findByKaryawan(karyawan);
            Soal validasi = soalDao.findByJadwalAndStatusSoalAndStatusAndStatusApproveIn(jadwal,
                    StatusRecord.UAS,
                    StatusRecord.AKTIF,
                    Arrays.asList(StatusApprove.WAITING, StatusApprove.APPROVED));
            model.addAttribute("cek", validasi);
            model.addAttribute("jadwal", jadwal);
            model.addAttribute("soal", soal);
            model.addAttribute("dosen", dosen);
            model.addAttribute("status", status);
            model.addAttribute("approve", s);
        }

    }

    @PostMapping("/studiesActivity/assesment/upload/soal")
    public String uploadBukti(@Valid Soal soal,
                              BindingResult error, MultipartFile file,
                              Authentication currentUser) throws Exception {

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        System.out.println("Nama File : {}" + namaFile);
        System.out.println("Jenis File : {}" + jenisFile);
        System.out.println("Nama Asli File : {}" + namaAsli);
        System.out.println("Ukuran File : {}" + ukuran);

        // memisahkan extensi
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }

        String idFile = soal.getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah() + "-"
                + soal.getJadwal().getKelas().getNamaKelas();
        String lokasiUpload = uploadFolder + File.separator + soal.getJadwal().getId();
        LOGGER.debug("Lokasi upload : {}", lokasiUpload);
        new File(lokasiUpload).mkdirs();
        File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);
        LOGGER.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

        soal.setStatus(StatusRecord.AKTIF);
        soal.setTanggalUpload(LocalDate.now());
        soal.setStatusApprove(StatusApprove.WAITING);
        soal.setFileUpload(idFile + "." + extension);

        if (soal.getStatusSoal() == StatusRecord.UTS) {
            Soal s = soalDao.findByJadwalAndStatusAndStatusApproveNotInAndStatusSoal(soal.getJadwal(),
                    StatusRecord.AKTIF, Arrays.asList(StatusApprove.REJECTED), StatusRecord.UTS);
            if (s == null) {

                soalDao.save(soal);
            }

            if (s != null) {
                s.setStatus(StatusRecord.NONAKTIF);
                soalDao.save(s);
                soalDao.save(soal);
            }
        } else if (soal.getStatusSoal() == StatusRecord.UAS) {
            Soal s = soalDao.findByJadwalAndStatusAndStatusApproveNotInAndStatusSoal(soal.getJadwal(),
                    StatusRecord.AKTIF, Arrays.asList(StatusApprove.REJECTED), StatusRecord.UAS);
            if (s == null) {
                soalDao.save(soal);
            }

            if (s != null) {
                s.setStatus(StatusRecord.NONAKTIF);
                soalDao.save(s);
                soalDao.save(soal);
            }
        }

        Jadwal jadwal = jadwalDao.findById(soal.getJadwal().getId()).get();
        if (soal.getStatusSoal() == StatusRecord.UTS) {
            jadwal.setStatusUts(StatusApprove.WAITING);
            jadwal.setFormatUts(soal.getFormatUjian());
        }
        if (soal.getStatusSoal() == StatusRecord.UAS) {
            jadwal.setStatusUas(StatusApprove.WAITING);
            jadwal.setFormatUas(soal.getFormatUjian());

        }
        jadwalDao.save(jadwal);

        return "redirect:soal?jadwal=" + soal.getJadwal().getId() + "&status=" + soal.getStatusSoal();

    }

    @PostMapping("/studiesActivity/assesment/upload/soal/delete")
    public String deleteSoal(@RequestParam Soal soal) {
        soal.setStatus(StatusRecord.HAPUS);
        soalDao.save(soal);
        return "redirect:../soal?jadwal=" + soal.getJadwal().getId() + "&status=" + soal.getStatusSoal();
    }

    @GetMapping("/contoh/soal")
    public void contohUts(HttpServletResponse response) throws Exception {
        response.setContentType("application/msword");
        response.setHeader("Content-Disposition", "attachment; filename=Template-Soal.doc");
        FileCopyUtils.copy(contohSoal.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/contoh/soaluas")
    public void contohUas(HttpServletResponse response) throws Exception {
        response.setContentType("application/msword");
        response.setHeader("Content-Disposition", "attachment; filename=Template-Soal.doc");
        FileCopyUtils.copy(contohSoalUas.getInputStream(), response.getOutputStream());
        response.getOutputStream().flush();
    }

    @GetMapping("/studiesActivity/assesment/weight")
    public void weightAssesment(@RequestParam Jadwal jadwal, Model model) {
        model.addAttribute("absensi", presensiDosenDao.countByStatusAndJadwal(StatusRecord.AKTIF, jadwal));
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("jumlahMahasiswa",
                krsDetailDao.findByJadwalAndStatusOrderByMahasiswaNamaAsc(jadwal, StatusRecord.AKTIF)
                        .size());
        model.addAttribute("bobot", jadwalTugasDao.findBySesiKuliahJadwalAndStatusOrderByPertemuanKe(jadwal, StatusRecord.AKTIF));
    }

    @PostMapping("/studiesActivity/assesment/recalculate")
    public String recalculateNilai(@RequestParam Jadwal jadwal){
        scoreService.reCalculatingNilai(jadwal);

        return "redirect:weight?jadwal="+jadwal.getId();
    }

    @PostMapping("/studiesActivity/assesment/weight")
    public String prosesWeight(@ModelAttribute @Valid KriteriaDto kriteriaDto, RedirectAttributes attributes) {
        Jadwal jadwal = jadwalDao.findById(kriteriaDto.getJadwal()).get();

        jadwal.setBobotPresensi(kriteriaDto.getBobotPresensi());
        jadwal.setBobotTugas(kriteriaDto.getBobotTugas());
        jadwal.setBobotUts(kriteriaDto.getBobotUts());
        jadwal.setBobotUas(kriteriaDto.getBobotUas());
        jadwalDao.save(jadwal);

        System.out.println(kriteriaDto);

        return "redirect:weight?jadwal=" + jadwal.getId();
    }

    @GetMapping("/studiesActivity/assesment/score")
    public String assesmentScore(@RequestParam Jadwal jadwal, Model model, RedirectAttributes attributes) {

        if (jadwal.getFinalStatus().equals("FINAL")) {
            return "redirect:weight?jadwal=" + jadwal.getId();

        } else {
            if (jadwal.getMatakuliahKurikulum().getSds() != null) {
                BigDecimal totalBobot = jadwal.getBobotPresensi().add(jadwal.getBobotTugas())
                        .add(jadwal.getBobotUas())
                        .add(jadwal.getBobotUts())
                        .add(new BigDecimal(jadwal.getMatakuliahKurikulum().getSds()));
                if (totalBobot.toBigInteger().intValueExact() < 100) {
                    attributes.addFlashAttribute("tidakvalid", "Melebihi Batas");
                    return "redirect:weight ?jadwal=" + jadwal.getId();
                }
            } else {
                BigDecimal totalBobot = jadwal.getBobotPresensi().add(jadwal.getBobotTugas())
                        .add(jadwal.getBobotUas())
                        .add(jadwal.getBobotUts());
                if (totalBobot.toBigInteger().intValueExact() < 100) {
                    attributes.addFlashAttribute("tidakvalid", "Melebihi Batas");
                    return "redirect:weight?jadwal=" + jadwal.getId();
                }
                if (totalBobot.toBigInteger().intValueExact() > 100) {
                    attributes.addFlashAttribute("tidakvalid", "Melebihi Batas");
                    return "redirect:weight?jadwal=" + jadwal.getId();
                }
            }
        }


        model.addAttribute("detailJadwal", jadwal);
        model.addAttribute("jadwal", krsDetailDao.findByJadwalAndStatus(jadwal,StatusRecord.AKTIF));
        model.addAttribute("jumlahMahasiswa",
                krsDetailDao.findByJadwalAndStatusOrderByMahasiswaNamaAsc(jadwal, StatusRecord.AKTIF)
                        .size());
        model.addAttribute("nilai", tugasMateriService.getNilaiByJadwal(jadwal));

        return "studiesActivity/assesment/score";

    }

    @GetMapping("/studiesActivity/assesment/list-mahasiswa/{id}")
    @ResponseBody
    public ResponseEntity<BaseResponseDto> listMahasiswaAssesment(@PathVariable String id){
        Jadwal jadwal = jadwalDao.findById(id).get();
        List<KrsDetail> krsDetails = krsDetailDao.findByJadwalAndStatus(jadwal,StatusRecord.AKTIF);
        BaseResponseDto baseResponseDto = new BaseResponseDto();
        baseResponseDto.setResponseMessage("Success");
        baseResponseDto.setResponseCode("200");
        baseResponseDto.setData(krsDetails);

        return  ResponseEntity.ok().body(baseResponseDto);
    }

    @PostMapping("/studiesActivity/assesment/task")
    public String tambahTugas(@ModelAttribute @Valid BobotTugas bobotTugas,
                              RedirectAttributes redirectAttributes) {

        BigDecimal totalBobotUtama = jadwalDao.bobotUtsUas(bobotTugas.getJadwal().getId());
        if (totalBobotUtama == null) {
            totalBobotUtama = BigDecimal.ZERO;
        }

        BigDecimal totalBobotTugas = bobotTugasDao.totalBobotTugas(bobotTugas.getJadwal().getId());
        if (totalBobotTugas == null) {
            totalBobotTugas = BigDecimal.ZERO;
        }

        BigDecimal total = totalBobotTugas.add(totalBobotUtama.add(bobotTugas.getBobot()));

        if (total.compareTo(new BigDecimal(100)) > 0) {
            redirectAttributes.addFlashAttribute("gagal", "Save Data Gagal");
            return "redirect:weight?jadwal=" + bobotTugas.getJadwal().getId();
        } else {
            bobotTugasDao.save(bobotTugas);
            List<KrsDetail> krsDetail = krsDetailDao.findByJadwalAndStatus(bobotTugas.getJadwal(),
                    StatusRecord.AKTIF);
            for (KrsDetail kd : krsDetail) {
                NilaiTugas nilaiTugas = new NilaiTugas();
                nilaiTugas.setNilai(BigDecimal.ZERO);
                nilaiTugas.setNilaiAkhir(BigDecimal.ZERO);
                nilaiTugas.setKrsDetail(kd);
                nilaiTugas.setBobotTugas(bobotTugas);
                nilaiTugas.setStatus(StatusRecord.AKTIF);
                nilaiTugasDao.save(nilaiTugas);
            }
            return "redirect:weight?jadwal=" + bobotTugas.getJadwal().getId();
        }

    }

    @GetMapping("/studiesActivity/assesment/rekap")
    public void rekapScore(@RequestParam Jadwal jadwal, Model model, RedirectAttributes attributes) {
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("dosen", jadwalDosenDao.headerJadwal(jadwal.getId()));
        model.addAttribute("nilai", presensiMahasiswaDao.bkdNilai(jadwal));
    }

    @PostMapping(value = "/studiesActivity/assesment/score")
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public ResponseEntity<BaseResponseDto> simpanNilai(@RequestBody ScoreInput scoreInput) throws Exception {

        KrsDetail krsDetail = scoreService.hitungKomponenNilai(scoreInput);
        BobotDto bobotDto = new BobotDto();
        bobotDto.setNama(krsDetail.getGrade());
        bobotDto.setBobot(krsDetail.getNilaiAkhir());
        BaseResponseDto baseResponseDto = new BaseResponseDto();
        baseResponseDto.setResponseCode("200");
        baseResponseDto.setResponseMessage("Success");
        baseResponseDto.setData(bobotDto);

        return ResponseEntity.ok().body(baseResponseDto);

    }

//    @GetMapping("/studiesActivity/assesment/uploadnilai")
//    public String upload(Model model, @RequestParam Jadwal jadwal) {
//
//        List<BobotTugas> listTugas = bobotTugasDao.findByJadwalAndStatus(jadwal, StatusRecord.AKTIF);
//        model.addAttribute("jadwal", jadwal);
//        model.addAttribute("listTugas", listTugas);
//
//        if (jadwal.getFinalStatus().equals("FINAL")) {
//            return "redirect:weight?jadwal=" + jadwal.getId();
//
//        } else {
//            return "studiesActivity/assesment/uploadnilai";
//        }
//    }

    @GetMapping("/studiesActivity/assesment/sds")
    public void sds(Model model, @RequestParam Jadwal jadwal) {
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("mahasiswa",
                jadwalDao.cariMahasiswaBelumSds(jadwal.getProdi(), jadwal.getTahunAkademik()));
    }

    @PostMapping("/studiesActivity/assesment/sds")
    public String sds(@RequestParam Jadwal jadwal, @RequestParam(required = false) String[] data) {
        if (data != null) {
            for (String krs : data) {
                Krs k = krsDao.findById(krs).get();
                KrsDetail kd = new KrsDetail();
                kd.setJadwal(jadwal);
                kd.setKrs(k);
                kd.setMahasiswa(k.getMahasiswa());
                kd.setMatakuliahKurikulum(jadwal.getMatakuliahKurikulum());
                kd.setNilaiPresensi(BigDecimal.ZERO);
                kd.setNilaiTugas(BigDecimal.ZERO);
                kd.setFinalisasi("N");
                kd.setNilaiUas(BigDecimal.ZERO);
                kd.setNilaiUts(BigDecimal.ZERO);
                kd.setJumlahMangkir(0);
                kd.setJumlahKehadiran(0);
                kd.setTahunAkademik(jadwal.getTahunAkademik());
                kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                kd.setJumlahTerlambat(0);
                kd.setJumlahIzin(0);
                kd.setJumlahSakit(0);
                kd.setStatusEdom(StatusRecord.UNDONE);
                krsDetailDao.save(kd);
            }
        }

        return "redirect:list?tahunAkademik=" + jadwal.getTahunAkademik().getId() + "&prodi="
                + jadwal.getProdi().getId();
    }

    // TUGAS
    @GetMapping("/studiesActivity/assesment/excelTugas")
    public void downloadExcelTugas(@RequestParam(required = false) String id, HttpServletResponse response)
            throws IOException {

        List<String> staticColumn1 = new ArrayList<>();

        staticColumn1.add("No.   ");
        staticColumn1.add("NIM    ");
        staticColumn1.add("NAMA                                 ");
        staticColumn1.add("NILAI ");

        BobotTugas tugas = bobotTugasDao.findById(id).get();
        List<KrsDetail> krsDetail = krsDetailDao.findByJadwalAndStatusOrderByMahasiswaNimAsc(tugas.getJadwal(),
                StatusRecord.AKTIF);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("penilaian");
        sheet.autoSizeColumn(9);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setBorderTop(BorderStyle.THIN);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);
        headerCellStyle.setBorderLeft(BorderStyle.THIN);
        headerCellStyle.setBorderRight(BorderStyle.THIN);

        sheet.createRow(0).createCell(3).setCellValue("   SCORE RECAPITULATION FIRST SEMESTER");
        sheet.createRow(1).createCell(3).setCellValue("                        INSTITUT TAZKIA");
        sheet.createRow(2).createCell(3).setCellValue("                 ACADEMIC YEAR 2019/2020");

        int rowInfo = 5;
        Row rowi1 = sheet.createRow(rowInfo);
        rowi1.createCell(2).setCellValue("Subject :");
        rowi1.createCell(3)
                .setCellValue(tugas.getJadwal().getMatakuliahKurikulum().getMatakuliah()
                        .getNamaMatakuliah());
        rowi1.createCell(4).setCellValue("Course :");
        rowi1.createCell(5)
                .setCellValue(tugas.getJadwal().getMatakuliahKurikulum().getMatakuliah()
                        .getKodeMatakuliah());

        int rowInfo2 = 6;
        Row rowi2 = sheet.createRow(rowInfo2);
        rowi2.createCell(2).setCellValue("Day/date :");
        rowi2.createCell(3).setCellValue(tugas.getJadwal().getHari().getNamaHari());
        rowi2.createCell(4).setCellValue("Semester :");
        rowi2.createCell(5).setCellValue(tugas.getJadwal().getMatakuliahKurikulum().getSemester().toString());

        int rowInfo3 = 7;
        Row rowi3 = sheet.createRow(rowInfo3);
        rowi3.createCell(2).setCellValue("Room No/Time :");
        rowi3.createCell(3).setCellValue(tugas.getJadwal().getRuangan().getNamaRuangan());
        rowi3.createCell(4).setCellValue("Lecturer :");
        rowi3.createCell(5).setCellValue(tugas.getJadwal().getDosen().getKaryawan().getNamaKaryawan());

        int rowInfo4 = 8;
        Row rowi4 = sheet.createRow(rowInfo4);
        rowi4.createCell(2).setCellValue("Komponen :");
        rowi4.createCell(3).setCellValue("Tugas " + tugas.getNamaTugas());
        rowi4.createCell(4).setCellValue("Bobot :");
        rowi4.createCell(5).setCellValue(tugas.getBobot().toString());

        /**/
        Row headerRow = sheet.createRow(11);
        Integer cellNum = 2;

        for (String header : staticColumn1) {
            Cell cell = headerRow.createCell(cellNum);
            cell.setCellValue(header);
            cell.setCellStyle(headerCellStyle);
            sheet.autoSizeColumn(cellNum);
            cellNum++;
        }

        int rowNum = 12;
        int no = 1;
        for (KrsDetail kd : krsDetail) {
            int kolom = 2;
            Row row = sheet.createRow(rowNum);
            row.createCell(kolom++).setCellValue(no);
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNim());
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNama());
            no++;
            rowNum++;
        }

        String namaFile = "PenilaianTugas ";
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + namaFile
                        + tugas.getJadwal().getMatakuliahKurikulum().getMatakuliah()
                        .getNamaMatakuliah()
                        + "_"
                        + tugas.getJadwal().getKelas().getNamaKelas() + "_"
                        + tugas.getNamaTugas() + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    // UTS
    @GetMapping("/studiesActivity/assesment/excelUTS")
    public void downloadExcelUts(@RequestParam(required = false) String id, HttpServletResponse response)
            throws IOException {

        List<String> staticColumn1 = new ArrayList<>();

        staticColumn1.add("No.   ");
        staticColumn1.add("NIM    ");
        staticColumn1.add("NAMA                                 ");
        staticColumn1.add("NILAI ");

        Jadwal jadwal = jadwalDao.findById(id).get();
        List<KrsDetail> krsDetail = krsDetailDao.findByJadwalAndStatusOrderByMahasiswaNimAsc(jadwal,
                StatusRecord.AKTIF);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("penilaian");
        sheet.autoSizeColumn(9);

        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setBorderTop(BorderStyle.THIN);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);
        headerCellStyle.setBorderLeft(BorderStyle.THIN);
        headerCellStyle.setBorderRight(BorderStyle.THIN);

        sheet.createRow(0).createCell(3).setCellValue("   SCORE RECAPITULATION FIRST SEMESTER");
        sheet.createRow(1).createCell(3).setCellValue("                        INSTITUT TAZKIA");
        sheet.createRow(2).createCell(3).setCellValue("                 ACADEMIC YEAR 2019/2020");

        int rowInfo = 5;
        Row rowi1 = sheet.createRow(rowInfo);
        rowi1.createCell(2).setCellValue("Subject :");
        rowi1.createCell(3).setCellValue(jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah());
        rowi1.createCell(4).setCellValue("Course :");
        rowi1.createCell(5).setCellValue(jadwal.getMatakuliahKurikulum().getMatakuliah().getKodeMatakuliah());

        int rowInfo2 = 6;
        Row rowi2 = sheet.createRow(rowInfo2);
        rowi2.createCell(2).setCellValue("Day/date :");
        rowi2.createCell(3).setCellValue(jadwal.getHari().getNamaHari());
        rowi2.createCell(4).setCellValue("Semester :");
        rowi2.createCell(5).setCellValue(jadwal.getMatakuliahKurikulum().getSemester().toString());

        int rowInfo3 = 7;
        Row rowi3 = sheet.createRow(rowInfo3);
        rowi3.createCell(2).setCellValue("Room No/Time :");
        rowi3.createCell(3).setCellValue(jadwal.getRuangan().getNamaRuangan());
        rowi3.createCell(4).setCellValue("Lecturer :");
        rowi3.createCell(5).setCellValue(jadwal.getDosen().getKaryawan().getNamaKaryawan());

        int rowInfo4 = 8;
        Row rowi4 = sheet.createRow(rowInfo4);
        rowi4.createCell(2).setCellValue("Komponen :");
        rowi4.createCell(3).setCellValue("UTS");
        rowi4.createCell(4).setCellValue("Bobot :");
        rowi4.createCell(5).setCellValue(jadwal.getBobotUts().toString());

        /**/
        Row headerRow = sheet.createRow(11);
        Integer cellNum = 2;

        for (String header : staticColumn1) {
            Cell cell = headerRow.createCell(cellNum);
            cell.setCellValue(header);
            sheet.autoSizeColumn(cellNum);
            cell.setCellStyle(headerCellStyle);
            cellNum++;
        }

        int rowNum = 12;
        int no = 1;
        for (KrsDetail kd : krsDetail) {
            int kolom = 2;
            Row row = sheet.createRow(rowNum);
            row.createCell(kolom++).setCellValue(no);
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNim());
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNama());
            no++;
            rowNum++;
        }

        String namaFile = "PenilaianUTS ";
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + namaFile
                        + jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah()
                        + "_"
                        + jadwal.getKelas().getNamaKelas() + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    // UAS
    @GetMapping("/studiesActivity/assesment/excelUAS")
    public void downloadExcelUas(@RequestParam(required = false) String id, HttpServletResponse response)
            throws IOException {

        List<String> staticColumn1 = new ArrayList<>();

        staticColumn1.add("No.   ");
        staticColumn1.add("NIM    ");
        staticColumn1.add("NAMA                                 ");
        staticColumn1.add("NILAI ");

        Jadwal jadwal = jadwalDao.findById(id).get();
        List<KrsDetail> krsDetail = krsDetailDao.findByJadwalAndStatusOrderByMahasiswaNimAsc(jadwal,
                StatusRecord.AKTIF);
        List<BobotTugas> bobotTugas = bobotTugasDao.findByJadwalAndStatus(jadwal, StatusRecord.AKTIF);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("penilaian");
        sheet.autoSizeColumn(9);

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setBorderTop(BorderStyle.THIN);
        headerCellStyle.setBorderBottom(BorderStyle.THIN);
        headerCellStyle.setBorderLeft(BorderStyle.THIN);
        headerCellStyle.setBorderRight(BorderStyle.THIN);

        sheet.createRow(0).createCell(3).setCellValue("   SCORE RECAPITULATION FIRST SEMESTER");
        sheet.createRow(1).createCell(3).setCellValue("                        INSTITUT TAZKIA");
        sheet.createRow(2).createCell(3).setCellValue("                 ACADEMIC YEAR 2019/2020");

        int rowInfo = 5;
        Row rowi1 = sheet.createRow(rowInfo);
        rowi1.createCell(2).setCellValue("Subject :");
        rowi1.createCell(3).setCellValue(jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah());
        rowi1.createCell(4).setCellValue("Course :");
        rowi1.createCell(5).setCellValue(jadwal.getMatakuliahKurikulum().getMatakuliah().getKodeMatakuliah());

        int rowInfo2 = 6;
        Row rowi2 = sheet.createRow(rowInfo2);
        rowi2.createCell(2).setCellValue("Day/date :");
        rowi2.createCell(3).setCellValue(jadwal.getHari().getNamaHari());
        rowi2.createCell(4).setCellValue("Semester :");
        rowi2.createCell(5).setCellValue(jadwal.getMatakuliahKurikulum().getSemester().toString());

        int rowInfo3 = 7;
        Row rowi3 = sheet.createRow(rowInfo3);
        rowi3.createCell(2).setCellValue("Room No/Time :");
        rowi3.createCell(3).setCellValue(jadwal.getRuangan().getNamaRuangan());
        rowi3.createCell(4).setCellValue("Lecturer :");
        rowi3.createCell(5).setCellValue(jadwal.getDosen().getKaryawan().getNamaKaryawan());

        int rowInfo4 = 8;
        Row rowi4 = sheet.createRow(rowInfo4);
        rowi4.createCell(2).setCellValue("Komponen :");
        rowi4.createCell(3).setCellValue("UAS");
        rowi4.createCell(4).setCellValue("Bobot :");
        rowi4.createCell(5).setCellValue(jadwal.getBobotUas().toString());

        /**/
        Row headerRow = sheet.createRow(11);
        Integer cellNum = 2;

        for (String header : staticColumn1) {
            Cell cell = headerRow.createCell(cellNum);
            cell.setCellStyle(headerCellStyle);
            cell.setCellValue(header);
            sheet.autoSizeColumn(cellNum);
            cellNum++;
        }

        int rowNum = 12;
        int no = 1;
        for (KrsDetail kd : krsDetail) {
            int kolom = 2;
            Row row = sheet.createRow(rowNum);
            row.createCell(kolom++).setCellValue(no);
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNim());
            row.createCell(kolom++).setCellValue(kd.getMahasiswa().getNama());
            no++;
            rowNum++;
        }

        String namaFile = "PenilaianUAS ";
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment; filename=\"" + namaFile
                        + jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah()
                        + "_"
                        + jadwal.getKelas().getNamaKelas() + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    // post.excel.Tugas
    @PostMapping("/studiesActivity/assesment/formTugas")
    public String prosesFormUploadTugas(MultipartFile file,
                                        @RequestParam(name = "jadwal", value = "jadwal") BobotTugas bobotTugas) {

        LOGGER.debug("Nama file : {}", file.getOriginalFilename());
        LOGGER.debug("Ukuran file : {} bytes", file.getSize());

        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheetPertama = workbook.getSheetAt(0);

            int row = 12;
            Long jmlMhs = krsDetailDao.countByJadwalAndStatus(bobotTugas.getJadwal(), StatusRecord.AKTIF);
            for (int i = 0; i < jmlMhs; i++) {
                Row baris = sheetPertama.getRow(row + i);

                String nim = baris.getCell(3).getStringCellValue();
                Cell nilai = baris.getCell(5);

                if (nilai != null) {
                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);

                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            bobotTugas.getJadwal(), StatusRecord.AKTIF);
                    NilaiTugas nilaiTugas = nilaiTugasDao.findByStatusAndBobotTugasAndKrsDetail(
                            StatusRecord.AKTIF,
                            bobotTugas,
                            krsDetail);
                    BigDecimal nilaiAkhir = new BigDecimal(nilai.getNumericCellValue())
                            .multiply(bobotTugas.getBobot())
                            .divide(new BigDecimal(100));

                    nilaiTugas.setNilai(new BigDecimal(nilai.getNumericCellValue()));
                    nilaiTugas.setNilaiAkhir(nilaiAkhir);

                    nilaiTugasDao.save(nilaiTugas);
                    List<NilaiTugas> nilaiAkhirnya = nilaiTugasDao
                            .findByStatusAndKrsDetailAndBobotTugasStatus(
                                    StatusRecord.AKTIF, krsDetail,
                                    StatusRecord.AKTIF);

                    /* sum semua tugas */
                    BigDecimal nilTug = nilaiAkhirnya.stream()
                            .map(NilaiTugas::getNilaiAkhir)
                            .reduce(BigDecimal.ZERO, BigDecimal::add);
                    krsDetail.setNilaiTugas(nilTug);
                    krsDetailDao.save(krsDetail);
                } else {
                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);

                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            bobotTugas.getJadwal(), StatusRecord.AKTIF);
                    NilaiTugas nilaiTugas = nilaiTugasDao.findByStatusAndBobotTugasAndKrsDetail(
                            StatusRecord.AKTIF,
                            bobotTugas,
                            krsDetail);

                    nilaiTugas.setNilai(BigDecimal.ZERO);
                    nilaiTugas.setNilaiAkhir(BigDecimal.ZERO);

                    nilaiTugasDao.save(nilaiTugas);

                }

            }

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        List<ScoreHitungDto> scoreDtos = jadwalDao.hitungUploadScore(bobotTugas.getJadwal(),
                bobotTugas.getJadwal().getTahunAkademik());

        for (ScoreHitungDto s : scoreDtos) {

            KrsDetail krsDetail2 = krsDetailDao.findById(s.getKrs()).get();
            krsDetail2.setNilaiAkhir(s.getNilaiAkhir());
            krsDetail2.setGrade(s.getGrade());
            krsDetail2.setBobot(s.getBobot());

            krsDetailDao.save(krsDetail2);

        }

        return "redirect:/studiesActivity/assesment/uploadnilai?jadwal=" + bobotTugas.getJadwal().getId();

    }

    // post.excel.UTS
    @PostMapping("/studiesActivity/assesment/formUTS")
    public String prosesFormUploadUTS(MultipartFile file, @RequestParam Jadwal jadwal) {

        LOGGER.debug("Nama file : {}", file.getOriginalFilename());
        LOGGER.debug("Ukuran file : {} bytes", file.getSize());

        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheetPertama = workbook.getSheetAt(0);

            int row = 12;
            Long jmlMhs = krsDetailDao.countByJadwalAndStatus(jadwal, StatusRecord.AKTIF);
            for (int i = 0; i < jmlMhs; i++) {
                Row baris = sheetPertama.getRow(row + i);

                String nim = baris.getCell(3).getStringCellValue();
                Cell nilai = baris.getCell(5);

                if (nilai != null) {
                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            jadwal, StatusRecord.AKTIF);

                    if (krsDetail == null) {
                        LOGGER.warn("KRS Detail untuk nim {} dan UTS {} tidak ditemukan", nim,
                                jadwal.getStatusUts());
                        return "redirect:/penilaian/list";
                    }

                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);

                    krsDetail.setNilaiUts(new BigDecimal(nilai.getNumericCellValue()));
                    krsDetailDao.save(krsDetail);

                } else {
                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            jadwal, StatusRecord.AKTIF);
                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);
                    krsDetail.setNilaiUts(BigDecimal.ZERO);
                    krsDetailDao.save(krsDetail);

                    if (krsDetail == null) {
                        LOGGER.warn("KRS Detail untuk nim {} dan UTS {} tidak ditemukan", nim,
                                jadwal.getStatusUts());
                        return "redirect:/penilaian/list";
                    }

                }

            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        List<ScoreHitungDto> scoreDtos = jadwalDao.hitungUploadScore(jadwal, jadwal.getTahunAkademik());

        for (ScoreHitungDto s : scoreDtos) {

            KrsDetail krsDetail2 = krsDetailDao.findById(s.getKrs()).get();
            krsDetail2.setNilaiAkhir(s.getNilaiAkhir());
            krsDetail2.setGrade(s.getGrade());
            krsDetail2.setBobot(s.getBobot());

            krsDetailDao.save(krsDetail2);

        }

        return "redirect:/studiesActivity/assesment/uploadnilai?jadwal=" + jadwal.getId();

    }

    // post.excel.UAS
    @PostMapping("/studiesActivity/assesment/formUAS")
    public String prosesFormUploadUAS(MultipartFile file, @RequestParam Jadwal jadwal) {

        LOGGER.debug("Nama file : {}", file.getOriginalFilename());
        LOGGER.debug("Ukuran file : {} bytes", file.getSize());

        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheetPertama = workbook.getSheetAt(0);

            int row = 12;
            Long jmlMhs = krsDetailDao.countByJadwalAndStatus(jadwal, StatusRecord.AKTIF);
            for (int i = 0; i < jmlMhs; i++) {
                Row baris = sheetPertama.getRow(row + i);

                Cell nilai = baris.getCell(5);
                String nim = baris.getCell(3).getStringCellValue();

                if (nilai != null) {
                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);

                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            jadwal, StatusRecord.AKTIF);

                    if (krsDetail == null) {
                        LOGGER.warn("KRS Detail untuk nim {} dan UAS {} tidak ditemukan", nim,
                                jadwal.getStatusUas());
                        return "redirect:/penilaian/list";
                    }

                    krsDetail.setNilaiUas(new BigDecimal(nilai.getNumericCellValue()));
                    krsDetailDao.save(krsDetail);

                } else {
                    LOGGER.info("NIM : {}, Nilai : {}", nim, nilai);

                    KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndJadwalAndStatus(
                            mahasiswaDao.findByNim(nim),
                            jadwal, StatusRecord.AKTIF);
                    krsDetail.setNilaiUas(BigDecimal.ZERO);
                    krsDetailDao.save(krsDetail);

                    if (krsDetail == null) {
                        LOGGER.warn("KRS Detail untuk nim {} dan UAS {} tidak ditemukan", nim,
                                jadwal.getStatusUas());
                        return "redirect:/penilaian/list";
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        List<ScoreHitungDto> scoreDtos = jadwalDao.hitungUploadScore(jadwal, jadwal.getTahunAkademik());

        for (ScoreHitungDto s : scoreDtos) {

            KrsDetail krsDetail2 = krsDetailDao.findById(s.getKrs()).get();
            krsDetail2.setNilaiAkhir(s.getNilaiAkhir());
            krsDetail2.setGrade(s.getGrade());
            krsDetail2.setBobot(s.getBobot());

            krsDetailDao.save(krsDetail2);

        }

        return "redirect:/studiesActivity/assesment/uploadnilai?jadwal=" + jadwal.getId();

    }

    // KHS

    @GetMapping("/studiesActivity/khs/list")
    public void listKhs(Model model, @RequestParam(required = false) TahunAkademik tahunAkademik,
                        @RequestParam(required = false) String nim) {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        model.addAttribute("tahun",
                tahunAkademikDao.findByStatusNotInOrderByTahunDesc(Arrays.asList(StatusRecord.HAPUS)));
        if (mahasiswa != null) {
            if (tahunAkademik != null) {
                model.addAttribute("selectedTahun", tahunAkademik);
                model.addAttribute("selectedNim", nim);
                List<DataKhsDto> krsDetail = krsDetailDao.getKhs(tahunAkademik, mahasiswa);
                if (!krsDetail.isEmpty()) {
                    model.addAttribute("khs", krsDetail);
                    model.addAttribute("ipk",
                            krsDetailDao.ipkTahunAkademik(mahasiswa,
                                    tahunAkademik.getKodeTahunAkademik()));
                    model.addAttribute("ip", krsDetailDao.ip(mahasiswa, tahunAkademik));
                }
            } else {
                model.addAttribute("selectedNim", nim);
                List<DataKhsDto> krsDetail = krsDetailDao.getKhs(
                        tahunAkademikDao.findByStatus(StatusRecord.AKTIF),
                        mahasiswa);
                model.addAttribute("khs", krsDetail);
                model.addAttribute("ipk", krsDetailDao.ipkTahunAkademik(mahasiswa,
                        tahunAkademikDao.findByStatus(StatusRecord.AKTIF)
                                .getKodeTahunAkademik()));
                model.addAttribute("ip", krsDetailDao.ip(mahasiswa,
                        tahunAkademikDao.findByStatus(StatusRecord.AKTIF)));
            }
        }
    }

    @GetMapping("/studiesActivity/khs/download")
    public void downloadKhs(Model model, @RequestParam(required = false) TahunAkademik tahunAkademik,
                            @RequestParam(required = false) String nim) {

        if (tahunAkademik != null) {
            Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
            model.addAttribute("selectedTahun", tahunAkademik);
            model.addAttribute("selectedNim", nim);
            List<DataKhsDto> krsDetail = krsDetailDao.getKhs(tahunAkademik, mahasiswa);
            int sumSks = krsDetail.stream().mapToInt(DataKhsDto::getSks).sum();
            Double sumBobot = krsDetail.stream().mapToDouble(DataKhsDto::getTotal).sum();
            model.addAttribute("khs", krsDetail);
            model.addAttribute("sks", sumSks);
            model.addAttribute("bobot", sumBobot);
            model.addAttribute("mahasiswa", mahasiswa);
            model.addAttribute("tahun", tahunAkademik);
            model.addAttribute("ipk",
                    krsDetailDao.ipkTahunAkademik(mahasiswa, tahunAkademik.getKodeTahunAkademik()));
            model.addAttribute("ip", krsDetailDao.ip(mahasiswa, tahunAkademik));
        }

    }

    @GetMapping("/studiesActivity/khs/downloadexcel")
    public void khsExcel(Model model, @RequestParam(required = false) TahunAkademik tahunAkademik,
                         @RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException, URISyntaxException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

        List<DataKhsDto> krsDetail = krsDetailDao.getKhs(tahunAkademik, mahasiswa);

        int sumSks = krsDetail.stream().mapToInt(DataKhsDto::getSks).sum();
        IpkDto ipk = krsDetailDao.ipkTahunAkademik(mahasiswa, tahunAkademik.getKodeTahunAkademik());
        IpkDto ip = krsDetailDao.ip(mahasiswa, tahunAkademik);

        InputStream file = contohExcelKhs.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:B8"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A9:B9"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A10:B10"));

        Font manajemenFont = workbook.createFont();
        manajemenFont.setBold(true);
        manajemenFont.setFontHeightInPoints((short) 12);

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 12);

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 14);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 12);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 12);
        dataFont.setFontName("Cambria");

        Font prodiFont = workbook.createFont();
        prodiFont.setBold(true);
        prodiFont.setFontHeightInPoints((short) 12);
        prodiFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setFontHeightInPoints((short) 12);
        ipFont.setFontName("Cambria");

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setFont(manajemenFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.CENTER);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setBorderTop(BorderStyle.THIN);
        styleDataKhs.setBorderBottom(BorderStyle.THIN);
        styleDataKhs.setBorderLeft(BorderStyle.THIN);
        styleDataKhs.setBorderRight(BorderStyle.THIN);
        styleDataKhs.setFont(dataFont);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.CENTER);
        styleSymbol.setFont(symbolFont);

        CellStyle styleIp = workbook.createCellStyle();
        styleIp.setAlignment(HorizontalAlignment.RIGHT);
        styleIp.setVerticalAlignment(VerticalAlignment.CENTER);
        styleIp.setFont(ipFont);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);

        int rowInfoTahun = 2;
        Row rowTahun = sheet.createRow(rowInfoTahun);
        rowTahun.createCell(0).setCellValue(tahunAkademik.getNamaTahunAkademik());
        rowTahun.getCell(0).setCellStyle(styleSubHeader);

        int rowInfoNama = 7;
        Row rowNama = sheet.createRow(rowInfoNama);
        rowNama.createCell(0).setCellValue("Nama");
        rowNama.createCell(2).setCellValue(":");
        rowNama.createCell(3).setCellValue(mahasiswa.getNama());
        rowNama.getCell(0).setCellStyle(styleData);
        rowNama.getCell(2).setCellStyle(styleSymbol);
        rowNama.getCell(3).setCellStyle(styleData);

        int rowInfoNim = 8;
        Row rowNim = sheet.createRow(rowInfoNim);
        rowNim.createCell(0).setCellValue("NIM");
        rowNim.createCell(2).setCellValue(":");
        rowNim.createCell(3).setCellValue(mahasiswa.getNim());
        rowNim.getCell(0).setCellStyle(styleData);
        rowNim.getCell(2).setCellStyle(styleSymbol);
        rowNim.getCell(3).setCellStyle(styleData);

        int rowInfo3 = 9;
        Row rowi3 = sheet.createRow(rowInfo3);
        rowi3.createCell(0).setCellValue("Program Studi");
        rowi3.createCell(2).setCellValue(":");
        rowi3.createCell(3).setCellValue(mahasiswa.getIdProdi().getNamaProdi());
        rowi3.getCell(0).setCellStyle(styleData);
        rowi3.getCell(2).setCellStyle(styleSymbol);
        rowi3.getCell(3).setCellStyle(styleData);

        int rowNum = 13;
        int no = 1;
        for (DataKhsDto kd : krsDetail) {
            Row row = sheet.createRow(rowNum);
            sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 2, 3));

            row.createCell(0).setCellValue(no);
            row.getCell(0).setCellStyle(styleDataKhs);
            row.createCell(1).setCellValue(kd.getKode());
            row.getCell(1).setCellStyle(styleDataKhs);
            row.createCell(2).setCellValue(kd.getMatakuliah());
            row.getCell(2).setCellStyle(styleDataKhs);
            row.createCell(4).setCellValue(kd.getSks());
            row.getCell(4).setCellStyle(styleDataKhs);
            if (kd.getMatakuliah().equals("Student Dynamic Session")) {
                row.createCell(5).setCellValue("-");
                row.getCell(5).setCellStyle(styleDataKhs);
                row.createCell(6).setCellValue("-");
                row.getCell(6).setCellStyle(styleDataKhs);
                row.createCell(7).setCellValue("-");
                row.getCell(7).setCellStyle(styleDataKhs);
            } else {
                row.createCell(5).setCellValue(kd.getBobot().toString());
                row.getCell(5).setCellStyle(styleDataKhs);
                row.createCell(6).setCellValue(kd.getGrade());
                row.getCell(6).setCellStyle(styleDataKhs);
                row.createCell(7).setCellValue(
                        kd.getBobot().multiply(BigDecimal.valueOf(kd.getSks())).toString());
                row.getCell(7).setCellStyle(styleDataKhs);
            }
            no++;
            rowNum++;
        }

        int rowTotalSks = 13 + krsDetail.size();
        Row totalSks = sheet.createRow(rowTotalSks);
        totalSks.createCell(3).setCellValue("Jumlah SKS  :");
        totalSks.createCell(4).setCellValue(sumSks);
        totalSks.createCell(5).setCellValue("IP Semester  :");
        totalSks.createCell(7).setCellValue(ip.getIpk().toString());
        totalSks.getCell(3).setCellStyle(styleIp);
        totalSks.getCell(5).setCellStyle(styleIp);
        totalSks.getCell(4).setCellStyle(styleIpk);
        totalSks.getCell(7).setCellStyle(styleIpk);
        sheet.addMergedRegion(new CellRangeAddress(rowTotalSks, rowTotalSks, 5, 6));

        int rowTotalIpk = 13 + krsDetail.size() + 1;
        Row totalIpk = sheet.createRow(rowTotalIpk);
        totalIpk.createCell(5).setCellValue("IPK   :");
        totalIpk.createCell(7).setCellValue(ipk.getIpk().toString());
        totalIpk.getCell(5).setCellStyle(styleIp);
        totalIpk.getCell(7).setCellStyle(styleIpk);
        sheet.addMergedRegion(new CellRangeAddress(rowTotalIpk, rowTotalIpk, 5, 6));

        int rowKoor = 13 + krsDetail.size() + 5;
        Row koor = sheet.createRow(rowKoor);
        koor.createCell(4).setCellValue("Koordinator Program Studi ");
        koor.getCell(4).setCellStyle(styleManajemen);

        int rowProdi = 13 + krsDetail.size() + 6;
        Row prodi = sheet.createRow(rowProdi);
        prodi.createCell(4).setCellValue(mahasiswa.getIdProdi().getNamaProdi());
        prodi.getCell(4).setCellStyle(styleProdi);

        int rowDosen = 13 + krsDetail.size() + 11;
        Row namaDosen = sheet.createRow(rowDosen);
        namaDosen.createCell(4).setCellValue(mahasiswa.getIdProdi().getDosen().getKaryawan().getNamaKaryawan());
        namaDosen.getCell(4).setCellStyle(styleManajemen);

        String namaFile = "KHS-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    // Edom

    @GetMapping("/studiesActivity/assesment/hasiledom")
    public void hasilEdom(Model model, @RequestParam Jadwal jadwal, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        Dosen dosen = dosenDao.findByKaryawan(karyawan);

        List<Object[]> headerEdom = edomMahasiswaDao.headerEdomMahasiswaPerDosen(jadwal, dosen);
        model.addAttribute("headerEdom", headerEdom);
        if (headerEdom != null) {
            List<Object[]> detailEdom = edomQuestionDao.detailEdomPerDosen(jadwal, dosen);
            model.addAttribute("detailEdom", detailEdom);
        } else {
            model.addAttribute("questionNull", "Pertanyaan Edom Belum Dibuat");
        }
        model.addAttribute("jadwal", jadwal);

        // model.addAttribute("edom", krsDetailDao.edomJadwal(jadwal));
        //
        // EdomQuestion edomQuestion1 =
        // edomQuestionDao.findByStatusAndNomorAndTahunAkademik(StatusRecord.AKTIF,1,jadwal.getTahunAkademik());
        // EdomQuestion edomQuestion2 =
        // edomQuestionDao.findByStatusAndNomorAndTahunAkademik(StatusRecord.AKTIF,2,jadwal.getTahunAkademik());
        // EdomQuestion edomQuestion3 =
        // edomQuestionDao.findByStatusAndNomorAndTahunAkademik(StatusRecord.AKTIF,3,jadwal.getTahunAkademik());
        // EdomQuestion edomQuestion4 =
        // edomQuestionDao.findByStatusAndNomorAndTahunAkademik(StatusRecord.AKTIF,4,jadwal.getTahunAkademik());
        // EdomQuestion edomQuestion5 =
        // edomQuestionDao.findByStatusAndNomorAndTahunAkademik(StatusRecord.AKTIF,5,jadwal.getTahunAkademik());
        // model.addAttribute("edomQuestion1",edomQuestion1);
        // model.addAttribute("edomQuestion2",edomQuestion2);
        // model.addAttribute("edomQuestion3",edomQuestion3);
        // model.addAttribute("edomQuestion4",edomQuestion4);
        // model.addAttribute("edomQuestion5",edomQuestion5);

    }

    // BKD

    @GetMapping("/studiesActivity/assesment/topic")
    public void topic(Model model, @RequestParam Jadwal jadwal) {
        String tahun = jadwal.getTahunAkademik().getNamaTahunAkademik().substring(0, 9);

        model.addAttribute("tahun", tahun);
        model.addAttribute("topic", presensiDosenDao.bkdBeritaAcara(jadwal));
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("dosen", jadwalDosenDao.headerJadwal(jadwal.getId()));

    }

    @GetMapping("/studiesActivity/assesment/nilai")
    public void nilai(Model model, @RequestParam Jadwal jadwal) {
        String tahun = jadwal.getTahunAkademik().getNamaTahunAkademik().substring(0, 9);

        model.addAttribute("tahun", tahun);
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("dosen", jadwalDosenDao.headerJadwal(jadwal.getId()));
        model.addAttribute("nilai", presensiMahasiswaDao.bkdNilai(jadwal));
    }

    @GetMapping("/studiesActivity/assesment/attendance")
    public void attendance(Model model, @RequestParam Jadwal jadwal) {
        String tahun = jadwal.getTahunAkademik().getNamaTahunAkademik().substring(0, 9);

        model.addAttribute("tahun", tahun);

        // JadwalDosenDto jadwalDosenDto = (JadwalDosenDto)
        // jadwalDosenDao.headerJadwal(jadwal.getId());

        model.addAttribute("jadwal", jadwal);

        model.addAttribute("dosen", jadwalDosenDao.headerJadwal(jadwal.getId()));

        List<Object[]> hasil = presensiMahasiswaDao.bkdAttendance(jadwal);
        log.debug("BKD Attendance : {}", hasil.size());
        //
        // hasil = presensiMahasiswaDao.bkdAttendance(jadwal);
        // log.debug("BKD Attendance : {}", hasil.size());
        //
        // hasil = presensiMahasiswaDao.bkdAttendance(jadwal);
        // log.debug("BKD Attendance : {}", hasil.size());

        model.addAttribute("attendance", hasil);

    }

    @GetMapping("/studiesActivity/assesment/report")
    public void downloadReport(@RequestParam Jadwal jadwal, HttpServletResponse response) throws IOException {

        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Presensi - Berita Acara - Nilai "
                + jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah() + " - "
                + jadwal.getKelas().getNamaKelas() + ".pdf";

        response.setHeader(headerKey, headerValue);

        reportService.downloadReport(jadwal, response);

    }

    @GetMapping("/studiesActivity/transcript/list")
    public void checkTranscript(Model model, @RequestParam(required = false) String nim) {

        if (nim != null) {
            model.addAttribute("nim", nim);
            Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
            if (mahasiswa != null) {
                model.addAttribute("mhsw", mahasiswa);

                // tampilsemua
                model.addAttribute("transkrip", krsDetailDao.transkrip(mahasiswa));
                model.addAttribute("sks", krsDetailDao.totalSks(mahasiswa));
                model.addAttribute("mutu", krsDetailDao.totalMutu(mahasiswa));
                model.addAttribute("semesterTranskript", krsDao.semesterTranskript(mahasiswa.getId()));
                model.addAttribute("transkriptTampil",
                        krsDetailDao.transkriptTampil(mahasiswa.getId()));
            } else {
                model.addAttribute("message", "error message");
            }

        }

    }

    public static String getPattern(int month) {
        String first = "MMMM d";
        String last = " yyyy";
        String pos = (month == 1 || month == 21 || month == 31) ? "'st'"
                : (month == 2 || month == 22) ? "'nd'" : (month == 3 || month == 23) ? "'rd'" : "'th'";
        return first + pos + last;
    }

    @GetMapping("/studiesActivity/transcript/cetaktranscript")
    public void cetakTranscript(Model model, @RequestParam(required = false) String nim,
                                @RequestParam(required = false) String nirm) {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

        if (StringUtils.hasText(nirm)) {
            model.addAttribute("nirm", nirm);

        }
        if (StringUtils.hasText(nim)) {
            model.addAttribute("search", nim);

        }
        if (mahasiswa != null) {
            Mahasiswa cekTanggalDisahkan = mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan());
            if (cekTanggalDisahkan != null) {
                model.addAttribute("disahkan", cekTanggalDisahkan.getTanggalDisahkan());
            }

            model.addAttribute("mahasiswa", mahasiswa);

        }
    }

    @PostMapping("/studiesActivity/transcript/cetaktranscript")
    public String updateTranscript(@Valid @ModelAttribute Mahasiswa mahasiswa,
                                   @RequestParam(required = false) String tanggal, @RequestParam(required = false) String nirm) {

        if (nirm != null) {
            mahasiswaDao.save(mahasiswa);
            return "redirect:cetaktranscript?nim=" + mahasiswa.getNim() + "&nirm=AKTIF";
        } else {
            LocalDate tanggalDisahkann = LocalDate.parse(tanggal);
            Mahasiswa cekTanggalDisahkan = mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan());
            if (cekTanggalDisahkan == null) {
                mahasiswa.setTanggalDisahkan(tanggalDisahkann);
            } else {
                cekTanggalDisahkan.setTanggalDisahkan(tanggalDisahkann);
            }

            mahasiswaDao.save(mahasiswa);
            return "redirect:cetaktranscript?nim=" + mahasiswa.getNim();
        }

    }

    @PostMapping("/studiesActivity/transcript/uploadpin")
    public String uploadPin(MultipartFile file, RedirectAttributes attributes) {

        List<BaseResponse> errors = new ArrayList<>();
        List<BaseResponse> success = new ArrayList<>();

        try {
            Workbook workbook = new XSSFWorkbook(file.getInputStream());
            Sheet sheetPertama = workbook.getSheetAt(0);

            int row = 1;
            int terakhir = sheetPertama.getLastRowNum() - row;

            for (int i = 0; i <= terakhir; i++) {
                Row baris = sheetPertama.getRow(row + i);

                if (baris.getCell(0) == null) {

                } else {

                    String nim = baris.getCell(0).getStringCellValue();
                    Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
                    if (mahasiswa != null) {
                        mahasiswa.setNoTranskript(baris.getCell(1).getStringCellValue());
                        mahasiswa.setIndukNasional(baris.getCell(2).getStringCellValue());
                        mahasiswaDao.save(mahasiswa);

                        BaseResponse response = new BaseResponse();
                        response.setNim(mahasiswa.getNim() + " - " + mahasiswa.getNama());
                        response.setMessage("Sukses");
                        success.add(response);

                    } else {
                        BaseResponse response = new BaseResponse();
                        response.setNim(nim);
                        response.setMessage("Mahasiswa tidak ditemukan");
                        errors.add(response);
                    }

                }
            }

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

        attributes.addFlashAttribute("errors", errors);
        attributes.addFlashAttribute("success", success);

        return "redirect:cetaktranscript";
        // attributes.addFlashAttribute("mahasiswa", mahasiswas);
    }

    @GetMapping("/studiesActivity/transcript/transkriptexcel")
    public void transkriptExcel(@RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        List<TranskriptDto> semester1 = krsDetailDao.excelTranskript(mahasiswa.getId(), "1");
        List<TranskriptDto> semester2 = krsDetailDao.excelTranskript(mahasiswa.getId(), "2");
        List<TranskriptDto> semester3 = krsDetailDao.excelTranskript(mahasiswa.getId(), "3");
        List<TranskriptDto> semester4 = krsDetailDao.excelTranskript(mahasiswa.getId(), "4");
        List<TranskriptDto> semester5 = krsDetailDao.excelTranskript(mahasiswa.getId(), "5");
        List<TranskriptDto> semester6 = krsDetailDao.excelTranskript(mahasiswa.getId(), "6");
        List<TranskriptDto> semester7 = krsDetailDao.excelTranskript(mahasiswa.getId(), "7");
        List<TranskriptDto> semester8 = krsDetailDao.excelTranskript(mahasiswa.getId(), "8");

        BigDecimal totalSKS = krsDetailDao.totalSksAkhir(mahasiswa.getId());
        BigDecimal totalMuti = krsDetailDao.totalMutuAkhir(mahasiswa.getId());

        IpkDto ipk = krsDetailDao.ipk(mahasiswa);

        // BigDecimal ipk = totalMuti.divide(totalSKS,2,BigDecimal.ROUND_HALF_DOWN);

        InputStream file = contohExcelTranskript.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());

        sheet.addMergedRegion(CellRangeAddress.valueOf("A7:C7"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:C8"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A9:C9"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A10:C10"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A11:C11"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A12:C12"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A13:C13"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A14:C14"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A15:C15"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A16:C16"));

        Font manajemenFont = workbook.createFont();
        manajemenFont.setItalic(true);
        manajemenFont.setFontHeightInPoints((short) 10);
        manajemenFont.setFontName("Cambria");

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 10);
        dataManajemenFont.setFontName("Cambria");

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 10);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 10);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        Font prodiFont = workbook.createFont();
        prodiFont.setUnderline(XSSFFont.U_DOUBLE);
        prodiFont.setFontHeightInPoints((short) 10);
        prodiFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setItalic(true);
        ipFont.setFontHeightInPoints((short) 10);
        ipFont.setFontName("Cambria");

        Font lectureFont = workbook.createFont();
        lectureFont.setBold(true);
        lectureFont.setFontName("Cambria");
        lectureFont.setUnderline(XSSFFont.U_DOUBLE);
        lectureFont.setFontHeightInPoints((short) 10);

        Font nikFont = workbook.createFont();
        nikFont.setBold(true);
        nikFont.setFontName("Cambria");
        nikFont.setFontHeightInPoints((short) 10);

        CellStyle styleNik = workbook.createCellStyle();
        styleNik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNik.setFont(nikFont);

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleManajemen.setAlignment(HorizontalAlignment.CENTER);
        styleManajemen.setFont(manajemenFont);

        CellStyle styleDosen = workbook.createCellStyle();
        styleDosen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDosen.setFont(lectureFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setBorderTop(BorderStyle.MEDIUM);
        styleProdi.setBorderBottom(BorderStyle.MEDIUM);
        styleProdi.setBorderLeft(BorderStyle.MEDIUM);
        styleProdi.setBorderRight(BorderStyle.MEDIUM);
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhs.setFont(dataFont);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.CENTER);
        styleSymbol.setFont(symbolFont);

        CellStyle styleTotal = workbook.createCellStyle();
        styleTotal.setAlignment(HorizontalAlignment.CENTER);
        styleTotal.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotal.setFont(ipFont);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIpk.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle stylePrestasiAkademik = workbook.createCellStyle();
        stylePrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        stylePrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePrestasiAkademik.setFont(dataFont);

        CellStyle styleSubHeader1 = workbook.createCellStyle();
        styleSubHeader1.setAlignment(HorizontalAlignment.CENTER);
        styleSubHeader1.setVerticalAlignment(VerticalAlignment.CENTER);
        styleSubHeader1.setFont(ipFont);

        int rowInfoNama = 5;
        Row nama = sheet.createRow(rowInfoNama);
        nama.createCell(0).setCellValue("Name");
        nama.createCell(3).setCellValue(":");
        nama.createCell(4).setCellValue(mahasiswa.getNama());
        nama.getCell(0).setCellStyle(styleData);
        nama.getCell(3).setCellStyle(styleSymbol);
        nama.getCell(4).setCellStyle(styleData);

        int rowInfoNim = 6;
        Row matricNo = sheet.createRow(rowInfoNim);
        matricNo.createCell(0).setCellValue("Student Matric No");
        matricNo.createCell(3).setCellValue(":");
        matricNo.createCell(4).setCellValue(mahasiswa.getNim());
        matricNo.getCell(0).setCellStyle(styleData);
        matricNo.getCell(3).setCellStyle(styleSymbol);
        matricNo.getCell(4).setCellStyle(styleData);

        int rowInfoEntry = 7;
        Row entry = sheet.createRow(rowInfoEntry);
        entry.createCell(0).setCellValue("Entry Matric No / Graduated Matric No");
        entry.createCell(3).setCellValue(":");
        entry.createCell(4).setCellValue(mahasiswa.getNirm() + " / " + mahasiswa.getNirl());
        entry.getCell(0).setCellStyle(styleData);
        entry.getCell(3).setCellStyle(styleSymbol);
        entry.getCell(4).setCellStyle(styleData);

        int rowInfoBirth = 8;
        Row birthDay = sheet.createRow(rowInfoBirth);
        birthDay.createCell(0).setCellValue("Place and Date of Birth");
        birthDay.createCell(3).setCellValue(":");

        int month = mahasiswa.getTanggalLahir().getDayOfMonth();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getPattern(month));
        String birthdate = mahasiswa.getTanggalLahir().format(formatter);
        birthDay.createCell(4).setCellValue(mahasiswa.getTempatLahir() + "," + " " + birthdate);
        birthDay.getCell(4).setCellStyle(styleData);

        birthDay.getCell(0).setCellStyle(styleData);
        birthDay.getCell(3).setCellStyle(styleSymbol);

        int rowInfoLevel = 9;
        Row level = sheet.createRow(rowInfoLevel);
        level.createCell(0).setCellValue("Level");
        level.createCell(3).setCellValue(":");

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            level.createCell(4).setCellValue("Undergraduate");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            level.createCell(4).setCellValue("Post Graduate");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("03").get()) {
            level.createCell(4).setCellValue("Undergraduate");
            level.getCell(4).setCellStyle(styleData);

        }
        level.getCell(0).setCellStyle(styleData);
        level.getCell(3).setCellStyle(styleSymbol);

        int rowInfoDepartment = 10;
        Row department = sheet.createRow(rowInfoDepartment);
        department.createCell(0).setCellValue("Department");
        department.createCell(3).setCellValue(":");
        department.createCell(4).setCellValue(mahasiswa.getIdProdi().getNamaProdiEnglish());
        department.getCell(0).setCellStyle(styleData);
        department.getCell(3).setCellStyle(styleSymbol);
        department.getCell(4).setCellStyle(styleData);

        int rowInfoFaculty = 11;
        Row facultyy = sheet.createRow(rowInfoFaculty);
        facultyy.createCell(0).setCellValue("Faculty");
        facultyy.createCell(3).setCellValue(":");
        facultyy.createCell(4).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultasEnglish());
        facultyy.getCell(0).setCellStyle(styleData);
        facultyy.getCell(3).setCellStyle(styleSymbol);
        facultyy.getCell(4).setCellStyle(styleData);

        int rowInfoNoAcred = 12;
        Row accreditation = sheet.createRow(rowInfoNoAcred);
        accreditation.createCell(0).setCellValue("No of Accreditation Decree");
        accreditation.createCell(3).setCellValue(":");
        accreditation.createCell(4).setCellValue(mahasiswa.getIdProdi().getNoSk());
        accreditation.getCell(0).setCellStyle(styleData);
        accreditation.getCell(3).setCellStyle(styleSymbol);
        accreditation.getCell(4).setCellStyle(styleData);

        int rowInfoDateAcred = 13;
        Row dateAccreditation = sheet.createRow(rowInfoDateAcred);
        dateAccreditation.createCell(0).setCellValue("Date of Accreditation Decree");
        dateAccreditation.createCell(3).setCellValue(":");
        int monthAccred = mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth();
        DateTimeFormatter formatterAccred = DateTimeFormatter.ofPattern(getPattern(monthAccred));
        String accredDate = mahasiswa.getIdProdi().getTanggalSk().format(formatterAccred);
        dateAccreditation.createCell(4).setCellValue(accredDate);
        dateAccreditation.getCell(4).setCellStyle(styleData);

        dateAccreditation.getCell(0).setCellStyle(styleData);
        dateAccreditation.getCell(3).setCellStyle(styleSymbol);

        int rowInfoGraduatedDate = 14;
        Row graduatedDate = sheet.createRow(rowInfoGraduatedDate);
        graduatedDate.createCell(0).setCellValue("Graduated Date");
        graduatedDate.createCell(3).setCellValue(":");
        int monthGraduate = mahasiswa.getTanggalLulus().getDayOfMonth();
        DateTimeFormatter formatterGraduate = DateTimeFormatter.ofPattern(getPattern(monthGraduate));
        String graduateDate = mahasiswa.getTanggalLulus().format(formatterGraduate);

        graduatedDate.createCell(4).setCellValue(graduateDate);
        graduatedDate.getCell(4).setCellStyle(styleData);

        graduatedDate.getCell(0).setCellStyle(styleData);
        graduatedDate.getCell(3).setCellStyle(styleSymbol);

        int rowInfoTranscript = 15;
        Row transcript = sheet.createRow(rowInfoTranscript);
        transcript.createCell(0).setCellValue("No of Transcript");
        transcript.createCell(3).setCellValue(":");
        transcript.createCell(4).setCellValue(mahasiswa.getNoTranskript());
        transcript.getCell(0).setCellStyle(styleData);
        transcript.getCell(3).setCellStyle(styleSymbol);
        transcript.getCell(4).setCellStyle(styleData);

        int rowNumSemester1 = 18;
        for (TranskriptDto sem1 : semester1) {
            Row row = sheet.createRow(rowNumSemester1);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 7, 8));

            row.createCell(0).setCellValue(sem1.getKode());
            row.createCell(1).setCellValue(sem1.getCourses());
            row.createCell(5).setCellValue(sem1.getSks());
            row.createCell(6).setCellValue(sem1.getGrade());
            row.createCell(7).setCellValue(sem1.getBobot().toString());
            row.createCell(9).setCellValue(sem1.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester1++;
        }

        int rowNumSemester2 = 18 + semester1.size();
        for (TranskriptDto sem2 : semester2) {
            Row row = sheet.createRow(rowNumSemester2);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 7, 8));

            row.createCell(0).setCellValue(sem2.getKode());
            row.createCell(1).setCellValue(sem2.getCourses());
            row.createCell(5).setCellValue(sem2.getSks());
            row.createCell(6).setCellValue(sem2.getGrade());
            row.createCell(7).setCellValue(sem2.getBobot().toString());
            row.createCell(9).setCellValue(sem2.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester2++;
        }

        int rowNumSemester3 = 18 + semester1.size() + semester2.size();
        for (TranskriptDto sem3 : semester3) {
            Row row = sheet.createRow(rowNumSemester3);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 7, 8));

            row.createCell(0).setCellValue(sem3.getKode());
            row.createCell(1).setCellValue(sem3.getCourses());
            row.createCell(5).setCellValue(sem3.getSks());
            row.createCell(6).setCellValue(sem3.getGrade());
            row.createCell(7).setCellValue(sem3.getBobot().toString());
            row.createCell(9).setCellValue(sem3.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester3++;
        }

        int rowNumSemester4 = 18 + semester1.size() + semester2.size() + semester3.size();
        for (TranskriptDto sem4 : semester4) {
            Row row = sheet.createRow(rowNumSemester4);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 7, 8));

            row.createCell(0).setCellValue(sem4.getKode());
            row.createCell(1).setCellValue(sem4.getCourses());
            row.createCell(5).setCellValue(sem4.getSks());
            row.createCell(6).setCellValue(sem4.getGrade());
            row.createCell(7).setCellValue(sem4.getBobot().toString());
            row.createCell(9).setCellValue(sem4.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester4++;
        }

        int rowNumSemester5 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size();
        for (TranskriptDto sem5 : semester5) {
            Row row = sheet.createRow(rowNumSemester5);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 7, 8));

            row.createCell(0).setCellValue(sem5.getKode());
            row.createCell(1).setCellValue(sem5.getCourses());
            row.createCell(5).setCellValue(sem5.getSks());
            row.createCell(6).setCellValue(sem5.getGrade());
            row.createCell(7).setCellValue(sem5.getBobot().toString());
            row.createCell(9).setCellValue(sem5.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester5++;
        }

        int rowNumSemester6 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size();
        for (TranskriptDto sem6 : semester6) {
            Row row = sheet.createRow(rowNumSemester6);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 7, 8));

            row.createCell(0).setCellValue(sem6.getKode());
            row.createCell(1).setCellValue(sem6.getCourses());
            row.createCell(5).setCellValue(sem6.getSks());
            row.createCell(6).setCellValue(sem6.getGrade());
            row.createCell(7).setCellValue(sem6.getBobot().toString());
            row.createCell(9).setCellValue(sem6.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester6++;
        }

        int rowNumSemester7 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size();
        for (TranskriptDto sem7 : semester7) {
            Row row = sheet.createRow(rowNumSemester7);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 7, 8));

            row.createCell(0).setCellValue(sem7.getKode());
            row.createCell(1).setCellValue(sem7.getCourses());
            row.createCell(5).setCellValue(sem7.getSks());
            row.createCell(6).setCellValue(sem7.getGrade());
            row.createCell(7).setCellValue(sem7.getBobot().toString());
            row.createCell(9).setCellValue(sem7.getMutu().toString());
            row.getCell(1).setCellStyle(styleData);
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester7++;
        }

        int rowNumSemester8 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size();
        for (TranskriptDto sem8 : semester8) {
            Row row = sheet.createRow(rowNumSemester8);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 7, 8));

            row.createCell(0).setCellValue(sem8.getKode());
            row.createCell(1).setCellValue(sem8.getCourses());
            row.createCell(5).setCellValue(sem8.getSks());
            row.createCell(6).setCellValue(sem8.getGrade());
            row.createCell(7).setCellValue(sem8.getBobot().toString());
            row.createCell(9).setCellValue(sem8.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester8++;
        }

        int total = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size();
        Row rowTotal = sheet.createRow(total);
        sheet.addMergedRegion(new CellRangeAddress(total, total, 1, 4));
        rowTotal.createCell(1).setCellValue("Total");
        rowTotal.createCell(5).setCellValue(totalSKS.intValue());
        rowTotal.createCell(9).setCellValue(totalMuti.toString());
        rowTotal.getCell(1).setCellStyle(styleTotal);
        rowTotal.getCell(5).setCellStyle(styleDataKhs);
        rowTotal.getCell(9).setCellStyle(styleDataKhs);

        int ipKomulatif = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 2;
        Row rowIpk = sheet.createRow(ipKomulatif);
        sheet.addMergedRegion(new CellRangeAddress(ipKomulatif, ipKomulatif, 0, 2));
        rowIpk.createCell(0).setCellValue("Cumulative Grade Point Average");
        rowIpk.createCell(5).setCellValue(ipk.getIpk().toString());
        rowIpk.getCell(0).setCellStyle(styleTotal);
        rowIpk.getCell(5).setCellStyle(styleDataKhs);

        int predicate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 4;
        Row predicateRow = sheet.createRow(predicate);
        predicateRow.createCell(0).setCellValue("Predicate :");
        if (ipk.getIpk().compareTo(new BigDecimal(2.99)) <= 0) {
            predicateRow.createCell(1).setCellValue("Satisfactory");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.00)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.49)) <= 0) {
            predicateRow.createCell(1).setCellValue("Good");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.50)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.79)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);

            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Very Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.80)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(4.00)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);

            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Excellent");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        predicateRow.getCell(0).setCellStyle(styleSubHeader);

        int thesis = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 5;
        Row thesisRow = sheet.createRow(thesis);
        thesisRow.createCell(0).setCellValue("Thesis Title :");
        thesisRow.createCell(1).setCellValue(mahasiswa.getTitle());
        thesisRow.getCell(0).setCellStyle(styleSubHeader);
        thesisRow.getCell(1).setCellStyle(styleData);

        int keyResult = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 8;
        Row resultRow = sheet.createRow(keyResult);
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 5, 9));
        resultRow.createCell(0).setCellValue("Key to Result");
        resultRow.createCell(5).setCellValue("Grading System");
        resultRow.getCell(0).setCellStyle(styleDataKhs);
        resultRow.getCell(5).setCellStyle(styleDataKhs);

        int remark = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 10;
        Row remarkRow = sheet.createRow(remark);
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 7, 9));
        remarkRow.createCell(0).setCellValue("Remarks");
        remarkRow.createCell(5).setCellValue("Grade");
        remarkRow.createCell(6).setCellValue("Value");
        remarkRow.createCell(7).setCellValue("Meaning");
        remarkRow.getCell(0).setCellStyle(styleDataKhs);
        remarkRow.getCell(5).setCellStyle(styleIpk);
        remarkRow.getCell(6).setCellStyle(styleIpk);
        remarkRow.getCell(7).setCellStyle(styleIpk);

        int excellent = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 11;
        Row excellentRow = sheet.createRow(excellent);
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 7, 9));
        excellentRow.createCell(0).setCellValue("3,80-4,00");
        excellentRow.createCell(1).setCellValue("Excellent (Minimum B)");
        excellentRow.createCell(5).setCellValue("A");
        excellentRow.createCell(6).setCellValue("4");
        excellentRow.createCell(7).setCellValue("Excellent");
        excellentRow.getCell(0).setCellStyle(styleProdi);
        excellentRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        excellentRow.getCell(5).setCellStyle(styleDataKhs);
        excellentRow.getCell(6).setCellStyle(styleDataKhs);
        excellentRow.getCell(7).setCellStyle(styleManajemen);

        int veryGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 12;
        Row veryGoodRow = sheet.createRow(veryGood);
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 7, 9));
        veryGoodRow.createCell(0).setCellValue("3,50-3,79");
        veryGoodRow.createCell(1).setCellValue("Very Good (Minimum B)");
        veryGoodRow.createCell(5).setCellValue("A-");
        veryGoodRow.createCell(6).setCellValue("3,7");
        veryGoodRow.createCell(7).setCellValue("Very Good");
        veryGoodRow.getCell(0).setCellStyle(styleProdi);
        veryGoodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        veryGoodRow.getCell(5).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(6).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(7).setCellStyle(styleManajemen);

        int good = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 13;
        Row goodRow = sheet.createRow(good);
        sheet.addMergedRegion(new CellRangeAddress(good, good, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(good, good, 7, 9));
        goodRow.createCell(0).setCellValue("3,00-3,49");
        goodRow.createCell(1).setCellValue("Good");
        goodRow.createCell(5).setCellValue("B+");
        goodRow.createCell(6).setCellValue("3,3");
        goodRow.createCell(7).setCellValue("Good");
        goodRow.getCell(0).setCellStyle(styleProdi);
        goodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        goodRow.getCell(5).setCellStyle(styleDataKhs);
        goodRow.getCell(6).setCellStyle(styleDataKhs);
        goodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactory = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 14;
        Row satisfactoryRow = sheet.createRow(satisfactory);
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 7, 9));
        satisfactoryRow.createCell(0).setCellValue("2,75-2,99");
        satisfactoryRow.createCell(1).setCellValue("Satisfactory");
        satisfactoryRow.createCell(5).setCellValue("B");
        satisfactoryRow.createCell(6).setCellValue("3");
        satisfactoryRow.createCell(7).setCellValue("Good");
        satisfactoryRow.getCell(0).setCellStyle(styleProdi);
        satisfactoryRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        satisfactoryRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(7).setCellStyle(styleManajemen);

        int almostGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 15;
        Row almostGoodRow = sheet.createRow(almostGood);
        sheet.addMergedRegion(new CellRangeAddress(almostGood, almostGood, 7, 9));
        almostGoodRow.createCell(5).setCellValue("B-");
        almostGoodRow.createCell(6).setCellValue("2,7");
        almostGoodRow.createCell(7).setCellValue("Almost Good");
        almostGoodRow.getCell(5).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(6).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryCplus = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 16;
        Row satisfactoryCplusRow = sheet.createRow(satisfactoryCplus);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryCplus, satisfactoryCplus, 7, 9));
        satisfactoryCplusRow.createCell(5).setCellValue("C+");
        satisfactoryCplusRow.createCell(6).setCellValue("2,3");
        satisfactoryCplusRow.createCell(7).setCellValue("Satisfactory");
        satisfactoryCplusRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryC = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 17;
        Row satisfactoryCRow = sheet.createRow(satisfactoryC);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryC, satisfactoryC, 7, 9));
        satisfactoryCRow.createCell(5).setCellValue("C");
        satisfactoryCRow.createCell(6).setCellValue("2");
        satisfactoryCRow.createCell(7).setCellValue("Satisfactory");
        satisfactoryCRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(7).setCellStyle(styleManajemen);

        int poor = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 18;
        Row poorRow = sheet.createRow(poor);
        sheet.addMergedRegion(new CellRangeAddress(poor, poor, 7, 9));
        poorRow.createCell(5).setCellValue("D");
        poorRow.createCell(6).setCellValue("1");
        poorRow.createCell(7).setCellValue("Poor");
        poorRow.getCell(5).setCellStyle(styleDataKhs);
        poorRow.getCell(6).setCellStyle(styleDataKhs);
        poorRow.getCell(7).setCellStyle(styleManajemen);

        int fail = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 19;
        Row failRow = sheet.createRow(fail);
        sheet.addMergedRegion(new CellRangeAddress(fail, fail, 7, 9));
        failRow.createCell(5).setCellValue("E");
        failRow.createCell(6).setCellValue("0");
        failRow.createCell(7).setCellValue("Fail");
        failRow.getCell(5).setCellStyle(styleDataKhs);
        failRow.getCell(6).setCellStyle(styleDataKhs);
        failRow.getCell(7).setCellStyle(styleManajemen);

        int createDate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 24;
        Row createDateRow = sheet.createRow(createDate);
        HijrahDate islamicDate = HijrahDate.from(LocalDate.now());
        String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
        String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("d", new Locale("en")));
        String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

        int monthCreate = LocalDate.now().getDayOfMonth();
        DateTimeFormatter formatterCreate = DateTimeFormatter.ofPattern(getPattern(monthCreate));
        String createDatee = LocalDate.now().format(formatterCreate);

        if (namaBulanHijri.equals("Jumada I")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Jumadil Awal "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Jumada II")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Jumadil Akhir "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Rabiʻ I")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Rabi'ul Awal "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Rabiʻ II")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Rabi'ul Akhir "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " " + namaBulanHijri
                            + " " + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        }

        int faculty = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 26;
        Row facultyRow = sheet.createRow(faculty);
        facultyRow.createCell(0).setCellValue("Dean of ");
        facultyRow.getCell(0).setCellStyle(styleData);
        facultyRow.createCell(5).setCellValue("Coordinator of ");
        facultyRow.getCell(5).setCellStyle(styleData);

        int faculty2 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 27;
        Row facultyRow2 = sheet.createRow(faculty2);
        facultyRow2.createCell(0).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultasEnglish());
        facultyRow2.getCell(0).setCellStyle(styleData);
        facultyRow2.createCell(5).setCellValue(mahasiswa.getIdProdi().getNamaProdiEnglish());
        facultyRow2.getCell(5).setCellStyle(styleData);

        int lecture = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 32;
        Row lectureRow = sheet.createRow(lecture);
        lectureRow.createCell(0)
                .setCellValue(mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNamaKaryawan());
        lectureRow.getCell(0).setCellStyle(styleDosen);
        lectureRow.createCell(5)
                .setCellValue(mahasiswa.getIdProdi().getDosen().getKaryawan().getNamaKaryawan());
        lectureRow.getCell(5).setCellStyle(styleDosen);

        int nik = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 33;
        Row nikRow = sheet.createRow(nik);
        nikRow.createCell(0)
                .setCellValue("NIK : " + mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNik());
        nikRow.getCell(0).setCellStyle(styleNik);
        nikRow.createCell(5).setCellValue("NIK : " + mahasiswa.getIdProdi().getDosen().getKaryawan().getNik());
        nikRow.getCell(5).setCellStyle(styleNik);

        PropertyTemplate propertyTemplate = new PropertyTemplate();
        // semester1
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester2
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 0,
                        0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 1,
                        4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 5,
                        5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 6,
                        6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 7,
                        8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 9,
                        9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester3
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester4
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester5
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester6
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester7
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester8
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        5, 9),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 11,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 12,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 13,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 14,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);

        propertyTemplate.applyBorders(sheet);

        String namaFile = "Transkript-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/studiesActivity/transcript/transkriptindo")
    public void transkriptExcelIndo(@RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        List<TranskriptDto> semester1 = krsDetailDao.excelTranskript(mahasiswa.getId(), "1");
        List<TranskriptDto> semester2 = krsDetailDao.excelTranskript(mahasiswa.getId(), "2");
        List<TranskriptDto> semester3 = krsDetailDao.excelTranskript(mahasiswa.getId(), "3");
        List<TranskriptDto> semester4 = krsDetailDao.excelTranskript(mahasiswa.getId(), "4");
        List<TranskriptDto> semester5 = krsDetailDao.excelTranskript(mahasiswa.getId(), "5");
        List<TranskriptDto> semester6 = krsDetailDao.excelTranskript(mahasiswa.getId(), "6");
        List<TranskriptDto> semester7 = krsDetailDao.excelTranskript(mahasiswa.getId(), "7");
        List<TranskriptDto> semester8 = krsDetailDao.excelTranskript(mahasiswa.getId(), "8");

        BigDecimal totalSKS = krsDetailDao.totalSksAkhir(mahasiswa.getId());
        BigDecimal totalMuti = krsDetailDao.totalMutuAkhir(mahasiswa.getId());

        // BigDecimal ipk = totalMuti.divide(totalSKS,2,BigDecimal.ROUND_HALF_DOWN);
        IpkDto ipk = krsDetailDao.ipk(mahasiswa);

        InputStream file = contohExcelTranskriptIndo.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());

        sheet.addMergedRegion(CellRangeAddress.valueOf("A7:C7"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:C8"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A9:C9"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A10:C10"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A11:C11"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A12:C12"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A13:C13"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A14:C14"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A15:C15"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A16:C16"));

        Font manajemenFont = workbook.createFont();
        manajemenFont.setItalic(true);
        manajemenFont.setFontHeightInPoints((short) 10);
        manajemenFont.setFontName("Cambria");

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 10);
        dataManajemenFont.setFontName("Cambria");

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 10);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 10);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        Font prodiFont = workbook.createFont();
        prodiFont.setUnderline(XSSFFont.U_DOUBLE);
        prodiFont.setFontHeightInPoints((short) 10);
        prodiFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setItalic(true);
        ipFont.setFontHeightInPoints((short) 10);
        ipFont.setFontName("Cambria");

        Font lectureFont = workbook.createFont();
        lectureFont.setBold(true);
        lectureFont.setFontName("Cambria");
        lectureFont.setUnderline(XSSFFont.U_DOUBLE);
        lectureFont.setFontHeightInPoints((short) 10);

        Font nikFont = workbook.createFont();
        nikFont.setBold(true);
        nikFont.setFontName("Cambria");
        nikFont.setFontHeightInPoints((short) 10);

        CellStyle styleNik = workbook.createCellStyle();
        styleNik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNik.setFont(nikFont);

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleManajemen.setAlignment(HorizontalAlignment.CENTER);
        styleManajemen.setFont(manajemenFont);

        CellStyle styleDosen = workbook.createCellStyle();
        styleDosen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDosen.setFont(lectureFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setBorderTop(BorderStyle.MEDIUM);
        styleProdi.setBorderBottom(BorderStyle.MEDIUM);
        styleProdi.setBorderLeft(BorderStyle.MEDIUM);
        styleProdi.setBorderRight(BorderStyle.MEDIUM);
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhs.setFont(dataFont);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.CENTER);
        styleSymbol.setFont(symbolFont);

        CellStyle styleTotal = workbook.createCellStyle();
        styleTotal.setAlignment(HorizontalAlignment.CENTER);
        styleTotal.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotal.setFont(ipFont);

        CellStyle stylePrestasiAkademik = workbook.createCellStyle();
        stylePrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        stylePrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePrestasiAkademik.setFont(dataFont);

        CellStyle styleSubHeader1 = workbook.createCellStyle();
        styleSubHeader1.setAlignment(HorizontalAlignment.CENTER);
        styleSubHeader1.setVerticalAlignment(VerticalAlignment.CENTER);
        styleSubHeader1.setFont(ipFont);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIpk.setVerticalAlignment(VerticalAlignment.CENTER);

        int rowInfoNama = 5;
        Row nama = sheet.createRow(rowInfoNama);
        nama.createCell(0).setCellValue("Nama Mahasiswa ");
        nama.createCell(3).setCellValue(":");
        nama.createCell(4).setCellValue(mahasiswa.getNama());
        nama.getCell(0).setCellStyle(styleData);
        nama.getCell(3).setCellStyle(styleSymbol);
        nama.getCell(4).setCellStyle(styleData);

        int rowInfoNim = 6;
        Row matricNo = sheet.createRow(rowInfoNim);
        matricNo.createCell(0).setCellValue("NIM");
        matricNo.createCell(3).setCellValue(":");
        matricNo.createCell(4).setCellValue(mahasiswa.getNim());
        matricNo.getCell(0).setCellStyle(styleData);
        matricNo.getCell(3).setCellStyle(styleSymbol);
        matricNo.getCell(4).setCellStyle(styleData);

        int rowInfoEntry = 7;
        Row entry = sheet.createRow(rowInfoEntry);
        entry.createCell(0).setCellValue("NIRM/NIRL");
        entry.createCell(3).setCellValue(":");
        entry.createCell(4).setCellValue(mahasiswa.getNirm() + " / " + mahasiswa.getNirl());
        entry.getCell(0).setCellStyle(styleData);
        entry.getCell(3).setCellStyle(styleSymbol);
        entry.getCell(4).setCellStyle(styleData);

        int rowInfoBirth = 8;
        Row birthDay = sheet.createRow(rowInfoBirth);
        birthDay.createCell(0).setCellValue("Tempat, Tanggal lahir");
        birthDay.createCell(3).setCellValue(":");

        if (mahasiswa.getTanggalLahir().getMonthValue() == 1) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Januari" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 2) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Februari" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 3) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Maret" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 4) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " April" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 5) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Mei" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 6) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Juni" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 7) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Juli" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 8) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Agustus" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 9) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " September" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 10) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Oktober" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 11) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " November" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 12) {
            birthDay.createCell(4)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Desember" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(4).setCellStyle(styleData);

        }

        birthDay.getCell(0).setCellStyle(styleData);
        birthDay.getCell(3).setCellStyle(styleSymbol);

        int rowInfoLevel = 9;
        Row level = sheet.createRow(rowInfoLevel);
        level.createCell(0).setCellValue("Program Pendidikan");
        level.createCell(3).setCellValue(":");

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            level.createCell(4).setCellValue("Sarjana");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            level.createCell(4).setCellValue("Magister");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("03").get()) {
            level.createCell(4).setCellValue("Sarjana");
            level.getCell(4).setCellStyle(styleData);

        }
        level.getCell(0).setCellStyle(styleData);
        level.getCell(3).setCellStyle(styleSymbol);

        int rowInfoDepartment = 10;
        Row department = sheet.createRow(rowInfoDepartment);
        department.createCell(0).setCellValue("Program Studi");
        department.createCell(3).setCellValue(":");
        department.createCell(4).setCellValue(mahasiswa.getIdProdi().getNamaProdi());
        department.getCell(0).setCellStyle(styleData);
        department.getCell(3).setCellStyle(styleSymbol);
        department.getCell(4).setCellStyle(styleData);

        int rowInfoFaculty = 11;
        Row faculty = sheet.createRow(rowInfoFaculty);
        faculty.createCell(0).setCellValue("Fakultas");
        faculty.createCell(3).setCellValue(":");
        faculty.createCell(4).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        faculty.getCell(0).setCellStyle(styleData);
        faculty.getCell(3).setCellStyle(styleSymbol);
        faculty.getCell(4).setCellStyle(styleData);

        int rowInfoNoAcred = 12;
        Row accreditation = sheet.createRow(rowInfoNoAcred);
        accreditation.createCell(0).setCellValue("No SK BAN - PT");
        accreditation.createCell(3).setCellValue(":");
        accreditation.createCell(4).setCellValue(mahasiswa.getIdProdi().getNoSk());
        accreditation.getCell(0).setCellStyle(styleData);
        accreditation.getCell(3).setCellStyle(styleSymbol);
        accreditation.getCell(4).setCellStyle(styleData);

        int rowInfoDateAcred = 13;
        Row dateAccreditation = sheet.createRow(rowInfoDateAcred);
        dateAccreditation.createCell(0).setCellValue("Tanggal SK BAN - PT ");
        dateAccreditation.createCell(3).setCellValue(":");
        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 1) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Januari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 2) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Februari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 3) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Maret" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 4) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " April" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 5) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Mei"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 6) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juni"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 7) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juli"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 8) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Agustus" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 9) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " September" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 10) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Oktober" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 11) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " November" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 12) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Desember" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        dateAccreditation.getCell(0).setCellStyle(styleData);
        dateAccreditation.getCell(3).setCellStyle(styleSymbol);

        int rowInfoGraduatedDate = 14;
        Row graduatedDate = sheet.createRow(rowInfoGraduatedDate);
        graduatedDate.createCell(0).setCellValue("Tanggal Kelulusan");
        graduatedDate.createCell(3).setCellValue(":");
        if (mahasiswa.getTanggalLulus().getMonthValue() == 1) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Januari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 2) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Februari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 3) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Maret" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 4) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " April" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 5) {
            graduatedDate.createCell(4).setCellValue(
                    mahasiswa.getTanggalLulus().getDayOfMonth() + " Mei" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 6) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juni" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 7) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juli" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 8) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Agustus" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 9) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " September" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 10) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Oktober" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 11) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " November" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 12) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Desember" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        graduatedDate.getCell(0).setCellStyle(styleData);
        graduatedDate.getCell(3).setCellStyle(styleSymbol);

        int rowInfoTranscript = 15;
        Row transcript = sheet.createRow(rowInfoTranscript);
        transcript.createCell(0).setCellValue("No Transkrip ");
        transcript.createCell(3).setCellValue(":");
        transcript.createCell(4).setCellValue(mahasiswa.getNoTranskript());
        transcript.getCell(0).setCellStyle(styleData);
        transcript.getCell(3).setCellStyle(styleSymbol);
        transcript.getCell(4).setCellStyle(styleData);

        int rowNumSemester1 = 18;
        for (TranskriptDto sem1 : semester1) {
            Row row = sheet.createRow(rowNumSemester1);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 7, 8));

            row.createCell(0).setCellValue(sem1.getKode());
            row.createCell(1).setCellValue(sem1.getMatakuliah());
            row.createCell(5).setCellValue(sem1.getSks());
            row.createCell(6).setCellValue(sem1.getGrade());
            row.createCell(7).setCellValue(sem1.getBobot().toString());
            row.createCell(9).setCellValue(sem1.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester1++;
        }

        int rowNumSemester2 = 18 + semester1.size();
        for (TranskriptDto sem2 : semester2) {
            Row row = sheet.createRow(rowNumSemester2);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 7, 8));

            row.createCell(0).setCellValue(sem2.getKode());
            row.createCell(1).setCellValue(sem2.getMatakuliah());
            row.createCell(5).setCellValue(sem2.getSks());
            row.createCell(6).setCellValue(sem2.getGrade());
            row.createCell(7).setCellValue(sem2.getBobot().toString());
            row.createCell(9).setCellValue(sem2.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester2++;
        }

        int rowNumSemester3 = 18 + semester1.size() + semester2.size();
        for (TranskriptDto sem3 : semester3) {
            Row row = sheet.createRow(rowNumSemester3);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 7, 8));

            row.createCell(0).setCellValue(sem3.getKode());
            row.createCell(1).setCellValue(sem3.getMatakuliah());
            row.createCell(5).setCellValue(sem3.getSks());
            row.createCell(6).setCellValue(sem3.getGrade());
            row.createCell(7).setCellValue(sem3.getBobot().toString());
            row.createCell(9).setCellValue(sem3.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester3++;
        }

        int rowNumSemester4 = 18 + semester1.size() + semester2.size() + semester3.size();
        for (TranskriptDto sem4 : semester4) {
            Row row = sheet.createRow(rowNumSemester4);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 7, 8));

            row.createCell(0).setCellValue(sem4.getKode());
            row.createCell(1).setCellValue(sem4.getMatakuliah());
            row.createCell(5).setCellValue(sem4.getSks());
            row.createCell(6).setCellValue(sem4.getGrade());
            row.createCell(7).setCellValue(sem4.getBobot().toString());
            row.createCell(9).setCellValue(sem4.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester4++;
        }

        int rowNumSemester5 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size();
        for (TranskriptDto sem5 : semester5) {
            Row row = sheet.createRow(rowNumSemester5);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 7, 8));

            row.createCell(0).setCellValue(sem5.getKode());
            row.createCell(1).setCellValue(sem5.getMatakuliah());
            row.createCell(5).setCellValue(sem5.getSks());
            row.createCell(6).setCellValue(sem5.getGrade());
            row.createCell(7).setCellValue(sem5.getBobot().toString());
            row.createCell(9).setCellValue(sem5.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester5++;
        }

        int rowNumSemester6 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size();
        for (TranskriptDto sem6 : semester6) {
            Row row = sheet.createRow(rowNumSemester6);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 7, 8));

            row.createCell(0).setCellValue(sem6.getKode());
            row.createCell(1).setCellValue(sem6.getMatakuliah());
            row.createCell(5).setCellValue(sem6.getSks());
            row.createCell(6).setCellValue(sem6.getGrade());
            row.createCell(7).setCellValue(sem6.getBobot().toString());
            row.createCell(9).setCellValue(sem6.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester6++;
        }

        int rowNumSemester7 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size();
        for (TranskriptDto sem7 : semester7) {
            Row row = sheet.createRow(rowNumSemester7);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 7, 8));

            row.createCell(0).setCellValue(sem7.getKode());
            row.createCell(1).setCellValue(sem7.getMatakuliah());
            row.createCell(5).setCellValue(sem7.getSks());
            row.createCell(6).setCellValue(sem7.getGrade());
            row.createCell(7).setCellValue(sem7.getBobot().toString());
            row.createCell(9).setCellValue(sem7.getMutu().toString());
            row.getCell(1).setCellStyle(styleData);
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester7++;
        }

        int rowNumSemester8 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size();
        for (TranskriptDto sem8 : semester8) {
            Row row = sheet.createRow(rowNumSemester8);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 7, 8));

            row.createCell(0).setCellValue(sem8.getKode());
            row.createCell(1).setCellValue(sem8.getMatakuliah());
            row.createCell(5).setCellValue(sem8.getSks());
            row.createCell(6).setCellValue(sem8.getGrade());
            row.createCell(7).setCellValue(sem8.getBobot().toString());
            row.createCell(9).setCellValue(sem8.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester8++;
        }

        int total = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size();
        Row rowTotal = sheet.createRow(total);
        sheet.addMergedRegion(new CellRangeAddress(total, total, 1, 4));
        rowTotal.createCell(1).setCellValue("Jumlah");
        rowTotal.createCell(5).setCellValue(totalSKS.intValue());
        rowTotal.createCell(9).setCellValue(totalMuti.toString());
        rowTotal.getCell(1).setCellStyle(styleTotal);
        rowTotal.getCell(5).setCellStyle(styleDataKhs);
        rowTotal.getCell(9).setCellStyle(styleDataKhs);

        int ipKomulatif = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 2;
        Row rowIpk = sheet.createRow(ipKomulatif);
        sheet.addMergedRegion(new CellRangeAddress(ipKomulatif, ipKomulatif, 0, 2));
        rowIpk.createCell(0).setCellValue("Indeks Prestasi Kumulatif");
        rowIpk.createCell(5).setCellValue(ipk.getIpk().toString());
        rowIpk.getCell(0).setCellStyle(styleTotal);
        rowIpk.getCell(5).setCellStyle(styleDataKhs);

        int predicate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 4;
        Row predicateRow = sheet.createRow(predicate);
        predicateRow.createCell(0).setCellValue("Predikat :");
        if (ipk.getIpk().compareTo(new BigDecimal(2.99)) <= 0) {
            predicateRow.createCell(1).setCellValue("Memuaskan");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.00)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.49)) <= 0) {
            predicateRow.createCell(1).setCellValue("Sangat Memuaskan");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.50)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.79)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Pujian ");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.80)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(4.00)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Pujian Tertinggi");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        predicateRow.getCell(0).setCellStyle(styleSubHeader);

        int thesis = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 5;
        Row thesisRow = sheet.createRow(thesis);
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            thesisRow.createCell(0).setCellValue("Judul skripsi :");
            thesisRow.createCell(1).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(0).setCellStyle(styleSubHeader);
            thesisRow.getCell(1).setCellStyle(styleData);
        }

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            thesisRow.createCell(0).setCellValue("Judul Tesis :");
            thesisRow.createCell(1).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(0).setCellStyle(styleSubHeader);
            thesisRow.getCell(1).setCellStyle(styleData);
        }

        int keyResult = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 8;
        Row resultRow = sheet.createRow(keyResult);
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 5, 9));
        resultRow.createCell(0).setCellValue("Prestasi Akademik");
        resultRow.createCell(5).setCellValue("Sistem Penilaian");
        resultRow.getCell(0).setCellStyle(styleDataKhs);
        resultRow.getCell(5).setCellStyle(styleDataKhs);

        int remark = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 10;
        Row remarkRow = sheet.createRow(remark);
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 7, 9));
        remarkRow.createCell(0).setCellValue("Keterangan");
        remarkRow.createCell(5).setCellValue("HM");
        remarkRow.createCell(6).setCellValue("AM");
        remarkRow.createCell(7).setCellValue("Arti");
        remarkRow.getCell(0).setCellStyle(styleDataKhs);
        remarkRow.getCell(5).setCellStyle(styleIpk);
        remarkRow.getCell(6).setCellStyle(styleIpk);
        remarkRow.getCell(7).setCellStyle(styleIpk);

        int excellent = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 11;
        Row excellentRow = sheet.createRow(excellent);
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 7, 9));
        excellentRow.createCell(0).setCellValue("3,80-4,00");
        excellentRow.createCell(1).setCellValue("Pujian Tertinggi (Minimal B)");
        excellentRow.createCell(5).setCellValue("A");
        excellentRow.createCell(6).setCellValue("4");
        excellentRow.createCell(7).setCellValue("Baik Sekali");
        excellentRow.getCell(0).setCellStyle(styleProdi);
        excellentRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        excellentRow.getCell(5).setCellStyle(styleDataKhs);
        excellentRow.getCell(6).setCellStyle(styleDataKhs);
        excellentRow.getCell(7).setCellStyle(styleManajemen);

        int veryGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 12;
        Row veryGoodRow = sheet.createRow(veryGood);
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 7, 9));
        veryGoodRow.createCell(0).setCellValue("3,50-3,79");
        veryGoodRow.createCell(1).setCellValue("Pujian (Minimal B)");
        veryGoodRow.createCell(5).setCellValue("A-");
        veryGoodRow.createCell(6).setCellValue("3,7");
        veryGoodRow.createCell(7).setCellValue("Baik Sekali");
        veryGoodRow.getCell(0).setCellStyle(styleProdi);
        veryGoodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        veryGoodRow.getCell(5).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(6).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(7).setCellStyle(styleManajemen);

        int good = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 13;
        Row goodRow = sheet.createRow(good);
        sheet.addMergedRegion(new CellRangeAddress(good, good, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(good, good, 7, 9));
        goodRow.createCell(0).setCellValue("3,00-3,49");
        goodRow.createCell(1).setCellValue("Sangat Memuaskan");
        goodRow.createCell(5).setCellValue("B+");
        goodRow.createCell(6).setCellValue("3,3");
        goodRow.createCell(7).setCellValue("Baik");
        goodRow.getCell(0).setCellStyle(styleProdi);
        goodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        goodRow.getCell(5).setCellStyle(styleDataKhs);
        goodRow.getCell(6).setCellStyle(styleDataKhs);
        goodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactory = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 14;
        Row satisfactoryRow = sheet.createRow(satisfactory);
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 7, 9));
        satisfactoryRow.createCell(0).setCellValue("2,75-2,99");
        satisfactoryRow.createCell(1).setCellValue("Memuaskan");
        satisfactoryRow.createCell(5).setCellValue("B");
        satisfactoryRow.createCell(6).setCellValue("3");
        satisfactoryRow.createCell(7).setCellValue("Baik");
        satisfactoryRow.getCell(0).setCellStyle(styleProdi);
        satisfactoryRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        satisfactoryRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(7).setCellStyle(styleManajemen);

        int almostGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 15;
        Row almostGoodRow = sheet.createRow(almostGood);
        sheet.addMergedRegion(new CellRangeAddress(almostGood, almostGood, 7, 9));
        almostGoodRow.createCell(5).setCellValue("B-");
        almostGoodRow.createCell(6).setCellValue("2,7");
        almostGoodRow.createCell(7).setCellValue("Baik");
        almostGoodRow.getCell(5).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(6).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryCplus = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 16;
        Row satisfactoryCplusRow = sheet.createRow(satisfactoryCplus);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryCplus, satisfactoryCplus, 7, 9));
        satisfactoryCplusRow.createCell(5).setCellValue("C+");
        satisfactoryCplusRow.createCell(6).setCellValue("2,3");
        satisfactoryCplusRow.createCell(7).setCellValue("Cukup");
        satisfactoryCplusRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryC = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 17;
        Row satisfactoryCRow = sheet.createRow(satisfactoryC);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryC, satisfactoryC, 7, 9));
        satisfactoryCRow.createCell(5).setCellValue("C");
        satisfactoryCRow.createCell(6).setCellValue("2");
        satisfactoryCRow.createCell(7).setCellValue("Cukup");
        satisfactoryCRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(7).setCellStyle(styleManajemen);

        int poor = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 18;
        Row poorRow = sheet.createRow(poor);
        sheet.addMergedRegion(new CellRangeAddress(poor, poor, 7, 9));
        poorRow.createCell(5).setCellValue("D");
        poorRow.createCell(6).setCellValue("1");
        poorRow.createCell(7).setCellValue("Kurang");
        poorRow.getCell(5).setCellStyle(styleDataKhs);
        poorRow.getCell(6).setCellStyle(styleDataKhs);
        poorRow.getCell(7).setCellStyle(styleManajemen);

        int fail = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 19;
        Row failRow = sheet.createRow(fail);
        sheet.addMergedRegion(new CellRangeAddress(fail, fail, 7, 9));
        failRow.createCell(5).setCellValue("E");
        failRow.createCell(6).setCellValue("0");
        failRow.createCell(7).setCellValue("Sangat Kurang");
        failRow.getCell(5).setCellStyle(styleDataKhs);
        failRow.getCell(6).setCellStyle(styleDataKhs);
        failRow.getCell(7).setCellStyle(styleManajemen);

        int createDate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 24;
        Row createDateRow = sheet.createRow(createDate);
        HijrahDate islamicDate = HijrahDate.from(LocalDate.now());
        String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
        String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("dd", new Locale("en")));
        String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

        if (LocalDate.now().getMonthValue() == 1) {

            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari " + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 2) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 3) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 4) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 5) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 6) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 7) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 8) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 9) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 10) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 11) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 12) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        int facultyy = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 26;
        Row facultyRow = sheet.createRow(facultyy);
        facultyRow.createCell(0).setCellValue("Dekan ");
        facultyRow.getCell(0).setCellStyle(styleData);
        facultyRow.createCell(5).setCellValue("Koordinator ");
        facultyRow.getCell(5).setCellStyle(styleData);

        int faculty2 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 27;
        Row facultyRow2 = sheet.createRow(faculty2);
        facultyRow2.createCell(0)
                .setCellValue("Fakultas " + mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        facultyRow2.getCell(0).setCellStyle(styleData);
        facultyRow2.createCell(5).setCellValue("Program Studi " + mahasiswa.getIdProdi().getNamaProdi());
        facultyRow2.getCell(5).setCellStyle(styleData);

        int lecture = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 32;
        Row lectureRow = sheet.createRow(lecture);
        lectureRow.createCell(0)
                .setCellValue(mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNamaKaryawan());
        lectureRow.getCell(0).setCellStyle(styleDosen);
        lectureRow.createCell(5)
                .setCellValue(mahasiswa.getIdProdi().getDosen().getKaryawan().getNamaKaryawan());
        lectureRow.getCell(5).setCellStyle(styleDosen);

        int nik = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 33;
        Row nikRow = sheet.createRow(nik);
        nikRow.createCell(0)
                .setCellValue("NIK : " + mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNik());
        nikRow.getCell(0).setCellStyle(styleNik);
        nikRow.createCell(5).setCellValue("NIK : " + mahasiswa.getIdProdi().getDosen().getKaryawan().getNik());
        nikRow.getCell(5).setCellStyle(styleNik);

        PropertyTemplate propertyTemplate = new PropertyTemplate();
        // semester1
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester2
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 0,
                        0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 1,
                        4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 5,
                        5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 6,
                        6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 7,
                        8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 9,
                        9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester3
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester4
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester5
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester6
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester7
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester8
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        5, 9),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 11,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 12,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 13,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 14,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);

        propertyTemplate.applyBorders(sheet);

        String namaFile = "Transkript-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/studiesActivity/transcript/print")
    public void printTranskript(Model model, @RequestParam(required = false) String nim,
                                HttpServletResponse response)
            throws Exception {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        model.addAttribute("mhsw", mahasiswa);
        model.addAttribute("ipk", krsDetailDao.ipk(mahasiswa));

        IpkDto ipk = krsDetailDao.ipk(mahasiswa);
        Long sks = krsDetailDao.totalSks(mahasiswa);
        Long mutu = krsDetailDao.totalMutu(mahasiswa);

        List<Object[]> semester1 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "1");
        List<Object[]> semester2 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "2");
        List<Object[]> semester3 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "3");
        List<Object[]> semester4 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "4");
        List<Object[]> semester5 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "5");
        List<Object[]> semester6 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "6");
        List<Object[]> semester7 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "7");
        List<Object[]> semester8 = krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa, "8");

        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Transcript Sementara Mahasiswa " + mahasiswa.getNama()
                + ".pdf";

        response.setHeader(headerKey, headerValue);

        transcriptService.transkripSementara(mahasiswa, semester1, semester2, semester3, semester4, semester5,
                semester6, semester7, semester8, sks, ipk, mutu, response);

        // model.addAttribute("sks", krsDetailDao.totalSks(mahasiswa));
        // model.addAttribute("mutu", krsDetailDao.totalMutu(mahasiswa));
        // model.addAttribute("transkrip1",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"1"));
        // model.addAttribute("transkrip2",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"2"));
        // model.addAttribute("transkrip3",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"3"));
        // model.addAttribute("transkrip4",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"4"));
        // model.addAttribute("transkrip5",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"5"));
        // model.addAttribute("transkrip6",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"6"));
        // model.addAttribute("transkrip7",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"7"));
        // model.addAttribute("transkrip8",
        // krsDetailDao.transkripSemesterWithoutWaiting(mahasiswa,"8"));
    }

    @GetMapping("/studiesActivity/transcript/print1")
    public void printTranskript1(Model model, @RequestParam(required = false) String nim,
                                 HttpServletResponse response)
            throws IOException {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        model.addAttribute("mhsw", mahasiswa);

        BigDecimal totalSKS = krsDetailDao.totalSksAkhir(mahasiswa.getId());
        List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mahasiswa);

        int sks = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();

        BigDecimal mutu = listTranskript.stream().map(DataTranskript::getMutu)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal ipk = mutu.divide(new BigDecimal(sks), 2, BigDecimal.ROUND_HALF_UP);

        model.addAttribute("ipk", ipk);
        model.addAttribute("sks", sks);
        model.addAttribute("mutu", mutu);

        List<DataTranskript> semester1 = new ArrayList<>();
        List<DataTranskript> semester2 = new ArrayList<>();
        List<DataTranskript> semester3 = new ArrayList<>();
        List<DataTranskript> semester4 = new ArrayList<>();
        List<DataTranskript> semester5 = new ArrayList<>();
        List<DataTranskript> semester6 = new ArrayList<>();
        List<DataTranskript> semester7 = new ArrayList<>();
        List<DataTranskript> semester8 = new ArrayList<>();

        for (DataTranskript data : listTranskript) {
            System.out.println(data.getSks());
            if (data.getSemester().equals("1")) {
                semester1.add(data);
            }
            if (data.getSemester().equals("2")) {
                semester2.add(data);
            }
            if (data.getSemester().equals("3")) {
                semester3.add(data);
            }
            if (data.getSemester().equals("4")) {
                semester4.add(data);
            }
            if (data.getSemester().equals("5")) {
                semester5.add(data);
            }
            if (data.getSemester().equals("6")) {
                semester6.add(data);
            }
            if (data.getSemester().equals("7")) {
                semester7.add(data);
            }
            if (data.getSemester().equals("8")) {
                semester8.add(data);
            }
        }

        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Transcript Mahasiswa " + mahasiswa.getNama() + ".pdf";

        response.setHeader(headerKey, headerValue);

        transcriptService.transkripIndo(mahasiswa, semester1, semester2, semester3, semester4, semester5,
                semester6,
                semester7, semester8, sks, ipk, mutu, response);

        // model.addAttribute("transkrip", krsDetailDao.listTranskript(mahasiswa));
        // model.addAttribute("transkrip1", semester1);
        // model.addAttribute("transkrip2", semester2);
        // model.addAttribute("transkrip3", semester3);
        // model.addAttribute("transkrip4", semester4);
        // model.addAttribute("transkrip5", semester5);
        // model.addAttribute("transkrip6", semester6);
        // model.addAttribute("transkrip7", semester7);
        // model.addAttribute("transkrip8", semester8);

        // return "dashboardadmin";

    }

    @GetMapping("/studiesActivity/transcript/inggris")
    public void printTranskriptInggris(Model model, @RequestParam(required = false) String nim,
                                       HttpServletResponse response) throws Exception {
        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        model.addAttribute("mhsw", mahasiswa);

        BigDecimal totalSKS = krsDetailDao.totalSksAkhir(mahasiswa.getId());
        List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mahasiswa);
        listTranskript.removeIf(e -> e.getGrade().equals("E"));

        int sks = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();

        BigDecimal mutu = listTranskript.stream().map(DataTranskript::getMutu)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal ipk = mutu.divide(new BigDecimal(sks), 2, BigDecimal.ROUND_HALF_UP);

        model.addAttribute("ipk", ipk);
        model.addAttribute("sks", sks);
        model.addAttribute("mutu", mutu);

        List<DataTranskript> semester1 = new ArrayList<>();
        List<DataTranskript> semester2 = new ArrayList<>();
        List<DataTranskript> semester3 = new ArrayList<>();
        List<DataTranskript> semester4 = new ArrayList<>();
        List<DataTranskript> semester5 = new ArrayList<>();
        List<DataTranskript> semester6 = new ArrayList<>();
        List<DataTranskript> semester7 = new ArrayList<>();
        List<DataTranskript> semester8 = new ArrayList<>();

        for (DataTranskript data : listTranskript) {
            if (data.getSemester().equals("1")) {
                semester1.add(data);
            }
            if (data.getSemester().equals("2")) {
                semester2.add(data);
            }
            if (data.getSemester().equals("3")) {
                semester3.add(data);
            }
            if (data.getSemester().equals("4")) {
                semester4.add(data);
            }
            if (data.getSemester().equals("5")) {
                semester5.add(data);
            }
            if (data.getSemester().equals("6")) {
                semester6.add(data);
            }
            if (data.getSemester().equals("7")) {
                semester7.add(data);
            }
            if (data.getSemester().equals("8")) {
                semester8.add(data);
            }
        }

        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Student Transcript " + mahasiswa.getNama() + ".pdf";

        response.setHeader(headerKey, headerValue);

        transcriptService.transcriptEnglish(mahasiswa, semester1, semester2, semester3, semester4, semester5,
                semester6,
                semester7, semester8, sks, ipk, mutu, response);

        // model.addAttribute("transkrip", krsDetailDao.listTranskript(mahasiswa));
        // model.addAttribute("transkrip1", semester1);
        // model.addAttribute("transkrip2", semester2);
        // model.addAttribute("transkrip3", semester3);
        // model.addAttribute("transkrip4", semester4);
        // model.addAttribute("transkrip5", semester5);
        // model.addAttribute("transkrip6", semester6);
        // model.addAttribute("transkrip7", semester7);
        // model.addAttribute("transkrip8", semester8);

    }

    // Exam Validation

    @GetMapping("/studiesActivity/validation/list")
    public void validationQuestion(Model model, @RequestParam(required = false) TahunAkademik tahun, Pageable page,
                                   @RequestParam(required = false) StatusApprove status,
                                   @RequestParam(required = false) String search) {

        model.addAttribute("akademik",
                tahunAkademikDao.findByStatusNotInOrderByNamaTahunAkademikDesc(
                        Arrays.asList(StatusRecord.HAPUS)));

        if (tahun != null && status == null) {
            model.addAttribute("tahunAkademik", tahun);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndDosenKaryawanNamaKaryawanContainingIgnoreCaseOrMatakuliahKurikulumMatakuliahNamaMatakuliahContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, search, search, page));
            } else {
                model.addAttribute("jadwal",
                        jadwalDao.findByStatusAndTahunAkademikAndJamMulaiNotNullAndHariNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, page));
            }

        }

        if (tahun != null && status != null) {
            model.addAttribute("tahunAkademik", tahun);
            model.addAttribute("status", status);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndStatusUtsAndDosenKaryawanNamaKaryawanContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNullOrStatusAndStatusUtsAndTahunAkademikAndMatakuliahKurikulumMatakuliahNamaMatakuliahContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, status, search,
                                StatusRecord.AKTIF, status, tahun, search,
                                page));
            } else {
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndStatusUtsAndJamMulaiNotNullAndHariNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, status, page));
            }
        }

    }

    @GetMapping("/studiesActivity/validation/listuas")
    public void validationUas(Model model, @RequestParam(required = false) TahunAkademik tahun, Pageable page,
                              @RequestParam(required = false) StatusApprove status,
                              @RequestParam(required = false) String search) {
        model.addAttribute("akademik",
                tahunAkademikDao.findByStatusNotInOrderByNamaTahunAkademikDesc(
                        Arrays.asList(StatusRecord.HAPUS)));

        if (tahun != null && status != null) {
            model.addAttribute("status", status);
            model.addAttribute("tahunAkademik", tahun);
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndStatusUasAndDosenKaryawanNamaKaryawanContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNullOrStatusAndStatusUasAndTahunAkademikAndMatakuliahKurikulumMatakuliahNamaMatakuliahContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, status, search,
                                StatusRecord.AKTIF, status, tahun, search,
                                page));
            } else {
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndStatusUasAndJamMulaiNotNullAndHariNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, status, page));
            }
        }

        if (tahun != null && status == null) {
            model.addAttribute("tahunAkademik", tahun);
            if (StringUtils.hasText(search)) {
                model.addAttribute("jadwal", jadwalDao
                        .findByStatusAndTahunAkademikAndDosenKaryawanNamaKaryawanContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNullOrStatusAndTahunAkademikAndMatakuliahKurikulumMatakuliahNamaMatakuliahContainingIgnoreCaseAndHariNotNullAndJamMulaiNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, search, StatusRecord.AKTIF,
                                tahun, search, page));
                model.addAttribute("search", search);
            } else {
                model.addAttribute("jadwal",
                        jadwalDao.findByStatusAndTahunAkademikAndJamMulaiNotNullAndHariNotNullAndKelasNotNull(
                                StatusRecord.AKTIF, tahun, page));
            }

        }
    }

    @GetMapping("/studiesActivity/validation/approval")
    public void approval(Model model, @RequestParam Jadwal jadwal, @RequestParam StatusRecord status) {
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("soal", soalDao.findByJadwalAndStatusAndStatusApproveNotInAndStatusSoal(jadwal,
                StatusRecord.AKTIF, Arrays.asList(StatusApprove.REJECTED, StatusApprove.APPROVED),
                status));
    }

    @GetMapping("/studiesActivity/validation/detail")
    public void detailApprove(Model model, @RequestParam Jadwal jadwal, @RequestParam StatusRecord status) {
        model.addAttribute("jadwal", jadwal);
        model.addAttribute("soal", soalDao.findByJadwalAndStatusAndStatusApproveAndStatusSoal(jadwal,
                StatusRecord.AKTIF, StatusApprove.APPROVED, status));
    }

    @PostMapping("/studiesActivity/validation/approval")
    public String prosesApprove(@RequestParam Soal soal, MultipartFile file) throws Exception {

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        System.out.println("Nama File : {}" + namaFile);
        System.out.println("Jenis File : {}" + jenisFile);
        System.out.println("Nama Asli File : {}" + namaAsli);
        System.out.println("Ukuran File : {}" + ukuran);

        // memisahkan extensi
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }

        // String idFile =
        // soal.getJadwal().getTahunAkademik().getKodeTahunAkademik()+"-"+soal.getStatusSoal()+"-"+soal.getJadwal().getKelas().getNamaKelas()+"-"+soal.getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah()+"-"+soal.getDosen().getKaryawan().getNamaKaryawan();
        String idFile = "VLD-" + soal.getJadwal().getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah()
                + "-"
                + soal.getJadwal().getKelas().getNamaKelas();
        if (idFile.length() > 65) {

        }

        idFile = idFile.replaceAll(" ", "-").toLowerCase();
        String lokasiUpload = uploadFolder + File.separator + soal.getJadwal().getId();
        LOGGER.debug("Lokasi upload : {}", lokasiUpload);
        new File(lokasiUpload).mkdirs();
        File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);
        LOGGER.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());
        soal.setFileApprove(idFile + "." + extension);
        soal.setStatusApprove(StatusApprove.APPROVED);
        soal.setKeteranganApprove(soal.getKeterangan());
        soalDao.save(soal);
        mailService.validasiSoal(soal, "APPROVE");

        Jadwal jadwal = jadwalDao.findById(soal.getJadwal().getId()).get();
        if (soal.getStatusSoal() == StatusRecord.UTS) {
            jadwal.setStatusUts(StatusApprove.APPROVED);
        }
        if (soal.getStatusSoal() == StatusRecord.UAS) {
            jadwal.setStatusUas(StatusApprove.APPROVED);
        }
        jadwalDao.save(jadwal);

        if (soal.getStatusSoal() == StatusRecord.UAS) {
            return "redirect:listuas?tahun=" + jadwal.getTahunAkademik().getId() + "&status="
                    + StatusApprove.APPROVED;
        }
        return "redirect:list?tahun=" + jadwal.getTahunAkademik().getId() + "&status=" + StatusApprove.APPROVED;
    }

    @PostMapping("/studiesActivity/validation/rejected")
    public String prosesReject(@RequestParam Soal soal, @RequestParam(required = false) String keteranganApprove) {
        soal.setStatusApprove(StatusApprove.REJECTED);
        soal.setKeteranganApprove(keteranganApprove);
        soalDao.save(soal);
        mailService.validasiSoal(soal, "REJECT");

        Jadwal jadwal = jadwalDao.findById(soal.getJadwal().getId()).get();
        if (soal.getStatusSoal() == StatusRecord.UTS) {
            jadwal.setStatusUts(StatusApprove.REJECTED);
        }
        if (soal.getStatusSoal() == StatusRecord.UAS) {
            jadwal.setStatusUas(StatusApprove.REJECTED);
        }
        jadwalDao.save(jadwal);

        if (soal.getStatusSoal() == StatusRecord.UAS) {
            return "redirect:listuas?tahun=" + jadwal.getTahunAkademik().getId() + "&status="
                    + StatusApprove.REJECTED;
        }

        return "redirect:list?tahun=" + soal.getJadwal().getTahunAkademik().getId() + "&status="
                + StatusApprove.REJECTED;
    }

    @RequestMapping("/filedownload/")
    public void downloadSoal(HttpServletRequest request,
                             HttpServletResponse response,
                             @RequestParam Soal soal) {
        // If user is not authorized - he should be thrown out from here itself
        String fileName = soal.getFileApprove();
        // Authorized user will download the file
        String lokasi = uploadFolder + File.separator + soal.getJadwal().getId();
        String dataDirectory = request.getServletContext().getRealPath(lokasi);
        Path file = Paths.get(lokasi, fileName);
        System.out.println(file);
        if (Files.exists(file)) {
            response.setContentType(
                    "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @RequestMapping("/file/{fileName}")
    public void downloadPDFResource(HttpServletRequest request,
                                    HttpServletResponse response,
                                    @RequestParam Soal soal,
                                    @PathVariable("fileName") String fileName) {
        // If user is not authorized - he should be thrown out from here itself

        // Authorized user will download the file
        String lokasi = uploadFolder + File.separator + soal.getJadwal().getId();
        String dataDirectory = request.getServletContext().getRealPath(lokasi);
        Path file = Paths.get(lokasi, fileName);
        System.out.println(file);
        if (Files.exists(file)) {
            response.setContentType(
                    "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            try {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    @PostMapping("/studiesActivity/validation/canceluts")
    public String cancel(@RequestParam Jadwal jadwal) {
        Soal soal = soalDao.findByJadwalAndStatusAndStatusApproveAndStatusSoal(jadwal, StatusRecord.AKTIF,
                StatusApprove.APPROVED, StatusRecord.UTS);
        soal.setStatusApprove(StatusApprove.REJECTED);
        soal.setKeteranganApprove("Dibatalkan");
        soalDao.save(soal);

        jadwal.setStatusUts(StatusApprove.REJECTED);
        jadwalDao.save(jadwal);

        return "redirect:list?tahun=" + jadwal.getTahunAkademik().getId() + "&status=" + StatusApprove.APPROVED;

    }

    @PostMapping("/studiesActivity/validation/canceluas")
    public String cancelUas(@RequestParam Jadwal jadwal) {
        Soal soal = soalDao.findByJadwalAndStatusAndStatusApproveAndStatusSoal(jadwal, StatusRecord.AKTIF,
                StatusApprove.APPROVED, StatusRecord.UAS);
        soal.setStatusApprove(StatusApprove.REJECTED);
        soal.setKeteranganApprove("Dibatalkan");
        soalDao.save(soal);

        jadwal.setStatusUas(StatusApprove.REJECTED);
        jadwalDao.save(jadwal);

        return "redirect:listuas?tahun=" + jadwal.getTahunAkademik().getId() + "&status="
                + StatusApprove.APPROVED;

    }

    @GetMapping("/studiesActivity/assesment/upload/rps")
    public void listRps(@RequestParam Jadwal jadwal, Model model) {
        model.addAttribute("jadwal", jadwal);
        Iterable<Rps> rps = rpsDao.findByStatusAndJadwal(StatusRecord.AKTIF, jadwal);

        model.addAttribute("rps", rps);

    }

    @PostMapping("/studiesActivity/assesment/upload/rps")
    public String uploadRps(@Valid Rps rps, MultipartFile file, @RequestParam Jadwal jadwal) throws Exception {

        String namaFile = file.getName();
        String jenisFile = file.getContentType();
        String namaAsli = file.getOriginalFilename();
        Long ukuran = file.getSize();

        System.out.println("Nama File : {}" + namaFile);
        System.out.println("Jenis File : {}" + jenisFile);
        System.out.println("Nama Asli File : {}" + namaAsli);
        System.out.println("Ukuran File : {}" + ukuran);

        // memisahkan extensi
        String extension = "";

        int i = namaAsli.lastIndexOf('.');
        int p = Math.max(namaAsli.lastIndexOf('/'), namaAsli.lastIndexOf('\\'));

        if (i > p) {
            extension = namaAsli.substring(i + 1);
        }

        String idFile = "RPS-" + jadwal.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah();
        String lokasiUpload = uploadRps + File.separator + rps.getJadwal().getId();
        LOGGER.debug("Lokasi upload : {}", lokasiUpload);
        new File(lokasiUpload).mkdirs();
        File tujuan = new File(lokasiUpload + File.separator + idFile + "." + extension);
        file.transferTo(tujuan);
        LOGGER.debug("File sudah dicopy ke : {}", tujuan.getAbsolutePath());

        rps.setJadwal(jadwal);
        rps.setStatus(StatusRecord.AKTIF);
        rps.setTanggalUpload(LocalDate.now());
        rps.setFilename(idFile + "." + extension);
        Rps rpsId = rpsDao.findByJadwalAndStatus(rps.getJadwal(), StatusRecord.AKTIF);

        if (rpsId == null) {
            rpsDao.save(rps);
        }

        if (rpsId != null) {
            rpsId.setStatus(StatusRecord.NONAKTIF);
            rpsDao.save(rpsId);
            rpsDao.save(rps);
        }

        return "redirect:/studiesActivity/assesment/upload/rps?jadwal=" + rps.getJadwal().getId();

    }

    @GetMapping("/validation/download")
    public void validationExamDownload(@RequestParam String tahunAkademik, @RequestParam String jenis,
                                       HttpServletResponse response, @PageableDefault(size = Integer.MAX_VALUE) Pageable page)
            throws IOException {

        TahunAkademik tahun = tahunAkademikDao.findById(tahunAkademik).get();

        String[] columns = {"NO. ", "Matakuliah", "Kelas", "Dosen", "Status Upload", "Bentuk Ujian", "Prodi"};

        Page<Jadwal> jadwalPage = jadwalDao
                .findByStatusAndTahunAkademikAndJamMulaiNotNullAndHariNotNullAndKelasNotNull(
                        StatusRecord.AKTIF, tahun, page);

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("List validasi soal");

        Font fontHeader = workbook.createFont();
        fontHeader.setBold(true);
        fontHeader.setFontHeightInPoints((short) 11);
        fontHeader.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(fontHeader);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Jadwal list : jadwalPage) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(
                    list.getMatakuliahKurikulum().getMatakuliah().getNamaMatakuliah());
            row.createCell(2).setCellValue(list.getKelas().getNamaKelas());
            row.createCell(3).setCellValue(list.getDosen().getKaryawan().getNamaKaryawan());
            if ("uts".equals(jenis)) {
                row.createCell(4).setCellValue(list.getStatusUts().toString());
                row.createCell(5).setCellValue(list.getFormatUts());
            } else if ("uas".equals(jenis)) {
                row.createCell(4).setCellValue(list.getStatusUas().toString());
                row.createCell(5).setCellValue(list.getFormatUas());
            }
            row.createCell(6).setCellValue(list.getProdi().getKodeProdi());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment; filename=List_validasi_soal " + tahun.getNamaTahunAkademik() + ".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    // Pra KRS SP

    @GetMapping("/studiesActivity/sp/list")
    public void listSp(Model model) {

        List<Object[]> p1 = null;
        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
            if (tahun == null) {
                model.addAttribute("message", "Aktifkan tahun akademik pendek");
            } else {
                p1 = praKrsSpDao.listKrsSp(tahun.getId());
            }
        } else {
            p1 = praKrsSpDao.listKrsSp(tahun.getId());
        }

        model.addAttribute("list", p1);
        model.addAttribute("listDosen", dosenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS)));
        model.addAttribute("listProdi", prodiDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listTahun",
                tahunAkademikDao.findByStatusNotInOrderByTahunDesc(Arrays.asList(StatusRecord.HAPUS)));
        model.addAttribute("jadwal", jadwalDao.findByStatusAndTahunAkademik(StatusRecord.AKTIF, tahun));

        model.addAttribute("matkulDetail1", praKrsSpDao.detailSp(tahun));

    }

    @GetMapping("/studiesActivity/sp/proses")
    public void prosesSp(Model model, @RequestParam(required = false) String[] checkBox,
                         Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        }

        List<String> listSp = new ArrayList<>();
        for (String list : checkBox) {
            listSp.add(list);
        }
        List<Object[]> spList = praKrsSpDao.listLunasSpPerMatkul(tahun, listSp);
        model.addAttribute("listSp", spList);
        List<MatakuliahKurikulum> matkul = matakuliahKurikulumDao.findByStatusAndMatakuliahIdIn(
                StatusRecord.AKTIF,
                listSp);
        model.addAttribute("listMatkul", matkul);
        model.addAttribute("listId", listSp);

        // jadwal yang sudah ada
        List<Jadwal> existSchedule = jadwalDao.findByTahunAkademikAndStatusAndMatakuliahKurikulumIn(tahun,
                StatusRecord.AKTIF, matkul);
        model.addAttribute("jadwal", existSchedule);

        // halaman approve
        model.addAttribute("listDosen", dosenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS)));
        model.addAttribute("listProdi", prodiDao.findByStatus(StatusRecord.AKTIF));
        model.addAttribute("listTahun",
                tahunAkademikDao.findByStatusNotInOrderByTahunDesc(Arrays.asList(StatusRecord.HAPUS)));

        // halaman reject

        List<Object[]> listAllSp = praKrsSpDao.listAllSpPerMatkul(tahun, listSp);
        model.addAttribute("listAllSp", listAllSp);

        // halaman custom

    }

    @Transactional
    @PostMapping("/studiesActivity/sp/approve")
    public String approveSp(@RequestParam(required = false) List<String> id,
                            @RequestParam(required = false) String checkBox,
                            @RequestParam(required = false) String checkJadwal,
                            @RequestParam(required = false) String[] checkMhs, @RequestParam(required = false) String dosen,
                            @RequestParam(required = false) String prodi,
                            @RequestParam(required = false) String tahunAkademik,
                            Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        }

        List<String> listMhs = new ArrayList<>();
        for (String list : checkMhs) {
            listMhs.add(list);
        }
        System.out.println("tes : " + listMhs);

        praKrsSpDao.updateStatus(karyawan, listMhs, "APPROVED");

        Kelas kelas = kelasDao.findById("SP-01").get();

        Jadwal jadwal;
        MatakuliahKurikulum matkur;
        TahunAkademik tahunJadwal;

        if (checkJadwal != null) {
            jadwal = jadwalDao.findById(checkJadwal).get();
            matkur = jadwal.getMatakuliahKurikulum();
            tahunJadwal = jadwal.getTahunAkademik();
        } else {
            tahunJadwal = tahunAkademikDao.findById(tahunAkademik).get();
            matkur = matakuliahKurikulumDao.findById(checkBox).get();
            Dosen dos = dosenDao.findById(dosen).get();
            Prodi prod = prodiDao.findById(prodi).get();
            TahunAkademikProdi tahunProdi = tahunProdiDao.findByTahunAkademikAndProdi(tahunJadwal, prod);

            jadwal = new Jadwal();
            jadwal.setJumlahSesi(1);
            jadwal.setBobotUts(BigDecimal.ZERO);
            jadwal.setBobotUas(BigDecimal.ZERO);
            jadwal.setBobotTugas(BigDecimal.ZERO);
            jadwal.setBobotPresensi(BigDecimal.ZERO);
            jadwal.setKelas(kelas);
            jadwal.setProdi(prod);
            jadwal.setDosen(dos);
            jadwal.setTahunAkademik(tahunJadwal);
            jadwal.setTahunAkademikProdi(tahunProdi);
            jadwal.setMatakuliahKurikulum(matkur);
            jadwal.setStatusUas(StatusApprove.NOT_UPLOADED_YET);
            jadwal.setProgram(programDao.findById("01").get());
            jadwal.setAkses(Akses.UMUM);
            jadwal.setStatusUts(StatusApprove.NOT_UPLOADED_YET);
            jadwalDao.save(jadwal);
            System.out.println(matkur.getId());

            JadwalDosen jadwalDosen = new JadwalDosen();
            jadwalDosen.setStatusJadwalDosen(StatusJadwalDosen.PENGAMPU);
            jadwalDosen.setJadwal(jadwal);
            jadwalDosen.setDosen(dos);
            jadwalDosen.setJumlahIzin(0);
            jadwalDosen.setJumlahKehadiran(0);
            jadwalDosen.setJumlahMangkir(0);
            jadwalDosen.setJumlahSakit(0);
            jadwalDosen.setJumlahTerlambat(0);
            jadwalDosenDao.save(jadwalDosen);
        }

        for (String krsId : listMhs) {
            PraKrsSp findMahasiswa = praKrsSpDao.findById(krsId).get();
            Mahasiswa mhs = mahasiswaDao.findByNim(findMahasiswa.getMahasiswa().getNim());
            TahunAkademikProdi tahunAkademikProdi = tahunProdiDao.findByTahunAkademikAndProdi(tahunJadwal,
                    mhs.getIdProdi());

            KrsDetail krsDetail = krsDetailDao.findByMahasiswaAndTahunAkademikAndJadwalAndStatus(mhs,
                    tahunJadwal,
                    jadwal, StatusRecord.AKTIF);
            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mhs, tahunJadwal, StatusRecord.AKTIF);

            if (krsDetail == null) {
                if (k == null) {
                    Krs krs = new Krs();
                    krs.setTahunAkademik(tahunJadwal);
                    krs.setTahunAkademikProdi(tahunAkademikProdi);
                    krs.setProdi(mhs.getIdProdi());
                    krs.setMahasiswa(mhs);
                    krs.setNim(mhs.getNim());
                    krs.setTanggalTransaksi(LocalDateTime.now());
                    krs.setStatus(StatusRecord.AKTIF);
                    krsDao.save(krs);

                    KrsDetail kd = new KrsDetail();
                    kd.setKrs(krs);
                    kd.setMahasiswa(mhs);
                    kd.setJadwal(jadwal);
                    kd.setMatakuliahKurikulum(matkur);
                    kd.setNilaiPresensi(BigDecimal.ZERO);
                    kd.setNilaiUts(BigDecimal.ZERO);
                    kd.setNilaiTugas(BigDecimal.ZERO);
                    kd.setFinalisasi("N");
                    kd.setNilaiUas(BigDecimal.ZERO);
                    kd.setJumlahKehadiran(0);
                    kd.setJumlahMangkir(0);
                    kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                    kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                    kd.setJumlahTerlambat(0);
                    kd.setJumlahIzin(0);
                    kd.setJumlahSakit(0);
                    kd.setStatusEdom(StatusRecord.UNDONE);
                    kd.setStatus(StatusRecord.AKTIF);
                    kd.setTahunAkademik(tahunJadwal);
                    krsDetailDao.save(kd);

                } else {

                    KrsDetail kd = new KrsDetail();
                    kd.setKrs(k);
                    kd.setMahasiswa(mhs);
                    kd.setJadwal(jadwal);
                    kd.setMatakuliahKurikulum(matkur);
                    kd.setNilaiPresensi(BigDecimal.ZERO);
                    kd.setNilaiUts(BigDecimal.ZERO);
                    kd.setNilaiTugas(BigDecimal.ZERO);
                    kd.setFinalisasi("N");
                    kd.setNilaiUas(BigDecimal.ZERO);
                    kd.setJumlahKehadiran(0);
                    kd.setJumlahMangkir(0);
                    kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                    kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                    kd.setJumlahTerlambat(0);
                    kd.setJumlahIzin(0);
                    kd.setJumlahSakit(0);
                    kd.setStatusEdom(StatusRecord.UNDONE);
                    kd.setStatus(StatusRecord.AKTIF);
                    kd.setTahunAkademik(tahunJadwal);
                    krsDetailDao.save(kd);
                }
            }

        }

        return "redirect:list";

    }

    @PostMapping("/studiesActivity/sp/reject")
    public String rejectSp(@RequestParam(required = false) List<String> id,
                           @RequestParam(required = false) String[] checkMhsRe, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        }

        List<String> listMhs = new ArrayList<>();
        for (String list : checkMhsRe) {
            listMhs.add(list);
        }
        System.out.println("list mahasiswa : " + listMhs);

        praKrsSpDao.updateReject(karyawan, listMhs);

        return "redirect:list";
    }

    @GetMapping("/studiesActivity/sp/custom")
    public void customKelas(Model model, @RequestParam String id,
                            @RequestParam(required = false) List<String> matkul,
                            @RequestParam(required = false) String jenis, @RequestParam(required = false) String jumlah,
                            @RequestParam(required = false) String maks) {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }

        model.addAttribute("listMahasiswa", matkul);
        System.out.println("list matkul 1 : " + matkul);

        MatakuliahKurikulum mk = matakuliahKurikulumDao.findById(id).get();
        model.addAttribute("matkul", mk);
        if (jenis != null) {
            model.addAttribute("jadwal",
                    jadwalDao.findByMatakuliahKurikulumAndStatus(mk, StatusRecord.PREVIEW));
            model.addAttribute("jenis", jenis);
            model.addAttribute("jumlah", jumlah);
            model.addAttribute("maks", maks);
            model.addAttribute("listMahasiswa", matkul);
            model.addAttribute("listProdi", prodiDao.findByStatus(StatusRecord.AKTIF));
            model.addAttribute("listDosen", dosenDao.findByStatusNotIn(Arrays.asList(StatusRecord.HAPUS)));
            model.addAttribute("listTahun",
                    tahunAkademikDao.findByStatusNotInOrderByTahunDesc(
                            Arrays.asList(StatusRecord.HAPUS)));
            // model.addAttribute("listKelas",
            // kelasDao.findByStatusAndNamaKelasContainingIgnoreCaseOrderByNamaKelas(StatusRecord.AKTIF,
            // "SP"));
        }

    }

    @PostMapping("/studiesActivity/sp/custom")
    public String kelasCustom(@RequestParam String id, @RequestParam List<String> matkul,
                              @RequestParam(required = false) String jenis, @RequestParam(required = false) String jumlah,
                              @RequestParam(required = false) String maks, Authentication authentication,
                              RedirectAttributes attributes) {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        MatakuliahKurikulum mk = matakuliahKurikulumDao.findById(id).get();
        List<Jadwal> cekPreview = jadwalDao.findByMatakuliahKurikulumAndStatus(mk, StatusRecord.PREVIEW);
        for (Jadwal j : cekPreview) {
            jadwalDao.delete(j);
        }

        List<Object[]> m = praKrsSpDao.listAllSpPerMatkul(tahun, matkul);
        System.out.println("cek list: " + m);
        List<String> listId = new ArrayList<>();
        for (Object[] pks : m) {
            listId.add(pks[9].toString());
        }
        praKrsSpDao.updateSp(karyawan, listId);
        System.out.println("mhs : " + listId);

        int j = Integer.parseInt(jumlah);
        for (int i = 1; i <= j; i++) {
            Jadwal jadwal = new Jadwal();
            jadwal.setJumlahSesi(1);
            jadwal.setBobotTugas(BigDecimal.ZERO);
            jadwal.setBobotUas(BigDecimal.ZERO);
            jadwal.setBobotUts(BigDecimal.ZERO);
            jadwal.setBobotPresensi(BigDecimal.ZERO);
            jadwal.setMatakuliahKurikulum(mk);
            jadwal.setStatusUas(StatusApprove.NOT_UPLOADED_YET);
            jadwal.setProgram(programDao.findById("01").get());
            jadwal.setAkses(Akses.UMUM);
            jadwal.setStatusUts(StatusApprove.NOT_UPLOADED_YET);
            jadwal.setStatus(StatusRecord.PREVIEW);
            jadwalDao.save(jadwal);
        }

        attributes.addFlashAttribute("testMatkul", matkul);
        return "redirect:custom?id=" + id + "&jenis=" + jenis + "&jumlah=" + jumlah + "&maks=" + maks;
    }

    @Transactional
    @PostMapping("/studiesActivity/sp/cancel")
    public String cancelKelas(@RequestParam String id, @RequestParam List<String> matkul,
                              Authentication authentication) {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }

        User user = currentUserService.currentUser(authentication);
        Karyawan karyawan = karyawanDao.findByIdUser(user);
        MatakuliahKurikulum mk = matakuliahKurikulumDao.findById(id).get();

        List<Jadwal> cekJadwal = jadwalDao.findByMatakuliahKurikulumAndStatus(mk, StatusRecord.PREVIEW);
        for (Jadwal j : cekJadwal) {
            jadwalDao.delete(j);
        }

        List<Object[]> m = praKrsSpDao.listAllSpPerMatkul(tahun, matkul);
        System.out.println("cek list: " + m);
        List<String> listId = new ArrayList<>();
        for (Object[] pks : m) {
            listId.add(pks[9].toString());
        }
        System.out.println("mhs : " + listId);
        praKrsSpDao.updateStatus(karyawan, listId, "WAITING");

        return "redirect:list";

    }

    @Transactional
    @PostMapping("/studiesActivity/sp/custom/submit")
    public String submitCustom(@RequestParam String id, @RequestParam List<String> matkul,
                               @RequestParam(required = false) String jenis, @RequestParam(required = false) String jumlah,
                               @RequestParam(required = false) String maks,
                               HttpServletRequest request, Authentication authentication) {

        TahunAkademik semesterPendek = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF,
                StatusRecord.PENDEK);
        if (semesterPendek == null) {
            semesterPendek = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF,
                    StatusRecord.PENDEK);
        }

        MatakuliahKurikulum mk = matakuliahKurikulumDao.findById(id).get();
        List<Jadwal> jadwal = jadwalDao.findByMatakuliahKurikulumAndStatus(mk, StatusRecord.PREVIEW);
        for (Jadwal j : jadwal) {
            String pilihan = request.getParameter("dosen-" + j.getId());
            if (pilihan != null && !pilihan.trim().isEmpty()) {
                User user = currentUserService.currentUser(authentication);
                Karyawan karyawan = karyawanDao.findByIdUser(user);
                TahunAkademik tahun = tahunAkademikDao
                        .findById(request.getParameter("tahun-" + j.getId())).get();
                Prodi prodi = prodiDao.findById(request.getParameter("prodi-" + j.getId())).get();
                TahunAkademikProdi tahunProdi = tahunProdiDao.findByTahunAkademikAndProdi(tahun, prodi);
                SpDto m = praKrsSpDao.tampilPerMatkul(mk.getId());

                System.out.println("jenis : " + jenis);

                if (jenis.equals("campur")) {

                    Kelas kelas = kelasDao.findById("SP-01").get();
                    Dosen dosen = dosenDao.findById(pilihan).get();

                    Jadwal jdwl = jadwalDao.findById(j.getId()).get();
                    jdwl.setKelas(kelas);
                    jdwl.setTahunAkademik(tahun);
                    jdwl.setProdi(prodi);
                    jdwl.setTahunAkademikProdi(tahunProdi);
                    jdwl.setDosen(dosen);
                    jdwl.setStatus(StatusRecord.AKTIF);
                    jadwalDao.save(jdwl);

                    JadwalDosen jd = new JadwalDosen();
                    jd.setStatusJadwalDosen(StatusJadwalDosen.PENGAMPU);
                    jd.setJadwal(jdwl);
                    jd.setDosen(dosen);
                    jd.setJumlahIzin(0);
                    jd.setJumlahKehadiran(0);
                    jd.setJumlahMangkir(0);
                    jd.setJumlahSakit(0);
                    jd.setJumlahTerlambat(0);
                    jadwalDosenDao.save(jd);

                    List<Object[]> spLunas = praKrsSpDao.listLunasSpPerMatkul(tahun, matkul);
                    System.out.println("tes lunas : " + spLunas);
                    for (Object[] listLunas : spLunas) {
                        Mahasiswa mhs = mahasiswaDao.findByNim(listLunas[2].toString());
                        TahunAkademikProdi tap = tahunProdiDao
                                .findByTahunAkademikAndProdi(tahun, mhs.getIdProdi());
                        KrsDetail krsDetail = krsDetailDao
                                .findByMahasiswaAndTahunAkademikAndJadwalAndStatus(mhs,
                                        tahun,
                                        jdwl, StatusRecord.AKTIF);
                        Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mhs, tahun,
                                StatusRecord.AKTIF);
                        if (krsDetail == null) {
                            if (k == null) {
                                Krs krs = new Krs();
                                krs.setTahunAkademikProdi(tap);
                                krs.setProdi(mhs.getIdProdi());
                                krs.setMahasiswa(mhs);
                                krs.setNim(mhs.getNim());
                                krs.setTanggalTransaksi(LocalDateTime.now());
                                krs.setStatus(StatusRecord.AKTIF);
                                krsDao.save(krs);

                                KrsDetail kd = new KrsDetail();
                                kd.setKrs(krs);
                                kd.setMahasiswa(mhs);
                                kd.setJadwal(jdwl);
                                kd.setMatakuliahKurikulum(mk);
                                kd.setNilaiPresensi(BigDecimal.ZERO);
                                kd.setNilaiUts(BigDecimal.ZERO);
                                kd.setNilaiTugas(BigDecimal.ZERO);
                                kd.setFinalisasi("N");
                                kd.setNilaiUas(BigDecimal.ZERO);
                                kd.setJumlahKehadiran(0);
                                kd.setJumlahMangkir(0);
                                kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                                kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                                kd.setJumlahTerlambat(0);
                                kd.setJumlahIzin(0);
                                kd.setJumlahSakit(0);
                                kd.setStatusEdom(StatusRecord.UNDONE);
                                kd.setStatus(StatusRecord.AKTIF);
                                kd.setTahunAkademik(tahun);
                                krsDetailDao.save(kd);

                            } else {
                                KrsDetail kd = new KrsDetail();
                                kd.setKrs(k);
                                kd.setMahasiswa(mhs);
                                kd.setJadwal(jdwl);
                                kd.setMatakuliahKurikulum(mk);
                                kd.setNilaiPresensi(BigDecimal.ZERO);
                                kd.setNilaiUts(BigDecimal.ZERO);
                                kd.setNilaiTugas(BigDecimal.ZERO);
                                kd.setFinalisasi("N");
                                kd.setNilaiUas(BigDecimal.ZERO);
                                kd.setJumlahKehadiran(0);
                                kd.setJumlahMangkir(0);
                                kd.setKodeUts(RandomStringUtils.randomAlphanumeric(5));
                                kd.setKodeUas(RandomStringUtils.randomAlphanumeric(5));
                                kd.setJumlahTerlambat(0);
                                kd.setJumlahIzin(0);
                                kd.setJumlahSakit(0);
                                kd.setStatusEdom(StatusRecord.UNDONE);
                                kd.setStatus(StatusRecord.AKTIF);
                                kd.setTahunAkademik(tahun);
                                krsDetailDao.save(kd);
                            }
                        }

                    }

                } else if (jenis.equals("pisah")) {
                    Kelas kelas = kelasDao.findById(request.getParameter("kelas-" + j.getId()))
                            .get();
                    Dosen dosen = dosenDao.findById(pilihan).get();

                    Jadwal jdwl = jadwalDao.findById(j.getId()).get();
                    jdwl.setKelas(kelas);
                    jdwl.setTahunAkademik(tahun);
                    jdwl.setProdi(prodi);
                    jdwl.setTahunAkademikProdi(tahunProdi);
                    jdwl.setDosen(dosen);
                    jdwl.setStatus(StatusRecord.AKTIF);
                    jadwalDao.save(jdwl);

                    JadwalDosen jd = new JadwalDosen();
                    jd.setStatusJadwalDosen(StatusJadwalDosen.PENGAMPU);
                    jd.setJadwal(jdwl);
                    jd.setDosen(dosen);
                    jd.setJumlahIzin(0);
                    jd.setJumlahKehadiran(0);
                    jd.setJumlahMangkir(0);
                    jd.setJumlahSakit(0);
                    jd.setJumlahTerlambat(0);
                    jadwalDosenDao.save(jd);

                    if (kelas.getKodeKelas().equals("SP-01-AKHWAT")) {
                        List<Object[]> listLunas = praKrsSpDao.listLunasPisahKelas(tahun,
                                matkul, "WANITA");
                        System.out.println("tes lunas : " + listLunas);
                        for (Object[] akhwatLunas : listLunas) {
                            Mahasiswa mhs = mahasiswaDao
                                    .findByNim(akhwatLunas[2].toString());
                            TahunAkademikProdi tap = tahunProdiDao
                                    .findByTahunAkademikAndProdi(tahun,
                                            mhs.getIdProdi());
                            KrsDetail krsDetail = krsDetailDao
                                    .findByMahasiswaAndTahunAkademikAndJadwalAndStatus(
                                            mhs,
                                            tahun, jdwl,
                                            StatusRecord.AKTIF);
                            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mhs,
                                    tahun, StatusRecord.AKTIF);
                            if (krsDetail == null) {
                                if (k == null) {
                                    Krs krs = new Krs();
                                    krs.setTahunAkademik(tahun);
                                    krs.setProdi(mhs.getIdProdi());
                                    krs.setTahunAkademikProdi(tap);
                                    krs.setMahasiswa(mhs);
                                    krs.setNim(mhs.getNim());
                                    krs.setTanggalTransaksi(LocalDateTime.now());
                                    krs.setStatus(StatusRecord.AKTIF);
                                    krsDao.save(krs);

                                    KrsDetail kd = new KrsDetail();
                                    kd.setKrs(krs);
                                    kd.setMahasiswa(mhs);
                                    kd.setJadwal(jdwl);
                                    kd.setMatakuliahKurikulum(mk);
                                    kd.setNilaiPresensi(BigDecimal.ZERO);
                                    kd.setNilaiUts(BigDecimal.ZERO);
                                    kd.setNilaiUas(BigDecimal.ZERO);
                                    kd.setNilaiTugas(BigDecimal.ZERO);
                                    kd.setFinalisasi("N");
                                    kd.setJumlahKehadiran(0);
                                    kd.setJumlahMangkir(0);
                                    kd.setJumlahTerlambat(0);
                                    kd.setJumlahIzin(0);
                                    kd.setJumlahSakit(0);
                                    kd.setKodeUts(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setKodeUas(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setStatusEdom(StatusRecord.UNDONE);
                                    kd.setStatus(StatusRecord.AKTIF);
                                    kd.setTahunAkademik(tahun);
                                    krsDetailDao.save(kd);
                                } else {
                                    KrsDetail kd = new KrsDetail();
                                    kd.setKrs(k);
                                    kd.setMahasiswa(mhs);
                                    kd.setJadwal(jdwl);
                                    kd.setMatakuliahKurikulum(mk);
                                    kd.setNilaiPresensi(BigDecimal.ZERO);
                                    kd.setNilaiUts(BigDecimal.ZERO);
                                    kd.setNilaiUas(BigDecimal.ZERO);
                                    kd.setNilaiTugas(BigDecimal.ZERO);
                                    kd.setFinalisasi("N");
                                    kd.setJumlahKehadiran(0);
                                    kd.setJumlahMangkir(0);
                                    kd.setJumlahTerlambat(0);
                                    kd.setJumlahIzin(0);
                                    kd.setJumlahSakit(0);
                                    kd.setKodeUts(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setKodeUas(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setStatusEdom(StatusRecord.UNDONE);
                                    kd.setStatus(StatusRecord.AKTIF);
                                    kd.setTahunAkademik(tahun);
                                    krsDetailDao.save(kd);
                                }
                            }

                        }

                    } else if (kelas.getKodeKelas().equals("SP-01-IKHWAN")) {
                        List<Object[]> listLunas = praKrsSpDao.listLunasPisahKelas(tahun,
                                matkul, "PRIA");
                        System.out.println("tes lunas : " + listLunas);
                        for (Object[] listIkhwan : listLunas) {
                            Mahasiswa mhs = mahasiswaDao
                                    .findByNim(listIkhwan[2].toString());
                            TahunAkademikProdi tap = tahunProdiDao
                                    .findByTahunAkademikAndProdi(tahun,
                                            mhs.getIdProdi());
                            KrsDetail krsDetail = krsDetailDao
                                    .findByMahasiswaAndTahunAkademikAndJadwalAndStatus(
                                            mhs,
                                            tahun, jdwl,
                                            StatusRecord.AKTIF);
                            Krs k = krsDao.findByMahasiswaAndTahunAkademikAndStatus(mhs,
                                    tahun, StatusRecord.AKTIF);
                            if (krsDetail == null) {
                                if (k == null) {
                                    Krs krs = new Krs();
                                    krs.setMahasiswa(mhs);
                                    krs.setTahunAkademik(tahun);
                                    krs.setProdi(mhs.getIdProdi());
                                    krs.setTahunAkademikProdi(tap);
                                    krs.setNim(mhs.getNim());
                                    krs.setTanggalTransaksi(LocalDateTime.now());
                                    krs.setStatus(StatusRecord.AKTIF);
                                    krsDao.save(krs);

                                    KrsDetail kd = new KrsDetail();
                                    kd.setKrs(krs);
                                    kd.setMahasiswa(mhs);
                                    kd.setJadwal(jdwl);
                                    kd.setMatakuliahKurikulum(mk);
                                    kd.setNilaiPresensi(BigDecimal.ZERO);
                                    kd.setNilaiUas(BigDecimal.ZERO);
                                    kd.setNilaiUts(BigDecimal.ZERO);
                                    kd.setNilaiTugas(BigDecimal.ZERO);
                                    kd.setFinalisasi("N");
                                    kd.setJumlahKehadiran(0);
                                    kd.setJumlahMangkir(0);
                                    kd.setKodeUts(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setKodeUas(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setJumlahTerlambat(0);
                                    kd.setJumlahIzin(0);
                                    kd.setJumlahSakit(0);
                                    kd.setStatusEdom(StatusRecord.UNDONE);
                                    kd.setStatus(StatusRecord.AKTIF);
                                    kd.setTahunAkademik(tahun);
                                    krsDetailDao.save(kd);

                                } else {
                                    KrsDetail kd = new KrsDetail();
                                    kd.setKrs(k);
                                    kd.setMahasiswa(mhs);
                                    kd.setJadwal(jdwl);
                                    kd.setMatakuliahKurikulum(mk);
                                    kd.setNilaiPresensi(BigDecimal.ZERO);
                                    kd.setNilaiUas(BigDecimal.ZERO);
                                    kd.setNilaiUts(BigDecimal.ZERO);
                                    kd.setNilaiTugas(BigDecimal.ZERO);
                                    kd.setFinalisasi("N");
                                    kd.setJumlahKehadiran(0);
                                    kd.setJumlahMangkir(0);
                                    kd.setKodeUts(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setKodeUas(RandomStringUtils
                                            .randomAlphanumeric(5));
                                    kd.setJumlahTerlambat(0);
                                    kd.setJumlahIzin(0);
                                    kd.setJumlahSakit(0);
                                    kd.setStatusEdom(StatusRecord.UNDONE);
                                    kd.setStatus(StatusRecord.AKTIF);
                                    kd.setTahunAkademik(tahun);
                                    krsDetailDao.save(kd);
                                }
                            }
                        }
                    }

                }
            }
        }
        return "redirect:../list";

    }

    @GetMapping("/studiesActivity/sp/detail")
    public void spDetail(Model model) {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }
        List<Object[]> detailSp = praKrsSpDao.allDetail(tahun);
        model.addAttribute("detailSp", detailSp);

    }

    @GetMapping("/download/detail/sp")
    public void listDetail(HttpServletResponse response) throws IOException {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }
        List<Object[]> listDetail = praKrsSpDao.allDetail(tahun);

        String[] columns = {"No", "Nama", "NIM", "Prodi", "Matakuliah", "SKS", "Pembayaran",
                "Tanggal Pembayaran"};

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("List detail mahasiswa request SP");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        Row headerRow = sheet.createRow(0);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 1;
        int baris = 1;

        for (Object[] detail : listDetail) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(detail[3].toString());
            row.createCell(2).setCellValue(detail[2].toString());
            row.createCell(3).setCellValue(detail[4].toString());
            row.createCell(4).setCellValue(detail[6].toString());
            row.createCell(5).setCellValue(detail[7].toString());
            row.createCell(6).setCellValue(detail[10].toString());
            row.createCell(7).setCellValue(detail[11].toString());
        }

        for (int i = 1; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition",
                "attachment; filename=Detail_MHS_SP_" + LocalDate.now() + ".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    @GetMapping("/download/list")
    public void listPerMatkul(@RequestParam(required = false) String matkul, HttpServletResponse response)
            throws IOException {

        TahunAkademik tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.AKTIF, StatusRecord.PENDEK);
        if (tahun == null) {
            tahun = tahunAkademikDao.findByStatusAndJenis(StatusRecord.PRAAKTIF, StatusRecord.PENDEK);
        }
        MatakuliahKurikulum matkur = matakuliahKurikulumDao.findById(matkul).get();
        SpDto m = praKrsSpDao.tampilPerMatkul(matkur.getId());
        List<Object[]> listDownload = praKrsSpDao.excelDownlaod(tahun, m.getId(), m.getIdMatakuliah(),
                m.getKode(),
                m.getNamaMatakuliah());

        String[] columns = {"No", "Nim", "Nama", "Prodi", "Nomor Telepon", "Status Pembayaran",
                "Tanggal Pembayaran"};

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("List Mahasiswa Request SP");

        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setColor(IndexedColors.BLACK.getIndex());

        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        sheet.createRow(0).createCell(2)
                .setCellValue("Matakuliah : " + matkur.getMatakuliah().getNamaMatakuliah());

        Row headerRow = sheet.createRow(2);

        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        int rowNum = 3;
        int baris = 1;

        for (Object[] list : listDownload) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(baris++);
            row.createCell(1).setCellValue(list[2].toString());
            row.createCell(2).setCellValue(list[3].toString());
            row.createCell(3).setCellValue(list[5].toString());
            row.createCell(4).setCellValue(list[4].toString());
            row.createCell(5).setCellValue(list[9].toString());
            row.createCell(6).setCellValue(list[10].toString());
        }

        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=List_Request_SP_Matkul_"
                + matkur.getMatakuliah().getNamaMatakuliah() + "_" + LocalDate.now() + ".xlsx");
        workbook.write(response.getOutputStream());
        workbook.close();

    }

    // Status KRS
    @GetMapping("/studiesActivity/krs/status")
    public void statusKrs(Model model, @RequestParam(required = false) Prodi prodi,
                          @RequestParam(required = false) String angkatan,
                          @RequestParam(required = false) Dosen dosen, @PageableDefault(size = 10) Pageable page) {

        TahunAkademik tahun = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);

        model.addAttribute("selectProdi", prodi);
        model.addAttribute("selectAngkatan", angkatan);
        model.addAttribute("selectDosen", dosen);

        if (prodi != null && angkatan != null && dosen == null) {
            model.addAttribute("listMahasiswa",
                    krsDao.listKrsAdmin(angkatan, prodi.getId(), tahun.getId(), page));
        }

        if (prodi != null && angkatan != null && dosen != null) {
            model.addAttribute("listMahasiswa", krsDao.listKrsAdminByDosenWali(angkatan, prodi.getId(),
                    tahun.getId(), dosen.getId(), page));
        }
    }

    // Status Pembayaran KRS
    @GetMapping("/studiesActivity/krs/statusPembayaran")
    public void statusPembayaranKrs(Model model, @RequestParam(required = false) Prodi prodi,
                                    @RequestParam(required = false) String angkatan, @RequestParam(required = false) Dosen dosen,
                                    String search, @PageableDefault(size = 10) Pageable page) {

        TahunAkademik tahunAkademik = tahunAkademikDao.findByStatus(StatusRecord.AKTIF);

        model.addAttribute("selectProdi", prodi);
        model.addAttribute("selectAngkatan", angkatan);
//                model.addAttribute("selectDosen", dosen);


        if (prodi != null) {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listTotalSks", krsDetailDao.listTotalSks1ProdiSearch(tahunAkademik, search, prodi, angkatan, page));
            } else {
                model.addAttribute("listTotalSks", krsDetailDao.listTotalSks1Prodi(tahunAkademik, prodi, angkatan, page));
            }
        } else {
            if (StringUtils.hasText(search)) {
                model.addAttribute("search", search);
                model.addAttribute("listTotalSks", krsDetailDao.listTotalSks1Search(tahunAkademik, search, angkatan, page));
            } else {
                model.addAttribute("listTotalSks", krsDetailDao.listTotalSks1(tahunAkademik, angkatan, page));
            }
        }

    }

    @PostMapping("/studiesActivity/krs/statusPembayaran/statusAktif/save/{id}")
    @ResponseBody
    public void prosesPloting(@PathVariable String id, @RequestParam(required = false) String statusAktif) {
        Mahasiswa mahasiswa = mahasiswaDao.findById(id).get();

        mahasiswa.setStatusAktif(statusAktif);
        mahasiswaDao.save(mahasiswa);

    }


    @GetMapping("/studiesActivity/transcript/transkriptindo2")
    public void transkriptFormat2(@RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

        // Long totalSKS = krsDetailDao.totalSks(mahasiswa);
        // BigDecimal ipk = totalMuti.divide(totalSKS,2,BigDecimal.ROUND_HALF_DOWN);

        // file
        List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mahasiswa);
        listTranskript.removeIf(e -> e.getGrade().equals("E"));

        int totalSKS = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();

        BigDecimal totalMuti = listTranskript.stream().map(DataTranskript::getMutu)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal ipk = totalMuti.divide(new BigDecimal(totalSKS), 2, BigDecimal.ROUND_HALF_UP);

        List<DataTranskript> semester1 = new ArrayList<>();
        List<DataTranskript> semester2 = new ArrayList<>();
        List<DataTranskript> semester3 = new ArrayList<>();
        List<DataTranskript> semester4 = new ArrayList<>();
        List<DataTranskript> semester5 = new ArrayList<>();
        List<DataTranskript> semester6 = new ArrayList<>();
        List<DataTranskript> semester7 = new ArrayList<>();
        List<DataTranskript> semester8 = new ArrayList<>();

        for (DataTranskript data : listTranskript) {
            if (data.getSemester().equals("1")) {
                semester1.add(data);
            }
            if (data.getSemester().equals("2")) {
                semester2.add(data);
            }
            if (data.getSemester().equals("3")) {
                semester3.add(data);
            }
            if (data.getSemester().equals("4")) {
                semester4.add(data);
            }
            if (data.getSemester().equals("5")) {
                semester5.add(data);
            }
            if (data.getSemester().equals("6")) {
                semester6.add(data);
            }
            if (data.getSemester().equals("7")) {
                semester7.add(data);
            }
            if (data.getSemester().equals("8")) {
                semester8.add(data);
            }
        }

        InputStream file = contohExcelTranskriptIndo.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());

        sheet.addMergedRegion(CellRangeAddress.valueOf("A7:C7"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:C8"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A9:C9"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A10:C10"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A11:C11"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A12:C12"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A13:C13"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A14:C14"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A15:C15"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A16:C16"));

        Font manajemenFont = workbook.createFont();
        manajemenFont.setItalic(true);
        manajemenFont.setFontHeightInPoints((short) 10);
        manajemenFont.setFontName("Cambria");

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 10);
        dataManajemenFont.setFontName("Cambria");

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 10);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 10);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        Font prodiFont = workbook.createFont();
        prodiFont.setUnderline(XSSFFont.U_DOUBLE);
        prodiFont.setFontHeightInPoints((short) 10);
        prodiFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setItalic(true);
        ipFont.setFontHeightInPoints((short) 10);
        ipFont.setFontName("Cambria");

        Font lectureFont = workbook.createFont();
        lectureFont.setBold(true);
        lectureFont.setFontName("Cambria");
        lectureFont.setUnderline(XSSFFont.U_DOUBLE);
        lectureFont.setFontHeightInPoints((short) 10);

        Font nikFont = workbook.createFont();
        nikFont.setBold(true);
        nikFont.setFontName("Cambria");
        nikFont.setFontHeightInPoints((short) 10);

        CellStyle styleNik = workbook.createCellStyle();
        styleNik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNik.setFont(nikFont);

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleManajemen.setAlignment(HorizontalAlignment.CENTER);
        styleManajemen.setFont(manajemenFont);

        CellStyle styleDosen = workbook.createCellStyle();
        styleDosen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDosen.setFont(lectureFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setBorderTop(BorderStyle.MEDIUM);
        styleProdi.setBorderBottom(BorderStyle.MEDIUM);
        styleProdi.setBorderLeft(BorderStyle.MEDIUM);
        styleProdi.setBorderRight(BorderStyle.MEDIUM);
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhs.setFont(dataFont);

        CellStyle stylePrestasiAkademik = workbook.createCellStyle();
        stylePrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        stylePrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePrestasiAkademik.setFont(dataFont);

        CellStyle styleSubHeader1 = workbook.createCellStyle();
        styleSubHeader1.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader1.setVerticalAlignment(VerticalAlignment.CENTER);
        styleSubHeader1.setFont(ipFont);

        CellStyle styleJudulSkripsi = workbook.createCellStyle();
        styleJudulSkripsi.setFont(dataFont);
        styleJudulSkripsi.setWrapText(true);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.CENTER);
        styleSymbol.setFont(symbolFont);

        CellStyle styleTotal = workbook.createCellStyle();
        styleTotal.setAlignment(HorizontalAlignment.CENTER);
        styleTotal.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotal.setFont(ipFont);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIpk.setVerticalAlignment(VerticalAlignment.CENTER);

        int rowInfoNama = 5;
        Row nama = sheet.createRow(rowInfoNama);
        nama.createCell(0).setCellValue("Nama Mahasiswa ");
        nama.createCell(3).setCellValue(":");
        nama.createCell(4).setCellValue(mahasiswa.getNama());
        nama.getCell(0).setCellStyle(styleData);
        nama.getCell(3).setCellStyle(styleSymbol);
        nama.getCell(4).setCellStyle(styleData);

        int rowInfoNim = 6;
        Row matricNo = sheet.createRow(rowInfoNim);
        matricNo.createCell(0).setCellValue("NIM");
        matricNo.createCell(3).setCellValue(":");
        matricNo.createCell(4).setCellValue(mahasiswa.getNim());
        matricNo.getCell(0).setCellStyle(styleData);
        matricNo.getCell(3).setCellStyle(styleSymbol);
        matricNo.getCell(4).setCellStyle(styleData);

        int rowInfoEntry = 7;
        Row entry = sheet.createRow(rowInfoEntry);
        entry.createCell(0).setCellValue("Penomoran Induk Nasional");
        entry.createCell(3).setCellValue(":");
        entry.createCell(4).setCellValue(mahasiswa.getIndukNasional());
        entry.getCell(0).setCellStyle(styleData);
        entry.getCell(3).setCellStyle(styleSymbol);
        entry.getCell(4).setCellStyle(styleData);

        int rowInfoBirth = 8;
        Row birthDay = sheet.createRow(rowInfoBirth);
        birthDay.createCell(0).setCellValue("Tempat, Tanggal lahir");
        birthDay.createCell(3).setCellValue(":");

        birthDay.createCell(4).setCellValue(mahasiswa.getTempatLahir() + ","
                + transcriptService.convertDateIndonesia(mahasiswa.getTanggalLahir()));
        birthDay.getCell(4).setCellStyle(styleData);

        birthDay.getCell(0).setCellStyle(styleData);
        birthDay.getCell(3).setCellStyle(styleSymbol);

        int rowInfoLevel = 9;
        Row level = sheet.createRow(rowInfoLevel);
        level.createCell(0).setCellValue("Program Pendidikan");
        level.createCell(3).setCellValue(":");

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            level.createCell(4).setCellValue("Sarjana");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            level.createCell(4).setCellValue("Magister");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("03").get()) {
            level.createCell(4).setCellValue("Sarjana");
            level.getCell(4).setCellStyle(styleData);

        }
        level.getCell(0).setCellStyle(styleData);
        level.getCell(3).setCellStyle(styleSymbol);

        int rowInfoDepartment = 10;
        Row department = sheet.createRow(rowInfoDepartment);
        department.createCell(0).setCellValue("Program Studi");
        department.createCell(3).setCellValue(":");
        department.createCell(4).setCellValue(mahasiswa.getIdProdi().getNamaProdi());
        department.getCell(0).setCellStyle(styleData);
        department.getCell(3).setCellStyle(styleSymbol);
        department.getCell(4).setCellStyle(styleData);

        int rowInfoFaculty = 11;
        Row faculty = sheet.createRow(rowInfoFaculty);
        faculty.createCell(0).setCellValue("Fakultas");
        faculty.createCell(3).setCellValue(":");
        faculty.createCell(4).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        faculty.getCell(0).setCellStyle(styleData);
        faculty.getCell(3).setCellStyle(styleSymbol);
        faculty.getCell(4).setCellStyle(styleData);

        int rowInfoNoAcred = 12;
        Row accreditation = sheet.createRow(rowInfoNoAcred);
        accreditation.createCell(0).setCellValue("No SK BAN - PT");
        accreditation.createCell(3).setCellValue(":");
        accreditation.createCell(4).setCellValue(mahasiswa.getIdProdi().getNoSk());
        accreditation.getCell(0).setCellStyle(styleData);
        accreditation.getCell(3).setCellStyle(styleSymbol);
        accreditation.getCell(4).setCellStyle(styleData);

        int rowInfoDateAcred = 13;
        Row dateAccreditation = sheet.createRow(rowInfoDateAcred);
        dateAccreditation.createCell(0).setCellValue("Tanggal SK BAN - PT ");
        dateAccreditation.createCell(3).setCellValue(":");
        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 1) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Januari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 2) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Februari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 3) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Maret" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 4) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " April" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 5) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Mei"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 6) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juni"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 7) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juli"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 8) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Agustus" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 9) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " September" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 10) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Oktober" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 11) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " November" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 12) {
            dateAccreditation.createCell(4)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Desember" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(4).setCellStyle(styleData);

        }

        dateAccreditation.getCell(0).setCellStyle(styleData);
        dateAccreditation.getCell(3).setCellStyle(styleSymbol);

        int rowInfoGraduatedDate = 14;
        Row graduatedDate = sheet.createRow(rowInfoGraduatedDate);
        graduatedDate.createCell(0).setCellValue("Tanggal Kelulusan");
        graduatedDate.createCell(3).setCellValue(":");
        if (mahasiswa.getTanggalLulus().getMonthValue() == 1) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Januari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 2) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Februari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 3) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Maret" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 4) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " April" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 5) {
            graduatedDate.createCell(4).setCellValue(
                    mahasiswa.getTanggalLulus().getDayOfMonth() + " Mei" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 6) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juni" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 7) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juli" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 8) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Agustus" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 9) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " September" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 10) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Oktober" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 11) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " November" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 12) {
            graduatedDate.createCell(4)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Desember" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(4).setCellStyle(styleData);

        }

        graduatedDate.getCell(0).setCellStyle(styleData);
        graduatedDate.getCell(3).setCellStyle(styleSymbol);

        int rowInfoTranscript = 15;
        Row transcript = sheet.createRow(rowInfoTranscript);
        transcript.createCell(0).setCellValue("No Transkrip ");
        transcript.createCell(3).setCellValue(":");
        transcript.createCell(4).setCellValue(mahasiswa.getNoTranskript());
        transcript.getCell(0).setCellStyle(styleData);
        transcript.getCell(3).setCellStyle(styleSymbol);
        transcript.getCell(4).setCellStyle(styleData);

        int rowNumSemester1 = 18;
        for (DataTranskript sem1 : semester1) {
            Row row = sheet.createRow(rowNumSemester1);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 7, 8));

            row.createCell(0).setCellValue(sem1.getKode());
            row.createCell(1).setCellValue(sem1.getMatkul());
            row.createCell(5).setCellValue(sem1.getSks());
            row.createCell(6).setCellValue(sem1.getGrade());
            row.createCell(7).setCellValue(sem1.getBobot().toString());
            row.createCell(9).setCellValue(sem1.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester1++;
        }

        int rowNumSemester2 = 18 + semester1.size();
        for (DataTranskript sem2 : semester2) {
            Row row = sheet.createRow(rowNumSemester2);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 7, 8));

            row.createCell(0).setCellValue(sem2.getKode());
            row.createCell(1).setCellValue(sem2.getMatkul());
            row.createCell(5).setCellValue(sem2.getSks());
            row.createCell(6).setCellValue(sem2.getGrade());
            row.createCell(7).setCellValue(sem2.getBobot().toString());
            row.createCell(9).setCellValue(sem2.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester2++;
        }

        int rowNumSemester3 = 18 + semester1.size() + semester2.size();
        for (DataTranskript sem3 : semester3) {
            Row row = sheet.createRow(rowNumSemester3);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 7, 8));

            row.createCell(0).setCellValue(sem3.getKode());
            row.createCell(1).setCellValue(sem3.getMatkul());
            row.createCell(5).setCellValue(sem3.getSks());
            row.createCell(6).setCellValue(sem3.getGrade());
            row.createCell(7).setCellValue(sem3.getBobot().toString());
            row.createCell(9).setCellValue(sem3.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester3++;
        }

        int rowNumSemester4 = 18 + semester1.size() + semester2.size() + semester3.size();
        for (DataTranskript sem4 : semester4) {
            Row row = sheet.createRow(rowNumSemester4);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 7, 8));

            row.createCell(0).setCellValue(sem4.getKode());
            row.createCell(1).setCellValue(sem4.getMatkul());
            row.createCell(5).setCellValue(sem4.getSks());
            row.createCell(6).setCellValue(sem4.getGrade());
            row.createCell(7).setCellValue(sem4.getBobot().toString());
            row.createCell(9).setCellValue(sem4.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester4++;
        }

        int rowNumSemester5 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size();
        for (DataTranskript sem5 : semester5) {
            Row row = sheet.createRow(rowNumSemester5);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 7, 8));

            row.createCell(0).setCellValue(sem5.getKode());
            row.createCell(1).setCellValue(sem5.getMatkul());
            row.createCell(5).setCellValue(sem5.getSks());
            row.createCell(6).setCellValue(sem5.getGrade());
            row.createCell(7).setCellValue(sem5.getBobot().toString());
            row.createCell(9).setCellValue(sem5.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester5++;
        }

        int rowNumSemester6 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size();
        for (DataTranskript sem6 : semester6) {
            Row row = sheet.createRow(rowNumSemester6);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 7, 8));

            row.createCell(0).setCellValue(sem6.getKode());
            row.createCell(1).setCellValue(sem6.getMatkul());
            row.createCell(5).setCellValue(sem6.getSks());
            row.createCell(6).setCellValue(sem6.getGrade());
            row.createCell(7).setCellValue(sem6.getBobot().toString());
            row.createCell(9).setCellValue(sem6.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester6++;
        }

        int rowNumSemester7 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size();
        for (DataTranskript sem7 : semester7) {
            Row row = sheet.createRow(rowNumSemester7);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 7, 8));

            row.createCell(0).setCellValue(sem7.getKode());
            row.createCell(1).setCellValue(sem7.getMatkul());
            row.createCell(5).setCellValue(sem7.getSks());
            row.createCell(6).setCellValue(sem7.getGrade());
            row.createCell(7).setCellValue(sem7.getBobot().toString());
            row.createCell(9).setCellValue(sem7.getMutu().toString());
            row.getCell(1).setCellStyle(styleData);
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester7++;
        }

        int rowNumSemester8 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size();
        for (DataTranskript sem8 : semester8) {
            Row row = sheet.createRow(rowNumSemester8);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 7, 8));

            row.createCell(0).setCellValue(sem8.getKode());
            row.createCell(1).setCellValue(sem8.getMatkul());
            row.createCell(5).setCellValue(sem8.getSks());
            row.createCell(6).setCellValue(sem8.getGrade());
            row.createCell(7).setCellValue(sem8.getBobot().toString());
            row.createCell(9).setCellValue(sem8.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester8++;
        }

        int total = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size();
        Row rowTotal = sheet.createRow(total);
        sheet.addMergedRegion(new CellRangeAddress(total, total, 1, 4));
        rowTotal.createCell(1).setCellValue("Jumlah");
        rowTotal.createCell(5).setCellValue(totalSKS);
        rowTotal.createCell(9).setCellValue(totalMuti.toString());
        rowTotal.getCell(1).setCellStyle(styleTotal);
        rowTotal.getCell(5).setCellStyle(styleDataKhs);
        rowTotal.getCell(9).setCellStyle(styleDataKhs);

        int ipKomulatif = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 2;
        Row rowIpk = sheet.createRow(ipKomulatif);
        sheet.addMergedRegion(new CellRangeAddress(ipKomulatif, ipKomulatif, 0, 2));
        rowIpk.createCell(0).setCellValue("Indeks Prestasi Kumulatif");
        rowIpk.createCell(5).setCellValue(ipk.toString());
        rowIpk.getCell(0).setCellStyle(styleTotal);
        rowIpk.getCell(5).setCellStyle(styleDataKhs);

        int predicate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 4;
        Row predicateRow = sheet.createRow(predicate);
        predicateRow.createCell(0).setCellValue("Predikat :");
        if (ipk.compareTo(new BigDecimal(2.99)) <= 0) {
            predicateRow.createCell(1).setCellValue("Memuaskan");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.compareTo(new BigDecimal(3.00)) >= 0 && ipk.compareTo(new BigDecimal(3.49)) <= 0) {
            predicateRow.createCell(1).setCellValue("Sangat Memuaskan");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.compareTo(new BigDecimal(3.50)) >= 0 && ipk.compareTo(new BigDecimal(3.79)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(2).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(2).setCellStyle(styleData);
            } else {
                predicateRow.createCell(2).setCellValue("Pujian ");
                predicateRow.getCell(2).setCellStyle(styleData);
            }

        }

        if (ipk.compareTo(new BigDecimal(3.80)) >= 0 && ipk.compareTo(new BigDecimal(4.00)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(2).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(2).setCellStyle(styleData);
            } else {
                predicateRow.createCell(2).setCellValue("Pujian Tertinggi");
                predicateRow.getCell(2).setCellStyle(styleData);
            }

        }

        predicateRow.getCell(0).setCellStyle(styleSubHeader);

        int thesis = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 5;
        Row thesisRow = sheet.createRow(thesis);
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            thesisRow.createCell(0).setCellValue("Judul skripsi :");
            thesisRow.createCell(1).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(0).setCellStyle(styleSubHeader);
            thesisRow.getCell(1).setCellStyle(styleData);
        }

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            thesisRow.createCell(0).setCellValue("Judul Tesis :");
            thesisRow.createCell(1).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(0).setCellStyle(styleSubHeader);
            thesisRow.getCell(1).setCellStyle(styleData);
        }

        int keyResult = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 8;
        Row resultRow = sheet.createRow(keyResult);
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 5, 9));
        resultRow.createCell(0).setCellValue("Prestasi Akademik");
        resultRow.createCell(5).setCellValue("Sistem Penilaian");
        resultRow.getCell(0).setCellStyle(styleDataKhs);
        resultRow.getCell(5).setCellStyle(styleDataKhs);

        int remark = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 10;
        Row remarkRow = sheet.createRow(remark);
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 7, 9));
        remarkRow.createCell(0).setCellValue("Keterangan");
        remarkRow.createCell(5).setCellValue("HM");
        remarkRow.createCell(6).setCellValue("AM");
        remarkRow.createCell(7).setCellValue("Arti");
        remarkRow.getCell(0).setCellStyle(styleDataKhs);
        remarkRow.getCell(5).setCellStyle(styleIpk);
        remarkRow.getCell(6).setCellStyle(styleIpk);
        remarkRow.getCell(7).setCellStyle(styleIpk);

        int excellent = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 11;
        Row excellentRow = sheet.createRow(excellent);
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 7, 9));
        excellentRow.createCell(0).setCellValue("3,80-4,00");
        excellentRow.createCell(1).setCellValue("Pujian Tertinggi (Minimal B)");
        excellentRow.createCell(5).setCellValue("A");
        excellentRow.createCell(6).setCellValue("4");
        excellentRow.createCell(7).setCellValue("Baik Sekali");
        excellentRow.getCell(0).setCellStyle(styleProdi);
        excellentRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        excellentRow.getCell(5).setCellStyle(styleDataKhs);
        excellentRow.getCell(6).setCellStyle(styleDataKhs);
        excellentRow.getCell(7).setCellStyle(styleManajemen);

        int veryGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 12;
        Row veryGoodRow = sheet.createRow(veryGood);
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 7, 9));
        veryGoodRow.createCell(0).setCellValue("3,50-3,79");
        veryGoodRow.createCell(1).setCellValue("Pujian (Minimal B)");
        veryGoodRow.createCell(5).setCellValue("A-");
        veryGoodRow.createCell(6).setCellValue("3,7");
        veryGoodRow.createCell(7).setCellValue("Baik Sekali");
        veryGoodRow.getCell(0).setCellStyle(styleProdi);
        veryGoodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        veryGoodRow.getCell(5).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(6).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(7).setCellStyle(styleManajemen);

        int good = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 13;
        Row goodRow = sheet.createRow(good);
        sheet.addMergedRegion(new CellRangeAddress(good, good, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(good, good, 7, 9));
        goodRow.createCell(0).setCellValue("3,00-3,49");
        goodRow.createCell(1).setCellValue("Sangat Memuaskan");
        goodRow.createCell(5).setCellValue("B+");
        goodRow.createCell(6).setCellValue("3,3");
        goodRow.createCell(7).setCellValue("Baik");
        goodRow.getCell(0).setCellStyle(styleProdi);
        goodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        goodRow.getCell(5).setCellStyle(styleDataKhs);
        goodRow.getCell(6).setCellStyle(styleDataKhs);
        goodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactory = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 14;
        Row satisfactoryRow = sheet.createRow(satisfactory);
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 7, 9));
        satisfactoryRow.createCell(0).setCellValue("2,75-2,99");
        satisfactoryRow.createCell(1).setCellValue("Memuaskan");
        satisfactoryRow.createCell(5).setCellValue("B");
        satisfactoryRow.createCell(6).setCellValue("3");
        satisfactoryRow.createCell(7).setCellValue("Baik");
        satisfactoryRow.getCell(0).setCellStyle(styleProdi);
        satisfactoryRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        satisfactoryRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(7).setCellStyle(styleManajemen);

        int almostGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 15;
        Row almostGoodRow = sheet.createRow(almostGood);
        sheet.addMergedRegion(new CellRangeAddress(almostGood, almostGood, 7, 9));
        almostGoodRow.createCell(5).setCellValue("B-");
        almostGoodRow.createCell(6).setCellValue("2,7");
        almostGoodRow.createCell(7).setCellValue("Baik");
        almostGoodRow.getCell(5).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(6).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryCplus = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 16;
        Row satisfactoryCplusRow = sheet.createRow(satisfactoryCplus);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryCplus, satisfactoryCplus, 7, 9));
        satisfactoryCplusRow.createCell(5).setCellValue("C+");
        satisfactoryCplusRow.createCell(6).setCellValue("2,3");
        satisfactoryCplusRow.createCell(7).setCellValue("Cukup");
        satisfactoryCplusRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryC = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 17;
        Row satisfactoryCRow = sheet.createRow(satisfactoryC);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryC, satisfactoryC, 7, 9));
        satisfactoryCRow.createCell(5).setCellValue("C");
        satisfactoryCRow.createCell(6).setCellValue("2");
        satisfactoryCRow.createCell(7).setCellValue("Cukup");
        satisfactoryCRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(7).setCellStyle(styleManajemen);

        int poor = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 18;
        Row poorRow = sheet.createRow(poor);
        sheet.addMergedRegion(new CellRangeAddress(poor, poor, 7, 9));
        poorRow.createCell(5).setCellValue("D");
        poorRow.createCell(6).setCellValue("1");
        poorRow.createCell(7).setCellValue("Kurang");
        poorRow.getCell(5).setCellStyle(styleDataKhs);
        poorRow.getCell(6).setCellStyle(styleDataKhs);
        poorRow.getCell(7).setCellStyle(styleManajemen);

        int fail = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 19;
        Row failRow = sheet.createRow(fail);
        sheet.addMergedRegion(new CellRangeAddress(fail, fail, 7, 9));
        failRow.createCell(5).setCellValue("E");
        failRow.createCell(6).setCellValue("0");
        failRow.createCell(7).setCellValue("Sangat Kurang");
        failRow.getCell(5).setCellStyle(styleDataKhs);
        failRow.getCell(6).setCellStyle(styleDataKhs);
        failRow.getCell(7).setCellStyle(styleManajemen);

        int createDate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 24;
        Row createDateRow = sheet.createRow(createDate);
        HijrahDate islamicDate = HijrahDate.from(LocalDate.now());
        String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
        String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("dd", new Locale("en")));
        String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

        if (LocalDate.now().getMonthValue() == 1) {

            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari " + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Januari" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 2) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Februari" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 3) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Maret" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 4) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " April" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 5) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Mei" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 6) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juni" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 7) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Juli" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 8) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Agustus" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 9) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " September" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 10) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Jumadil Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " Rabi'ul Akhir " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " Oktober" + " "
                                + LocalDate.now().getYear() + " / "
                                + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 11) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + LocalDate.now().getDayOfMonth() + " November" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        if (LocalDate.now().getMonthValue() == 12) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            } else {
                createDateRow.createCell(0)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + LocalDate.now().getDayOfMonth() + " Desember" + " "
                                + LocalDate.now().getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri);
                createDateRow.getCell(0).setCellStyle(styleData);
            }

        }

        int facultyy = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 26;
        Row facultyRow = sheet.createRow(facultyy);
        facultyRow.createCell(0).setCellValue("Dekan ");
        facultyRow.getCell(0).setCellStyle(styleData);
        facultyRow.createCell(5).setCellValue("Koordinator ");
        facultyRow.getCell(5).setCellStyle(styleData);

        int faculty2 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 27;
        Row facultyRow2 = sheet.createRow(faculty2);
        facultyRow2.createCell(0)
                .setCellValue("Fakultas " + mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        facultyRow2.getCell(0).setCellStyle(styleData);
        facultyRow2.createCell(5).setCellValue("Program Studi " + mahasiswa.getIdProdi().getNamaProdi());
        facultyRow2.getCell(5).setCellStyle(styleData);

        int lecture = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 32;
        Row lectureRow = sheet.createRow(lecture);
        lectureRow.createCell(0)
                .setCellValue(mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNamaKaryawan());
        lectureRow.getCell(0).setCellStyle(styleDosen);
        lectureRow.createCell(5)
                .setCellValue(mahasiswa.getIdProdi().getDosen().getKaryawan().getNamaKaryawan());
        lectureRow.getCell(5).setCellStyle(styleDosen);

        int nik = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 33;
        Row nikRow = sheet.createRow(nik);
        nikRow.createCell(0)
                .setCellValue("NIK : " + mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNik());
        nikRow.getCell(0).setCellStyle(styleNik);
        nikRow.createCell(5).setCellValue("NIK : " + mahasiswa.getIdProdi().getDosen().getKaryawan().getNik());
        nikRow.getCell(5).setCellStyle(styleNik);

        PropertyTemplate propertyTemplate = new PropertyTemplate();
        // semester1
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester2
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 0,
                        0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 1,
                        4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 5,
                        5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 6,
                        6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 7,
                        8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 9,
                        9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester3
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester4
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester5
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester6
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester7
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester8
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        5, 9),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 11,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 12,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 13,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 14,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);

        propertyTemplate.applyBorders(sheet);

        String namaFile = "Transkript-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/studiesActivity/transcript/transkriptexcel2")
    public void transkriptExcel2(@RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);
        List<TranskriptDto> semester1 = krsDetailDao.excelTranskript(mahasiswa.getId(), "1");
        List<TranskriptDto> semester2 = krsDetailDao.excelTranskript(mahasiswa.getId(), "2");
        List<TranskriptDto> semester3 = krsDetailDao.excelTranskript(mahasiswa.getId(), "3");
        List<TranskriptDto> semester4 = krsDetailDao.excelTranskript(mahasiswa.getId(), "4");
        List<TranskriptDto> semester5 = krsDetailDao.excelTranskript(mahasiswa.getId(), "5");
        List<TranskriptDto> semester6 = krsDetailDao.excelTranskript(mahasiswa.getId(), "6");
        List<TranskriptDto> semester7 = krsDetailDao.excelTranskript(mahasiswa.getId(), "7");
        List<TranskriptDto> semester8 = krsDetailDao.excelTranskript(mahasiswa.getId(), "8");

        BigDecimal totalSKS = krsDetailDao.totalSksAkhir(mahasiswa.getId());
        BigDecimal totalMuti = krsDetailDao.totalMutuAkhir(mahasiswa.getId());

        IpkDto ipk = krsDetailDao.ipk(mahasiswa);

        // BigDecimal ipk = totalMuti.divide(totalSKS,2,BigDecimal.ROUND_HALF_DOWN);

        InputStream file = contohExcelTranskript.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());

        sheet.addMergedRegion(CellRangeAddress.valueOf("A7:C7"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A8:C8"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A9:C9"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A10:C10"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A11:C11"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A12:C12"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A13:C13"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A14:C14"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A15:C15"));
        sheet.addMergedRegion(CellRangeAddress.valueOf("A16:C16"));

        Font manajemenFont = workbook.createFont();
        manajemenFont.setItalic(true);
        manajemenFont.setFontHeightInPoints((short) 10);
        manajemenFont.setFontName("Cambria");

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 10);
        dataManajemenFont.setFontName("Cambria");

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 10);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 10);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        Font prodiFont = workbook.createFont();
        prodiFont.setUnderline(XSSFFont.U_DOUBLE);
        prodiFont.setFontHeightInPoints((short) 10);
        prodiFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setItalic(true);
        ipFont.setFontHeightInPoints((short) 10);
        ipFont.setFontName("Cambria");

        Font lectureFont = workbook.createFont();
        lectureFont.setBold(true);
        lectureFont.setFontName("Cambria");
        lectureFont.setUnderline(XSSFFont.U_DOUBLE);
        lectureFont.setFontHeightInPoints((short) 10);

        Font nikFont = workbook.createFont();
        nikFont.setBold(true);
        nikFont.setFontName("Cambria");
        nikFont.setFontHeightInPoints((short) 10);

        CellStyle styleNik = workbook.createCellStyle();
        styleNik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNik.setFont(nikFont);

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleManajemen.setAlignment(HorizontalAlignment.CENTER);
        styleManajemen.setFont(manajemenFont);

        CellStyle styleDosen = workbook.createCellStyle();
        styleDosen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDosen.setFont(lectureFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setBorderTop(BorderStyle.MEDIUM);
        styleProdi.setBorderBottom(BorderStyle.MEDIUM);
        styleProdi.setBorderLeft(BorderStyle.MEDIUM);
        styleProdi.setBorderRight(BorderStyle.MEDIUM);
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhs.setFont(dataFont);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.CENTER);
        styleSymbol.setFont(symbolFont);

        CellStyle styleTotal = workbook.createCellStyle();
        styleTotal.setAlignment(HorizontalAlignment.CENTER);
        styleTotal.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotal.setFont(ipFont);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIpk.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle stylePrestasiAkademik = workbook.createCellStyle();
        stylePrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        stylePrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePrestasiAkademik.setFont(dataFont);

        CellStyle styleSubHeader1 = workbook.createCellStyle();
        styleSubHeader1.setAlignment(HorizontalAlignment.CENTER);
        styleSubHeader1.setVerticalAlignment(VerticalAlignment.CENTER);
        styleSubHeader1.setFont(ipFont);

        int rowInfoNama = 5;
        Row nama = sheet.createRow(rowInfoNama);
        nama.createCell(0).setCellValue("Name");
        nama.createCell(3).setCellValue(":");
        nama.createCell(4).setCellValue(mahasiswa.getNama());
        nama.getCell(0).setCellStyle(styleData);
        nama.getCell(3).setCellStyle(styleSymbol);
        nama.getCell(4).setCellStyle(styleData);

        int rowInfoNim = 6;
        Row matricNo = sheet.createRow(rowInfoNim);
        matricNo.createCell(0).setCellValue("Student Matric No");
        matricNo.createCell(3).setCellValue(":");
        matricNo.createCell(4).setCellValue(mahasiswa.getNim());
        matricNo.getCell(0).setCellStyle(styleData);
        matricNo.getCell(3).setCellStyle(styleSymbol);
        matricNo.getCell(4).setCellStyle(styleData);

        int rowInfoEntry = 7;
        Row entry = sheet.createRow(rowInfoEntry);
        entry.createCell(0).setCellValue("National Certificate Number");
        entry.createCell(3).setCellValue(":");
        entry.createCell(4).setCellValue(mahasiswa.getIndukNasional());
        entry.getCell(0).setCellStyle(styleData);
        entry.getCell(3).setCellStyle(styleSymbol);
        entry.getCell(4).setCellStyle(styleData);

        int rowInfoBirth = 8;
        Row birthDay = sheet.createRow(rowInfoBirth);
        birthDay.createCell(0).setCellValue("Place and Date of Birth");
        birthDay.createCell(3).setCellValue(":");

        int month = mahasiswa.getTanggalLahir().getDayOfMonth();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getPattern(month));
        String birthdate = mahasiswa.getTanggalLahir().format(formatter);
        birthDay.createCell(4).setCellValue(mahasiswa.getTempatLahir() + "," + " " + birthdate);
        birthDay.getCell(4).setCellStyle(styleData);

        birthDay.getCell(0).setCellStyle(styleData);
        birthDay.getCell(3).setCellStyle(styleSymbol);

        int rowInfoLevel = 9;
        Row level = sheet.createRow(rowInfoLevel);
        level.createCell(0).setCellValue("Level");
        level.createCell(3).setCellValue(":");

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            level.createCell(4).setCellValue("Undergraduate");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            level.createCell(4).setCellValue("Post Graduate");
            level.getCell(4).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("03").get()) {
            level.createCell(4).setCellValue("Undergraduate");
            level.getCell(4).setCellStyle(styleData);

        }
        level.getCell(0).setCellStyle(styleData);
        level.getCell(3).setCellStyle(styleSymbol);

        int rowInfoDepartment = 10;
        Row department = sheet.createRow(rowInfoDepartment);
        department.createCell(0).setCellValue("Department");
        department.createCell(3).setCellValue(":");
        department.createCell(4).setCellValue(mahasiswa.getIdProdi().getNamaProdiEnglish());
        department.getCell(0).setCellStyle(styleData);
        department.getCell(3).setCellStyle(styleSymbol);
        department.getCell(4).setCellStyle(styleData);

        int rowInfoFaculty = 11;
        Row facultyy = sheet.createRow(rowInfoFaculty);
        facultyy.createCell(0).setCellValue("Faculty");
        facultyy.createCell(3).setCellValue(":");
        facultyy.createCell(4).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultasEnglish());
        facultyy.getCell(0).setCellStyle(styleData);
        facultyy.getCell(3).setCellStyle(styleSymbol);
        facultyy.getCell(4).setCellStyle(styleData);

        int rowInfoNoAcred = 12;
        Row accreditation = sheet.createRow(rowInfoNoAcred);
        accreditation.createCell(0).setCellValue("No of Accreditation Decree");
        accreditation.createCell(3).setCellValue(":");
        accreditation.createCell(4).setCellValue(mahasiswa.getIdProdi().getNoSk());
        accreditation.getCell(0).setCellStyle(styleData);
        accreditation.getCell(3).setCellStyle(styleSymbol);
        accreditation.getCell(4).setCellStyle(styleData);

        int rowInfoDateAcred = 13;
        Row dateAccreditation = sheet.createRow(rowInfoDateAcred);
        dateAccreditation.createCell(0).setCellValue("Date of Accreditation Decree");
        dateAccreditation.createCell(3).setCellValue(":");
        int monthAccred = mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth();
        DateTimeFormatter formatterAccred = DateTimeFormatter.ofPattern(getPattern(monthAccred));
        String accredDate = mahasiswa.getIdProdi().getTanggalSk().format(formatterAccred);
        dateAccreditation.createCell(4).setCellValue(accredDate);
        dateAccreditation.getCell(4).setCellStyle(styleData);

        dateAccreditation.getCell(0).setCellStyle(styleData);
        dateAccreditation.getCell(3).setCellStyle(styleSymbol);

        int rowInfoGraduatedDate = 14;
        Row graduatedDate = sheet.createRow(rowInfoGraduatedDate);
        graduatedDate.createCell(0).setCellValue("Graduated Date");
        graduatedDate.createCell(3).setCellValue(":");
        int monthGraduate = mahasiswa.getTanggalLulus().getDayOfMonth();
        DateTimeFormatter formatterGraduate = DateTimeFormatter.ofPattern(getPattern(monthGraduate));
        String graduateDate = mahasiswa.getTanggalLulus().format(formatterGraduate);

        graduatedDate.createCell(4).setCellValue(graduateDate);
        graduatedDate.getCell(4).setCellStyle(styleData);

        graduatedDate.getCell(0).setCellStyle(styleData);
        graduatedDate.getCell(3).setCellStyle(styleSymbol);

        int rowInfoTranscript = 15;
        Row transcript = sheet.createRow(rowInfoTranscript);
        transcript.createCell(0).setCellValue("No of Transcript");
        transcript.createCell(3).setCellValue(":");
        transcript.createCell(4).setCellValue(mahasiswa.getNoTranskript());
        transcript.getCell(0).setCellStyle(styleData);
        transcript.getCell(3).setCellStyle(styleSymbol);
        transcript.getCell(4).setCellStyle(styleData);

        int rowNumSemester1 = 18;
        for (TranskriptDto sem1 : semester1) {
            Row row = sheet.createRow(rowNumSemester1);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 7, 8));

            row.createCell(0).setCellValue(sem1.getKode());
            row.createCell(1).setCellValue(sem1.getCourses());
            row.createCell(5).setCellValue(sem1.getSks());
            row.createCell(6).setCellValue(sem1.getGrade());
            row.createCell(7).setCellValue(sem1.getBobot().toString());
            row.createCell(9).setCellValue(sem1.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester1++;
        }

        int rowNumSemester2 = 18 + semester1.size();
        for (TranskriptDto sem2 : semester2) {
            Row row = sheet.createRow(rowNumSemester2);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 7, 8));

            row.createCell(0).setCellValue(sem2.getKode());
            row.createCell(1).setCellValue(sem2.getCourses());
            row.createCell(5).setCellValue(sem2.getSks());
            row.createCell(6).setCellValue(sem2.getGrade());
            row.createCell(7).setCellValue(sem2.getBobot().toString());
            row.createCell(9).setCellValue(sem2.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester2++;
        }

        int rowNumSemester3 = 18 + semester1.size() + semester2.size();
        for (TranskriptDto sem3 : semester3) {
            Row row = sheet.createRow(rowNumSemester3);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 7, 8));

            row.createCell(0).setCellValue(sem3.getKode());
            row.createCell(1).setCellValue(sem3.getCourses());
            row.createCell(5).setCellValue(sem3.getSks());
            row.createCell(6).setCellValue(sem3.getGrade());
            row.createCell(7).setCellValue(sem3.getBobot().toString());
            row.createCell(9).setCellValue(sem3.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester3++;
        }

        int rowNumSemester4 = 18 + semester1.size() + semester2.size() + semester3.size();
        for (TranskriptDto sem4 : semester4) {
            Row row = sheet.createRow(rowNumSemester4);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 7, 8));

            row.createCell(0).setCellValue(sem4.getKode());
            row.createCell(1).setCellValue(sem4.getCourses());
            row.createCell(5).setCellValue(sem4.getSks());
            row.createCell(6).setCellValue(sem4.getGrade());
            row.createCell(7).setCellValue(sem4.getBobot().toString());
            row.createCell(9).setCellValue(sem4.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester4++;
        }

        int rowNumSemester5 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size();
        for (TranskriptDto sem5 : semester5) {
            Row row = sheet.createRow(rowNumSemester5);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 7, 8));

            row.createCell(0).setCellValue(sem5.getKode());
            row.createCell(1).setCellValue(sem5.getCourses());
            row.createCell(5).setCellValue(sem5.getSks());
            row.createCell(6).setCellValue(sem5.getGrade());
            row.createCell(7).setCellValue(sem5.getBobot().toString());
            row.createCell(9).setCellValue(sem5.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester5++;
        }

        int rowNumSemester6 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size();
        for (TranskriptDto sem6 : semester6) {
            Row row = sheet.createRow(rowNumSemester6);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 7, 8));

            row.createCell(0).setCellValue(sem6.getKode());
            row.createCell(1).setCellValue(sem6.getCourses());
            row.createCell(5).setCellValue(sem6.getSks());
            row.createCell(6).setCellValue(sem6.getGrade());
            row.createCell(7).setCellValue(sem6.getBobot().toString());
            row.createCell(9).setCellValue(sem6.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester6++;
        }

        int rowNumSemester7 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size();
        for (TranskriptDto sem7 : semester7) {
            Row row = sheet.createRow(rowNumSemester7);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 7, 8));

            row.createCell(0).setCellValue(sem7.getKode());
            row.createCell(1).setCellValue(sem7.getCourses());
            row.createCell(5).setCellValue(sem7.getSks());
            row.createCell(6).setCellValue(sem7.getGrade());
            row.createCell(7).setCellValue(sem7.getBobot().toString());
            row.createCell(9).setCellValue(sem7.getMutu().toString());
            row.getCell(1).setCellStyle(styleData);
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester7++;
        }

        int rowNumSemester8 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size();
        for (TranskriptDto sem8 : semester8) {
            Row row = sheet.createRow(rowNumSemester8);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 1, 4));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 7, 8));

            row.createCell(0).setCellValue(sem8.getKode());
            row.createCell(1).setCellValue(sem8.getCourses());
            row.createCell(5).setCellValue(sem8.getSks());
            row.createCell(6).setCellValue(sem8.getGrade());
            row.createCell(7).setCellValue(sem8.getBobot().toString());
            row.createCell(9).setCellValue(sem8.getMutu().toString());
            row.getCell(0).setCellStyle(styleDataKhs);
            row.getCell(1).setCellStyle(styleData);
            row.getCell(5).setCellStyle(styleDataKhs);
            row.getCell(6).setCellStyle(styleDataKhs);
            row.getCell(7).setCellStyle(styleDataKhs);
            row.getCell(9).setCellStyle(styleDataKhs);

            rowNumSemester8++;
        }

        int total = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size();
        Row rowTotal = sheet.createRow(total);
        sheet.addMergedRegion(new CellRangeAddress(total, total, 1, 4));
        rowTotal.createCell(1).setCellValue("Total");
        rowTotal.createCell(5).setCellValue(totalSKS.intValue());
        rowTotal.createCell(9).setCellValue(totalMuti.toString());
        rowTotal.getCell(1).setCellStyle(styleTotal);
        rowTotal.getCell(5).setCellStyle(styleDataKhs);
        rowTotal.getCell(9).setCellStyle(styleDataKhs);

        int ipKomulatif = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 2;
        Row rowIpk = sheet.createRow(ipKomulatif);
        sheet.addMergedRegion(new CellRangeAddress(ipKomulatif, ipKomulatif, 0, 2));
        rowIpk.createCell(0).setCellValue("Cumulative Grade Point Average");
        rowIpk.createCell(5).setCellValue(ipk.getIpk().toString());
        rowIpk.getCell(0).setCellStyle(styleTotal);
        rowIpk.getCell(5).setCellStyle(styleDataKhs);

        int predicate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 4;
        Row predicateRow = sheet.createRow(predicate);
        predicateRow.createCell(0).setCellValue("Predicate :");
        if (ipk.getIpk().compareTo(new BigDecimal(2.99)) <= 0) {
            predicateRow.createCell(1).setCellValue("Satisfactory");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.00)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.49)) <= 0) {
            predicateRow.createCell(1).setCellValue("Good");
            predicateRow.getCell(1).setCellStyle(styleData);

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.50)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(3.79)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);

            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Very Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (ipk.getIpk().compareTo(new BigDecimal(3.80)) >= 0
                && ipk.getIpk().compareTo(new BigDecimal(4.00)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);

            if (!validate.equals(0)) {
                predicateRow.createCell(1).setCellValue("Good");
                predicateRow.getCell(1).setCellStyle(styleData);
            } else {
                predicateRow.createCell(1).setCellValue("Excellent");
                predicateRow.getCell(1).setCellStyle(styleData);
            }

        }

        predicateRow.getCell(0).setCellStyle(styleSubHeader);

        int thesis = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 5;
        Row thesisRow = sheet.createRow(thesis);
        thesisRow.createCell(0).setCellValue("Thesis Title :");
        thesisRow.createCell(1).setCellValue(mahasiswa.getTitle());
        thesisRow.getCell(0).setCellStyle(styleSubHeader);
        thesisRow.getCell(1).setCellStyle(styleData);

        int keyResult = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 8;
        Row resultRow = sheet.createRow(keyResult);
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 5, 9));
        resultRow.createCell(0).setCellValue("Key to Result");
        resultRow.createCell(5).setCellValue("Grading System");
        resultRow.getCell(0).setCellStyle(styleDataKhs);
        resultRow.getCell(5).setCellStyle(styleDataKhs);

        int remark = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 10;
        Row remarkRow = sheet.createRow(remark);
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 0, 3));
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 7, 9));
        remarkRow.createCell(0).setCellValue("Remarks");
        remarkRow.createCell(5).setCellValue("Grade");
        remarkRow.createCell(6).setCellValue("Value");
        remarkRow.createCell(7).setCellValue("Meaning");
        remarkRow.getCell(0).setCellStyle(styleDataKhs);
        remarkRow.getCell(5).setCellStyle(styleIpk);
        remarkRow.getCell(6).setCellStyle(styleIpk);
        remarkRow.getCell(7).setCellStyle(styleIpk);

        int excellent = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 11;
        Row excellentRow = sheet.createRow(excellent);
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 7, 9));
        excellentRow.createCell(0).setCellValue("3,80-4,00");
        excellentRow.createCell(1).setCellValue("Excellent (Minimum B)");
        excellentRow.createCell(5).setCellValue("A");
        excellentRow.createCell(6).setCellValue("4");
        excellentRow.createCell(7).setCellValue("Excellent");
        excellentRow.getCell(0).setCellStyle(styleProdi);
        excellentRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        excellentRow.getCell(5).setCellStyle(styleDataKhs);
        excellentRow.getCell(6).setCellStyle(styleDataKhs);
        excellentRow.getCell(7).setCellStyle(styleManajemen);

        int veryGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 12;
        Row veryGoodRow = sheet.createRow(veryGood);
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 7, 9));
        veryGoodRow.createCell(0).setCellValue("3,50-3,79");
        veryGoodRow.createCell(1).setCellValue("Very Good (Minimum B)");
        veryGoodRow.createCell(5).setCellValue("A-");
        veryGoodRow.createCell(6).setCellValue("3,7");
        veryGoodRow.createCell(7).setCellValue("Very Good");
        veryGoodRow.getCell(0).setCellStyle(styleProdi);
        veryGoodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        veryGoodRow.getCell(5).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(6).setCellStyle(styleDataKhs);
        veryGoodRow.getCell(7).setCellStyle(styleManajemen);

        int good = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 13;
        Row goodRow = sheet.createRow(good);
        sheet.addMergedRegion(new CellRangeAddress(good, good, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(good, good, 7, 9));
        goodRow.createCell(0).setCellValue("3,00-3,49");
        goodRow.createCell(1).setCellValue("Good");
        goodRow.createCell(5).setCellValue("B+");
        goodRow.createCell(6).setCellValue("3,3");
        goodRow.createCell(7).setCellValue("Good");
        goodRow.getCell(0).setCellStyle(styleProdi);
        goodRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        goodRow.getCell(5).setCellStyle(styleDataKhs);
        goodRow.getCell(6).setCellStyle(styleDataKhs);
        goodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactory = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 14;
        Row satisfactoryRow = sheet.createRow(satisfactory);
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 1, 3));
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 7, 9));
        satisfactoryRow.createCell(0).setCellValue("2,75-2,99");
        satisfactoryRow.createCell(1).setCellValue("Satisfactory");
        satisfactoryRow.createCell(5).setCellValue("B");
        satisfactoryRow.createCell(6).setCellValue("3");
        satisfactoryRow.createCell(7).setCellValue("Good");
        satisfactoryRow.getCell(0).setCellStyle(styleProdi);
        satisfactoryRow.getCell(1).setCellStyle(stylePrestasiAkademik);
        satisfactoryRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryRow.getCell(7).setCellStyle(styleManajemen);

        int almostGood = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 15;
        Row almostGoodRow = sheet.createRow(almostGood);
        sheet.addMergedRegion(new CellRangeAddress(almostGood, almostGood, 7, 9));
        almostGoodRow.createCell(5).setCellValue("B-");
        almostGoodRow.createCell(6).setCellValue("2,7");
        almostGoodRow.createCell(7).setCellValue("Almost Good");
        almostGoodRow.getCell(5).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(6).setCellStyle(styleDataKhs);
        almostGoodRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryCplus = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 16;
        Row satisfactoryCplusRow = sheet.createRow(satisfactoryCplus);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryCplus, satisfactoryCplus, 7, 9));
        satisfactoryCplusRow.createCell(5).setCellValue("C+");
        satisfactoryCplusRow.createCell(6).setCellValue("2,3");
        satisfactoryCplusRow.createCell(7).setCellValue("Satisfactory");
        satisfactoryCplusRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCplusRow.getCell(7).setCellStyle(styleManajemen);

        int satisfactoryC = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 17;
        Row satisfactoryCRow = sheet.createRow(satisfactoryC);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryC, satisfactoryC, 7, 9));
        satisfactoryCRow.createCell(5).setCellValue("C");
        satisfactoryCRow.createCell(6).setCellValue("2");
        satisfactoryCRow.createCell(7).setCellValue("Satisfactory");
        satisfactoryCRow.getCell(5).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(6).setCellStyle(styleDataKhs);
        satisfactoryCRow.getCell(7).setCellStyle(styleManajemen);

        int poor = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 18;
        Row poorRow = sheet.createRow(poor);
        sheet.addMergedRegion(new CellRangeAddress(poor, poor, 7, 9));
        poorRow.createCell(5).setCellValue("D");
        poorRow.createCell(6).setCellValue("1");
        poorRow.createCell(7).setCellValue("Poor");
        poorRow.getCell(5).setCellStyle(styleDataKhs);
        poorRow.getCell(6).setCellStyle(styleDataKhs);
        poorRow.getCell(7).setCellStyle(styleManajemen);

        int fail = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 19;
        Row failRow = sheet.createRow(fail);
        sheet.addMergedRegion(new CellRangeAddress(fail, fail, 7, 9));
        failRow.createCell(5).setCellValue("E");
        failRow.createCell(6).setCellValue("0");
        failRow.createCell(7).setCellValue("Fail");
        failRow.getCell(5).setCellStyle(styleDataKhs);
        failRow.getCell(6).setCellStyle(styleDataKhs);
        failRow.getCell(7).setCellStyle(styleManajemen);

        int createDate = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 24;
        Row createDateRow = sheet.createRow(createDate);
        HijrahDate islamicDate = HijrahDate.from(LocalDate.now());
        String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
        String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("d", new Locale("en")));
        String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

        int monthCreate = mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getDayOfMonth();
        DateTimeFormatter formatterCreate = DateTimeFormatter.ofPattern(getPattern(monthCreate));
        String createDatee = mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .format(formatterCreate);

        if (namaBulanHijri.equals("Jumada I")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Jumadil Awal "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Jumada II")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Jumadil Akhir "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Rabiʻ I")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Rabi'ul Awal "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else if (namaBulanHijri.equals("Rabiʻ II")) {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " Rabi'ul Akhir "
                            + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        } else {
            createDateRow.createCell(0)
                    .setCellValue("This is certified to be true and accurate statement, issued in Bogor on "
                            + createDatee + " / " + tanggalHijri + " " + namaBulanHijri
                            + " " + tahunHijri);
            createDateRow.getCell(0).setCellStyle(styleData);
        }

        int faculty = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 26;
        Row facultyRow = sheet.createRow(faculty);
        facultyRow.createCell(0).setCellValue("Dean of ");
        facultyRow.getCell(0).setCellStyle(styleData);
        facultyRow.createCell(5).setCellValue("Coordinator of ");
        facultyRow.getCell(5).setCellStyle(styleData);

        int faculty2 = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 27;
        Row facultyRow2 = sheet.createRow(faculty2);
        facultyRow2.createCell(0).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultasEnglish());
        facultyRow2.getCell(0).setCellStyle(styleData);
        facultyRow2.createCell(5).setCellValue(mahasiswa.getIdProdi().getNamaProdiEnglish());
        facultyRow2.getCell(5).setCellStyle(styleData);

        int lecture = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 32;
        Row lectureRow = sheet.createRow(lecture);
        lectureRow.createCell(0)
                .setCellValue(mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNamaKaryawan());
        lectureRow.getCell(0).setCellStyle(styleDosen);
        lectureRow.createCell(5)
                .setCellValue(mahasiswa.getIdProdi().getDosen().getKaryawan().getNamaKaryawan());
        lectureRow.getCell(5).setCellStyle(styleDosen);

        int nik = 18 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 33;
        Row nikRow = sheet.createRow(nik);
        nikRow.createCell(0)
                .setCellValue("NIK : " + mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNik());
        nikRow.getCell(0).setCellStyle(styleNik);
        nikRow.createCell(5).setCellValue("NIK : " + mahasiswa.getIdProdi().getDosen().getKaryawan().getNik());
        nikRow.getCell(5).setCellStyle(styleNik);

        PropertyTemplate propertyTemplate = new PropertyTemplate();
        // semester1
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(new CellRangeAddress(18, 17 + semester1.size(), 9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester2
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 0,
                        0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 1,
                        4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 5,
                        5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 6,
                        6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 7,
                        8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(), 17 + semester1.size() + semester2.size(), 9,
                        9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        // semester3
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester4
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester5
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester6
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester7
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size()
                                + semester7.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        // semester8
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        0, 0),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        6, 6),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        7, 8),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size(),
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);

        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        1, 4),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        5, 5),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(18 + semester1.size(),
                        17 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 1,
                        9, 9),
                BorderStyle.THIN, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 9,
                        5, 9),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 10,
                        0, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 11,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 12,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 13,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);
        propertyTemplate.drawBorders(
                new CellRangeAddress(
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 8,
                        18 + semester1.size() + semester2.size() + semester3.size()
                                + semester4.size()
                                + semester5.size() + semester6.size() + semester7.size()
                                + semester8.size() + 14,
                        1, 3),
                BorderStyle.MEDIUM, BorderExtent.OUTSIDE);

        propertyTemplate.applyBorders(sheet);

        String namaFile = "Transkript-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    // BERKAS

    @GetMapping("/berkas/uploadSoal/list")
    public void listBerkas(Model model, Authentication authentication) {

        User user = currentUserService.currentUser(authentication);
        Karyawan k = karyawanDao.findByIdUser(user);
        Dosen dosen = dosenDao.findByKaryawan(k);

        model.addAttribute("dosen", dosen);
        model.addAttribute("dosenAkses",
                jadwalDosenDao.findByJadwalTahunAkademik(
                        tahunAkademikDao.findByStatus(StatusRecord.AKTIF)));

        List<SoalDto> listUts = jadwalDao.listUts(tahunAkademikDao.findByStatus(StatusRecord.AKTIF), dosen);
        List<SoalDto> listUas = jadwalDao.listUas(tahunAkademikDao.findByStatus(StatusRecord.AKTIF), dosen);

        model.addAttribute("listUts", listUts);
        model.addAttribute("listUas", listUas);

    }

    @GetMapping("/studiesActivity/transcript/excelindo2")
    public void transkriptFormatExcel2(@RequestParam(required = false) String nim, HttpServletResponse response)
            throws IOException {

        Mahasiswa mahasiswa = mahasiswaDao.findByNim(nim);

        // file
        List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mahasiswa);
        listTranskript.removeIf(e -> e.getGrade().equals("E"));

        int totalSKS = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();

        BigDecimal totalMuti = listTranskript.stream().map(DataTranskript::getMutu)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal ipk = totalMuti.divide(new BigDecimal(totalSKS), 2, BigDecimal.ROUND_HALF_UP);

        List<DataTranskript> semester1 = new ArrayList<>();
        List<DataTranskript> semester2 = new ArrayList<>();
        List<DataTranskript> semester3 = new ArrayList<>();
        List<DataTranskript> semester4 = new ArrayList<>();
        List<DataTranskript> semester5 = new ArrayList<>();
        List<DataTranskript> semester6 = new ArrayList<>();
        List<DataTranskript> semester7 = new ArrayList<>();
        List<DataTranskript> semester8 = new ArrayList<>();

        for (DataTranskript data : listTranskript) {
            if (data.getSemester().equals("1")) {
                semester1.add(data);
            }
            if (data.getSemester().equals("2")) {
                semester2.add(data);
            }
            if (data.getSemester().equals("3")) {
                semester3.add(data);
            }
            if (data.getSemester().equals("4")) {
                semester4.add(data);
            }
            if (data.getSemester().equals("5")) {
                semester5.add(data);
            }
            if (data.getSemester().equals("6")) {
                semester6.add(data);
            }
            if (data.getSemester().equals("7")) {
                semester7.add(data);
            }
            if (data.getSemester().equals("8")) {
                semester8.add(data);
            }
        }

        InputStream file = getContohExcelTranskript.getInputStream();

        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheetAt(0);
        workbook.setSheetName(workbook.getSheetIndex(sheet), mahasiswa.getNama());

        Font manajemenFont = workbook.createFont();
        manajemenFont.setItalic(true);
        manajemenFont.setFontHeightInPoints((short) 10);
        manajemenFont.setFontName("Cambria");

        Font dataManajemenFont = workbook.createFont();
        dataManajemenFont.setFontHeightInPoints((short) 10);
        dataManajemenFont.setFontName("Cambria");

        Font subHeaderFont = workbook.createFont();
        subHeaderFont.setFontHeightInPoints((short) 10);
        subHeaderFont.setFontName("Cambria");
        subHeaderFont.setBold(true);

        Font judulFont = workbook.createFont();
        judulFont.setFontHeightInPoints((short) 10);
        judulFont.setFontName("Cambria");
        judulFont.setBold(true);

        Font thesisFont = workbook.createFont();
        thesisFont.setFontHeightInPoints((short) 10);
        thesisFont.setFontName("Cambria");

        Font symbolFont = workbook.createFont();
        symbolFont.setFontHeightInPoints((short) 10);
        symbolFont.setFontName("Cambria");

        Font dataFont = workbook.createFont();
        dataFont.setFontHeightInPoints((short) 10);
        dataFont.setFontName("Cambria");

        Font dataTableFont = workbook.createFont();
        dataTableFont.setFontHeightInPoints((short) 10);
        dataTableFont.setFontName("Cambria");

        Font prestasiFont = workbook.createFont();
        prestasiFont.setFontHeightInPoints((short) 10);
        prestasiFont.setFontName("Cambria");

        Font judulSkripsiFont = workbook.createFont();
        judulSkripsiFont.setFontHeightInPoints((short) 10);
        judulSkripsiFont.setFontName("Cambria");

        Font dataKhsFont = workbook.createFont();
        dataKhsFont.setFontHeightInPoints((short) 10);
        dataKhsFont.setFontName("Cambria");

        Font dataIpkFont = workbook.createFont();
        dataIpkFont.setFontHeightInPoints((short) 10);
        dataIpkFont.setFontName("Cambria");

        Font penilaianFont = workbook.createFont();
        penilaianFont.setFontHeightInPoints((short) 10);
        penilaianFont.setFontName("Cambria");

        Font prestasiFont2 = workbook.createFont();
        prestasiFont2.setFontHeightInPoints((short) 10);
        prestasiFont2.setFontName("Cambria");

        Font dataPrestasiFont = workbook.createFont();
        dataPrestasiFont.setFontHeightInPoints((short) 10);
        dataPrestasiFont.setFontName("Cambria");

        Font matkulFont = workbook.createFont();
        matkulFont.setFontHeightInPoints((short) 10);
        matkulFont.setFontName("Cambria");

        Font dataFontNew = workbook.createFont();
        dataFontNew.setFontHeightInPoints((short) 10);
        dataFontNew.setFontName("Times New Roman");

        Font prodiFont = workbook.createFont();
        prodiFont.setUnderline(XSSFFont.U_DOUBLE);
        prodiFont.setFontHeightInPoints((short) 10);
        prodiFont.setFontName("Cambria");

        Font presFont = workbook.createFont();
        presFont.setFontHeightInPoints((short) 10);
        presFont.setFontName("Cambria");

        Font ipFont = workbook.createFont();
        ipFont.setBold(true);
        ipFont.setItalic(true);
        ipFont.setFontHeightInPoints((short) 10);
        ipFont.setFontName("Cambria");

        Font ipFontBorder = workbook.createFont();
        ipFontBorder.setBold(true);
        ipFontBorder.setItalic(true);
        ipFontBorder.setFontHeightInPoints((short) 10);
        ipFontBorder.setFontName("Cambria");

        Font lectureFont = workbook.createFont();
        lectureFont.setBold(true);
        lectureFont.setFontName("Cambria");
        lectureFont.setUnderline(XSSFFont.U_DOUBLE);
        lectureFont.setFontHeightInPoints((short) 10);

        Font nikFont = workbook.createFont();
        nikFont.setBold(true);
        nikFont.setFontName("Cambria");
        nikFont.setFontHeightInPoints((short) 10);

        CellStyle styleNik = workbook.createCellStyle();
        styleNik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleNik.setFont(nikFont);

        CellStyle styleManajemen = workbook.createCellStyle();
        styleManajemen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleManajemen.setAlignment(HorizontalAlignment.CENTER);
        styleManajemen.setFont(manajemenFont);

        CellStyle styleDosen = workbook.createCellStyle();
        styleDosen.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDosen.setFont(lectureFont);

        CellStyle styleProdi = workbook.createCellStyle();
        styleProdi.setBorderTop(BorderStyle.MEDIUM);
        styleProdi.setBorderBottom(BorderStyle.MEDIUM);
        styleProdi.setBorderLeft(BorderStyle.MEDIUM);
        styleProdi.setBorderRight(BorderStyle.MEDIUM);
        styleProdi.setFont(dataManajemenFont);

        CellStyle styleSubHeader = workbook.createCellStyle();
        styleSubHeader.setFont(subHeaderFont);
        styleSubHeader.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle styleJudul = workbook.createCellStyle();
        styleJudul.setFont(judulFont);
        styleJudul.setAlignment(HorizontalAlignment.LEFT);
        styleJudul.setVerticalAlignment(VerticalAlignment.TOP);

        CellStyle styleThesis = workbook.createCellStyle();
        styleThesis.setFont(thesisFont);
        styleThesis.setAlignment(HorizontalAlignment.LEFT);
        styleThesis.setVerticalAlignment(VerticalAlignment.TOP);
        styleThesis.setWrapText(true);

        CellStyle styleData = workbook.createCellStyle();
        styleData.setFont(dataFont);

        CellStyle styleDataNew = workbook.createCellStyle();
        styleDataNew.setFont(dataFontNew);
        styleDataNew.setAlignment(HorizontalAlignment.CENTER);

        CellStyle styleDataKhs = workbook.createCellStyle();
        styleDataKhs.setAlignment(HorizontalAlignment.CENTER);
        styleDataKhs.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhs.setFont(dataKhsFont);

        CellStyle styleIsiIpk = workbook.createCellStyle();
        styleIsiIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIsiIpk.setVerticalAlignment(VerticalAlignment.CENTER);
        styleIsiIpk.setFont(dataIpkFont);

        CellStyle stylePenilaian = workbook.createCellStyle();
        stylePenilaian.setAlignment(HorizontalAlignment.CENTER);
        stylePenilaian.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePenilaian.setFont(penilaianFont);

        CellStyle styleDataTale = workbook.createCellStyle();
        styleDataTale.setFont(dataTableFont);
        styleDataTale.setAlignment(HorizontalAlignment.CENTER);
        styleDataTale.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataTale.setBorderRight(BorderStyle.THIN);
        styleDataTale.setBorderLeft(BorderStyle.THIN);

        CellStyle styleDataTale2 = workbook.createCellStyle();
        styleDataTale2.setFont(dataTableFont);
        styleDataTale2.setAlignment(HorizontalAlignment.CENTER);
        styleDataTale2.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataTale2.setBorderRight(BorderStyle.THIN);
        styleDataTale2.setBorderBottom(BorderStyle.THIN);
        styleDataTale2.setBorderLeft(BorderStyle.THIN);

        CellStyle styleDataPrestasiAkademik = workbook.createCellStyle();
        styleDataPrestasiAkademik.setFont(prestasiFont);
        styleDataPrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        styleDataPrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataPrestasiAkademik.setBorderBottom(BorderStyle.MEDIUM);
        styleDataPrestasiAkademik.setBorderTop(BorderStyle.MEDIUM);
        styleDataPrestasiAkademik.setBorderRight(BorderStyle.MEDIUM);
        styleDataPrestasiAkademik.setBorderLeft(BorderStyle.MEDIUM);

        CellStyle styleDataKhsTable = workbook.createCellStyle();
        styleDataKhsTable.setAlignment(HorizontalAlignment.LEFT);
        styleDataKhsTable.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhsTable.setFont(matkulFont);
        styleDataKhsTable.setBorderRight(BorderStyle.THIN);
        styleDataKhsTable.setBorderLeft(BorderStyle.THIN);

        CellStyle styleDataKhsTable2 = workbook.createCellStyle();
        styleDataKhsTable2.setAlignment(HorizontalAlignment.LEFT);
        styleDataKhsTable2.setVerticalAlignment(VerticalAlignment.CENTER);
        styleDataKhsTable2.setFont(matkulFont);
        styleDataKhsTable2.setBorderBottom(BorderStyle.THIN);
        styleDataKhsTable2.setBorderRight(BorderStyle.THIN);
        styleDataKhsTable2.setBorderLeft(BorderStyle.THIN);

        CellStyle stylePrestasiAkademik = workbook.createCellStyle();
        stylePrestasiAkademik.setAlignment(HorizontalAlignment.LEFT);
        stylePrestasiAkademik.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePrestasiAkademik.setFont(prestasiFont);

        CellStyle styleSubHeader1 = workbook.createCellStyle();
        styleSubHeader1.setAlignment(HorizontalAlignment.LEFT);
        styleSubHeader1.setVerticalAlignment(VerticalAlignment.CENTER);
        styleSubHeader1.setFont(ipFont);

        CellStyle styleJudulSkripsi = workbook.createCellStyle();
        styleJudulSkripsi.setFont(judulSkripsiFont);
        styleJudulSkripsi.setWrapText(true);

        CellStyle styleSymbol = workbook.createCellStyle();
        styleSymbol.setAlignment(HorizontalAlignment.RIGHT);
        styleSymbol.setFont(symbolFont);

        CellStyle styleTotal = workbook.createCellStyle();
        styleTotal.setAlignment(HorizontalAlignment.LEFT);
        styleTotal.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotal.setFont(ipFont);

        CellStyle styleTotalBorder = workbook.createCellStyle();
        styleTotalBorder.setAlignment(HorizontalAlignment.CENTER);
        styleTotalBorder.setVerticalAlignment(VerticalAlignment.CENTER);
        styleTotalBorder.setFont(ipFontBorder);
        styleTotalBorder.setBorderBottom(BorderStyle.THIN);
        styleTotalBorder.setBorderTop(BorderStyle.THIN);
        styleTotalBorder.setBorderRight(BorderStyle.THIN);
        styleTotalBorder.setBorderLeft(BorderStyle.THIN);

        CellStyle styleIpk = workbook.createCellStyle();
        styleIpk.setFont(prodiFont);
        styleIpk.setAlignment(HorizontalAlignment.CENTER);
        styleIpk.setVerticalAlignment(VerticalAlignment.CENTER);

        CellStyle stylePres = workbook.createCellStyle();
        stylePres.setFont(presFont);
        stylePres.setAlignment(HorizontalAlignment.CENTER);
        stylePres.setVerticalAlignment(VerticalAlignment.CENTER);
        stylePres.setBorderBottom(BorderStyle.MEDIUM);
        stylePres.setBorderTop(BorderStyle.MEDIUM);
        stylePres.setBorderRight(BorderStyle.MEDIUM);
        stylePres.setBorderLeft(BorderStyle.MEDIUM);

        CellStyle borderRight = workbook.createCellStyle();
        borderRight.setBorderRight(BorderStyle.MEDIUM);

        int rowInfoNama = 12;
        Row nama = sheet.createRow(rowInfoNama);
        nama.createCell(2).setCellValue("Nama Mahasiswa ");
        nama.createCell(4).setCellValue(":");
        nama.createCell(5).setCellValue(mahasiswa.getNama());
        nama.getCell(2).setCellStyle(styleData);
        nama.getCell(4).setCellStyle(styleSymbol);
        nama.getCell(5).setCellStyle(styleData);

        int rowInfoNim = 13;
        Row matricNo = sheet.createRow(rowInfoNim);
        matricNo.createCell(2).setCellValue("NIM");
        matricNo.createCell(4).setCellValue(":");
        matricNo.createCell(5).setCellValue(mahasiswa.getNim());
        matricNo.getCell(2).setCellStyle(styleData);
        matricNo.getCell(4).setCellStyle(styleSymbol);
        matricNo.getCell(5).setCellStyle(styleData);

        int rowInfoEntry = 14;
        Row entry = sheet.createRow(rowInfoEntry);
        entry.createCell(2).setCellValue("Nomor Ijazah Nasional");
        entry.createCell(4).setCellValue(":");
        entry.createCell(5).setCellValue(mahasiswa.getIndukNasional());
        entry.getCell(2).setCellStyle(styleData);
        entry.getCell(4).setCellStyle(styleSymbol);
        entry.getCell(5).setCellStyle(styleData);

        int rowInfoBirth = 15;
        Row birthDay = sheet.createRow(rowInfoBirth);
        birthDay.createCell(2).setCellValue("Tempat, Tanggal lahir");
        birthDay.createCell(4).setCellValue(":");

        if (mahasiswa.getTanggalLahir().getMonthValue() == 1) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Januari" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 2) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Februari" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 3) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Maret" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 4) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " April" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 5) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Mei" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 6) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Juni" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 7) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Juli" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 8) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Agustus" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 9) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " September" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 10) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Oktober" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 11) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " November" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLahir().getMonthValue() == 12) {
            birthDay.createCell(5)
                    .setCellValue(mahasiswa.getTempatLahir() + "," + " "
                            + mahasiswa.getTanggalLahir().getDayOfMonth()
                            + " Desember" + " " + mahasiswa.getTanggalLahir().getYear());
            birthDay.getCell(5).setCellStyle(styleData);

        }

        birthDay.getCell(2).setCellStyle(styleData);
        birthDay.getCell(4).setCellStyle(styleSymbol);

        int rowInfoLevel = 16;
        Row level = sheet.createRow(rowInfoLevel);
        level.createCell(2).setCellValue("Program Pendidikan");
        level.createCell(4).setCellValue(":");

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            level.createCell(5).setCellValue("Sarjana");
            level.getCell(5).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            level.createCell(5).setCellValue("Magister");
            level.getCell(5).setCellStyle(styleData);
        }
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("03").get()) {
            level.createCell(5).setCellValue("Sarjana");
            level.getCell(5).setCellStyle(styleData);

        }
        level.getCell(2).setCellStyle(styleData);
        level.getCell(4).setCellStyle(styleSymbol);

        int rowInfoDepartment = 17;
        Row department = sheet.createRow(rowInfoDepartment);
        department.createCell(2).setCellValue("Program Studi");
        department.createCell(4).setCellValue(":");
        department.createCell(5).setCellValue(mahasiswa.getIdProdi().getNamaProdi());
        department.getCell(2).setCellStyle(styleData);
        department.getCell(4).setCellStyle(styleSymbol);
        department.getCell(5).setCellStyle(styleData);

        int rowInfoFaculty = 18;
        Row faculty = sheet.createRow(rowInfoFaculty);
        faculty.createCell(2).setCellValue("Fakultas");
        faculty.createCell(4).setCellValue(":");
        faculty.createCell(5).setCellValue(mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        faculty.getCell(2).setCellStyle(styleData);
        faculty.getCell(4).setCellStyle(styleSymbol);
        faculty.getCell(5).setCellStyle(styleData);

        int rowInfoNoAcred = 19;
        Row accreditation = sheet.createRow(rowInfoNoAcred);
        accreditation.createCell(2).setCellValue("Nomor Akreditasi ");
        accreditation.createCell(4).setCellValue(":");
        accreditation.createCell(5).setCellValue(mahasiswa.getIdProdi().getNoSk());
        accreditation.getCell(2).setCellStyle(styleData);
        accreditation.getCell(4).setCellStyle(styleSymbol);
        accreditation.getCell(5).setCellStyle(styleData);

        int rowInfoDateAcred = 20;
        Row dateAccreditation = sheet.createRow(rowInfoDateAcred);
        dateAccreditation.createCell(2).setCellValue("Tanggal SK Akreditasi ");
        dateAccreditation.createCell(4).setCellValue(":");
        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 1) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Januari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 2) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Februari" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 3) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Maret" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 4) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " April" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 5) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Mei"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 6) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juni"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 7) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth() + " Juli"
                            + " " + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 8) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Agustus" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 9) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " September" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 10) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Oktober" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 11) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " November" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getIdProdi().getTanggalSk().getMonthValue() == 12) {
            dateAccreditation.createCell(5)
                    .setCellValue(mahasiswa.getIdProdi().getTanggalSk().getDayOfMonth()
                            + " Desember" + " "
                            + mahasiswa.getIdProdi().getTanggalSk().getYear());
            dateAccreditation.getCell(5).setCellStyle(styleData);

        }

        dateAccreditation.getCell(2).setCellStyle(styleData);
        dateAccreditation.getCell(4).setCellStyle(styleSymbol);

        int rowInfoGraduatedDate = 21;
        Row graduatedDate = sheet.createRow(rowInfoGraduatedDate);
        graduatedDate.createCell(2).setCellValue("Tanggal Kelulusan");
        graduatedDate.createCell(4).setCellValue(":");
        if (mahasiswa.getTanggalLulus().getMonthValue() == 1) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Januari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 2) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Februari" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 3) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Maret" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 4) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " April" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 5) {
            graduatedDate.createCell(5).setCellValue(
                    mahasiswa.getTanggalLulus().getDayOfMonth() + " Mei" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 6) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juni" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 7) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Juli" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 8) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Agustus" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 9) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " September" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 10) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Oktober" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 11) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " November" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        if (mahasiswa.getTanggalLulus().getMonthValue() == 12) {
            graduatedDate.createCell(5)
                    .setCellValue(mahasiswa.getTanggalLulus().getDayOfMonth() + " Desember" + " "
                            + mahasiswa.getTanggalLulus().getYear());
            graduatedDate.getCell(5).setCellStyle(styleData);

        }

        graduatedDate.getCell(2).setCellStyle(styleData);
        graduatedDate.getCell(4).setCellStyle(styleSymbol);

        int rowInfoTranscript = 10;
        Row transcript = sheet.createRow(rowInfoTranscript);
        sheet.addMergedRegion(new CellRangeAddress(rowInfoTranscript, rowInfoTranscript, 1, 10));
        transcript.createCell(1).setCellValue("Nomor : " + mahasiswa.getNoTranskript());
        transcript.getCell(1).setCellStyle(styleDataNew);

        int rowNumSemester1 = 24;
        for (DataTranskript sem1 : semester1) {
            Row row = sheet.createRow(rowNumSemester1);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester1, rowNumSemester1, 8, 9));

            row.createCell(1).setCellValue(sem1.getKode());
            row.createCell(2).setCellValue(sem1.getMatkul());
            row.createCell(6).setCellValue(sem1.getSks());
            row.createCell(7).setCellValue(sem1.getGrade());
            row.createCell(8).setCellValue(sem1.getBobot().toString());
            row.createCell(10).setCellValue(sem1.getMutu().toString());

            rowNumSemester1++;

            if (24 + semester1.size() == rowNumSemester1) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester2 = 24 + semester1.size();
        for (DataTranskript sem2 : semester2) {
            Row row = sheet.createRow(rowNumSemester2);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester2, rowNumSemester2, 8, 9));

            row.createCell(1).setCellValue(sem2.getKode());
            row.createCell(2).setCellValue(sem2.getMatkul());
            row.createCell(6).setCellValue(sem2.getSks());
            row.createCell(7).setCellValue(sem2.getGrade());
            row.createCell(8).setCellValue(sem2.getBobot().toString());
            row.createCell(10).setCellValue(sem2.getMutu().toString());

            rowNumSemester2++;

            if (24 + semester1.size() + semester2.size() == rowNumSemester2) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester3 = 24 + semester1.size() + semester2.size();
        for (DataTranskript sem3 : semester3) {
            Row row = sheet.createRow(rowNumSemester3);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester3, rowNumSemester3, 8, 9));

            row.createCell(1).setCellValue(sem3.getKode());
            row.createCell(2).setCellValue(sem3.getMatkul());
            row.createCell(6).setCellValue(sem3.getSks());
            row.createCell(7).setCellValue(sem3.getGrade());
            row.createCell(8).setCellValue(sem3.getBobot().toString());
            row.createCell(10).setCellValue(sem3.getMutu().toString());

            rowNumSemester3++;

            if (24 + semester1.size() + semester2.size() + semester3.size() == rowNumSemester3) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester4 = 24 + semester1.size() + semester2.size() + semester3.size();
        for (DataTranskript sem4 : semester4) {
            Row row = sheet.createRow(rowNumSemester4);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester4, rowNumSemester4, 8, 9));

            row.createCell(1).setCellValue(sem4.getKode());
            row.createCell(2).setCellValue(sem4.getMatkul());
            row.createCell(6).setCellValue(sem4.getSks());
            row.createCell(7).setCellValue(sem4.getGrade());
            row.createCell(8).setCellValue(sem4.getBobot().toString());
            row.createCell(10).setCellValue(sem4.getMutu().toString());

            rowNumSemester4++;

            if (24 + semester1.size() + semester2.size() + semester3.size()
                    + semester4.size() == rowNumSemester4) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester5 = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size();
        for (DataTranskript sem5 : semester5) {
            Row row = sheet.createRow(rowNumSemester5);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester5, rowNumSemester5, 8, 9));

            row.createCell(1).setCellValue(sem5.getKode());
            row.createCell(2).setCellValue(sem5.getMatkul());
            row.createCell(6).setCellValue(sem5.getSks());
            row.createCell(7).setCellValue(sem5.getGrade());
            row.createCell(8).setCellValue(sem5.getBobot().toString());
            row.createCell(10).setCellValue(sem5.getMutu().toString());

            rowNumSemester5++;

            if (24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                    + semester5.size() == rowNumSemester5) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester6 = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size();
        for (DataTranskript sem6 : semester6) {
            Row row = sheet.createRow(rowNumSemester6);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester6, rowNumSemester6, 8, 9));

            row.createCell(1).setCellValue(sem6.getKode());
            row.createCell(2).setCellValue(sem6.getMatkul());
            row.createCell(6).setCellValue(sem6.getSks());
            row.createCell(7).setCellValue(sem6.getGrade());
            row.createCell(8).setCellValue(sem6.getBobot().toString());
            row.createCell(10).setCellValue(sem6.getMutu().toString());

            rowNumSemester6++;

            if (24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                    + semester5.size()
                    + semester6.size() == rowNumSemester6) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester7 = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size();
        for (DataTranskript sem7 : semester7) {
            Row row = sheet.createRow(rowNumSemester7);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester7, rowNumSemester7, 8, 9));

            row.createCell(1).setCellValue(sem7.getKode());
            row.createCell(2).setCellValue(sem7.getMatkul());
            row.createCell(6).setCellValue(sem7.getSks());
            row.createCell(7).setCellValue(sem7.getGrade());
            row.createCell(8).setCellValue(sem7.getBobot().toString());
            row.createCell(10).setCellValue(sem7.getMutu().toString());

            rowNumSemester7++;

            if (24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                    + semester5.size()
                    + semester6.size() + semester7.size() == rowNumSemester7) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }

        int rowNumSemester8 = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size();
        for (DataTranskript sem8 : semester8) {
            Row row = sheet.createRow(rowNumSemester8);
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 2, 5));
            sheet.addMergedRegion(new CellRangeAddress(rowNumSemester8, rowNumSemester8, 8, 9));

            row.createCell(1).setCellValue(sem8.getKode());
            row.createCell(2).setCellValue(sem8.getMatkul());
            row.createCell(6).setCellValue(sem8.getSks());
            row.createCell(7).setCellValue(sem8.getGrade());
            row.createCell(8).setCellValue(sem8.getBobot().toString());
            row.createCell(10).setCellValue(sem8.getMutu().toString());
            rowNumSemester8++;

            if (24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                    + semester5.size()
                    + semester6.size() + semester7.size() + semester8.size() == rowNumSemester8) {
                row.getCell(1).setCellStyle(styleDataTale2);
                row.getCell(2).setCellStyle(styleDataKhsTable2);
                row.createCell(3).setCellStyle(styleDataKhsTable2);
                row.createCell(4).setCellStyle(styleDataKhsTable2);
                row.createCell(5).setCellStyle(styleDataKhsTable2);
                row.getCell(6).setCellStyle(styleDataTale2);
                row.getCell(7).setCellStyle(styleDataTale2);
                row.getCell(8).setCellStyle(styleDataTale2);
                row.getCell(10).setCellStyle(styleDataTale2);
            } else {
                row.getCell(1).setCellStyle(styleDataTale);
                row.getCell(2).setCellStyle(styleDataKhsTable);
                row.createCell(3).setCellStyle(styleDataKhsTable);
                row.createCell(4).setCellStyle(styleDataKhsTable);
                row.createCell(5).setCellStyle(styleDataKhsTable);
                row.getCell(6).setCellStyle(styleDataTale);
                row.getCell(7).setCellStyle(styleDataTale);
                row.getCell(8).setCellStyle(styleDataTale);
                row.getCell(10).setCellStyle(styleDataTale);
            }
        }
        int total = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size();
        Row rowTotal = sheet.createRow(total);
        sheet.addMergedRegion(new CellRangeAddress(total, total, 2, 5));
        rowTotal.createCell(2).setCellValue("Jumlah");
        rowTotal.createCell(3).setCellStyle(styleTotalBorder);
        rowTotal.createCell(4).setCellStyle(styleTotalBorder);
        rowTotal.createCell(5).setCellStyle(styleTotalBorder);
        rowTotal.createCell(6).setCellValue(totalSKS);
        rowTotal.createCell(10).setCellValue(totalMuti.toString());
        rowTotal.getCell(2).setCellStyle(styleTotalBorder);
        rowTotal.getCell(6).setCellStyle(styleDataTale2);
        rowTotal.getCell(10).setCellStyle(styleDataTale2);

        int ipKomulatif = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 2;
        Row rowIpk = sheet.createRow(ipKomulatif);
        rowIpk.createCell(1).setCellValue("Indeks Prestasi Kumulatif");
        rowIpk.createCell(6).setCellValue(ipk.toString());
        rowIpk.getCell(1).setCellStyle(styleTotal);
        rowIpk.getCell(6).setCellStyle(styleIsiIpk);

        int predicate = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 4;
        Row predicateRow = sheet.createRow(predicate);
        predicateRow.createCell(1).setCellValue("Predikat :");
        if (ipk.compareTo(new BigDecimal(2.99)) <= 0) {
            predicateRow.createCell(2).setCellValue("Memuaskan");
            predicateRow.getCell(2).setCellStyle(styleData);

        }

        if (ipk.compareTo(new BigDecimal(3.00)) >= 0 && ipk.compareTo(new BigDecimal(3.49)) <= 0) {
            predicateRow.createCell(2).setCellValue("Sangat Memuaskan");
            predicateRow.getCell(2).setCellStyle(styleData);

        }

        if (ipk.compareTo(new BigDecimal(3.50)) >= 0 && ipk.compareTo(new BigDecimal(3.79)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(2).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(2).setCellStyle(styleData);
            } else {
                predicateRow.createCell(2).setCellValue("Pujian ");
                predicateRow.getCell(2).setCellStyle(styleData);
            }

        }

        if (ipk.compareTo(new BigDecimal(3.80)) >= 0 && ipk.compareTo(new BigDecimal(4.00)) <= 0) {
            Integer validate = krsDetailDao.validasiPredikat(mahasiswa);
            if (!validate.equals(0)) {
                predicateRow.createCell(2).setCellValue("Sangat Memuaskan");
                predicateRow.getCell(2).setCellStyle(styleData);
            } else {
                predicateRow.createCell(2).setCellValue("Pujian Tertinggi");
                predicateRow.getCell(2).setCellStyle(styleData);
            }

        }

        predicateRow.getCell(1).setCellStyle(styleSubHeader);

        int thesis = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 5;
        Row thesisRow = sheet.createRow(thesis);
        sheet.addMergedRegion(new CellRangeAddress(thesis, thesis + 1, 1, 1));
        sheet.addMergedRegion(new CellRangeAddress(thesis, thesis + 1, 2, 10));
        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("01").get()) {
            thesisRow.createCell(1).setCellValue("Judul Skripsi :");
            thesisRow.createCell(2).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(1).setCellStyle(styleJudul);
            thesisRow.getCell(2).getRow()
                    .setHeightInPoints(thesisRow.getSheet().getDefaultRowHeightInPoints() * 2);
            thesisRow.getCell(2).setCellStyle(styleThesis);
        }

        if (mahasiswa.getIdProdi().getIdJenjang() == jenjangDao.findById("02").get()) {
            thesisRow.createCell(1).setCellValue("Judul Tesis :");
            thesisRow.createCell(2).setCellValue(mahasiswa.getJudul());
            thesisRow.getCell(1).setCellStyle(styleJudul);
            thesisRow.getCell(2).getRow()
                    .setHeightInPoints(thesisRow.getSheet().getDefaultRowHeightInPoints() * 2);
            thesisRow.getCell(2).setCellStyle(styleThesis);
        }

        int keyResult = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 8;
        Row resultRow = sheet.createRow(keyResult);
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 1, 4));
        sheet.addMergedRegion(new CellRangeAddress(keyResult, keyResult + 1, 6, 10));
        resultRow.createCell(1).setCellValue("Prestasi Akademik");
        resultRow.createCell(2).setCellStyle(stylePres);
        resultRow.createCell(3).setCellStyle(stylePres);
        resultRow.createCell(4).setCellStyle(stylePres);
        resultRow.createCell(6).setCellValue("Sistem Penilaian");
        resultRow.createCell(7).setCellStyle(stylePres);
        resultRow.createCell(8).setCellStyle(stylePres);
        resultRow.createCell(9).setCellStyle(stylePres);
        resultRow.createCell(10).setCellStyle(stylePres);
        resultRow.getCell(1).setCellStyle(stylePres);
        resultRow.getCell(6).setCellStyle(stylePres);

        int border = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 9;
        Row borderRow = sheet.createRow(border);
        borderRow.createCell(1).setCellStyle(stylePres);
        borderRow.createCell(2).setCellStyle(stylePres);
        borderRow.createCell(3).setCellStyle(stylePres);
        borderRow.createCell(4).setCellStyle(stylePres);
        borderRow.createCell(6).setCellStyle(stylePres);
        borderRow.createCell(7).setCellStyle(stylePres);
        borderRow.createCell(8).setCellStyle(stylePres);
        borderRow.createCell(9).setCellStyle(stylePres);
        borderRow.createCell(10).setCellStyle(stylePres);

        int remark = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 10;
        Row remarkRow = sheet.createRow(remark);
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 1, 4));
        sheet.addMergedRegion(new CellRangeAddress(remark, remark, 8, 10));
        remarkRow.createCell(1).setCellValue("Keterangan");
        remarkRow.createCell(2).setCellStyle(stylePres);
        remarkRow.createCell(3).setCellStyle(stylePres);
        remarkRow.createCell(4).setCellStyle(stylePres);
        remarkRow.createCell(6).setCellValue("HM");
        remarkRow.createCell(7).setCellValue("AM");
        remarkRow.createCell(8).setCellValue("Arti");
        remarkRow.getCell(1).setCellStyle(stylePres);
        remarkRow.getCell(6).setCellStyle(styleIpk);
        remarkRow.getCell(7).setCellStyle(styleIpk);
        remarkRow.getCell(8).setCellStyle(styleIpk);

        int excellent = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 11;
        Row excellentRow = sheet.createRow(excellent);
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 2, 4));
        sheet.addMergedRegion(new CellRangeAddress(excellent, excellent, 8, 10));
        excellentRow.createCell(1).setCellValue("3,80-4,00");
        excellentRow.createCell(2).setCellValue("Pujian Tertinggi (Minimal B)");
        excellentRow.createCell(3).setCellStyle(styleDataPrestasiAkademik);
        excellentRow.createCell(4).setCellStyle(styleDataPrestasiAkademik);
        excellentRow.createCell(6).setCellValue("A");
        excellentRow.createCell(7).setCellValue("4");
        excellentRow.createCell(8).setCellValue("Baik Sekali");
        excellentRow.getCell(1).setCellStyle(styleProdi);
        excellentRow.getCell(2).setCellStyle(styleDataPrestasiAkademik);
        excellentRow.getCell(6).setCellStyle(stylePenilaian);
        excellentRow.getCell(7).setCellStyle(stylePenilaian);
        excellentRow.getCell(8).setCellStyle(styleManajemen);

        int veryGood = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 12;
        Row veryGoodRow = sheet.createRow(veryGood);
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 2, 4));
        sheet.addMergedRegion(new CellRangeAddress(veryGood, veryGood, 8, 10));
        veryGoodRow.createCell(1).setCellValue("3,50-3,79");
        veryGoodRow.createCell(2).setCellValue("Pujian (Minimal B)");
        veryGoodRow.createCell(3).setCellStyle(styleDataPrestasiAkademik);
        veryGoodRow.createCell(4).setCellStyle(styleDataPrestasiAkademik);
        veryGoodRow.createCell(6).setCellValue("A-");
        veryGoodRow.createCell(7).setCellValue("3,7");
        veryGoodRow.createCell(8).setCellValue("Baik Sekali");
        veryGoodRow.getCell(1).setCellStyle(styleProdi);
        veryGoodRow.getCell(2).setCellStyle(styleDataPrestasiAkademik);
        veryGoodRow.getCell(6).setCellStyle(stylePenilaian);
        veryGoodRow.getCell(7).setCellStyle(stylePenilaian);
        veryGoodRow.getCell(8).setCellStyle(styleManajemen);

        int good = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 13;
        Row goodRow = sheet.createRow(good);
        sheet.addMergedRegion(new CellRangeAddress(good, good, 2, 4));
        sheet.addMergedRegion(new CellRangeAddress(good, good, 8, 10));
        goodRow.createCell(1).setCellValue("3,00-3,49");
        goodRow.createCell(2).setCellValue("Sangat Memuaskan");
        goodRow.createCell(3).setCellStyle(styleDataPrestasiAkademik);
        goodRow.createCell(4).setCellStyle(styleDataPrestasiAkademik);
        goodRow.createCell(6).setCellValue("B+");
        goodRow.createCell(7).setCellValue("3,3");
        goodRow.createCell(8).setCellValue("Baik");
        goodRow.getCell(1).setCellStyle(styleProdi);
        goodRow.getCell(2).setCellStyle(styleDataPrestasiAkademik);
        goodRow.getCell(6).setCellStyle(stylePenilaian);
        goodRow.getCell(7).setCellStyle(stylePenilaian);
        goodRow.getCell(8).setCellStyle(styleManajemen);

        int satisfactory = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 14;
        Row satisfactoryRow = sheet.createRow(satisfactory);
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 2, 4));
        sheet.addMergedRegion(new CellRangeAddress(satisfactory, satisfactory, 8, 10));
        satisfactoryRow.createCell(1).setCellValue("2,75-2,99");
        satisfactoryRow.createCell(2).setCellValue("Memuaskan");
        satisfactoryRow.createCell(3).setCellStyle(styleDataPrestasiAkademik);
        satisfactoryRow.createCell(4).setCellStyle(styleDataPrestasiAkademik);
        satisfactoryRow.createCell(6).setCellValue("B");
        satisfactoryRow.createCell(7).setCellValue("3");
        satisfactoryRow.createCell(8).setCellValue("Baik");
        satisfactoryRow.getCell(1).setCellStyle(styleProdi);
        satisfactoryRow.getCell(2).setCellStyle(styleDataPrestasiAkademik);
        satisfactoryRow.getCell(6).setCellStyle(stylePenilaian);
        satisfactoryRow.getCell(7).setCellStyle(stylePenilaian);
        satisfactoryRow.getCell(8).setCellStyle(styleManajemen);

        int almostGood = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 15;
        Row almostGoodRow = sheet.createRow(almostGood);
        sheet.addMergedRegion(new CellRangeAddress(almostGood, almostGood, 8, 10));
        almostGoodRow.createCell(6).setCellValue("B-");
        almostGoodRow.createCell(7).setCellValue("2,7");
        almostGoodRow.createCell(8).setCellValue("Baik");
        almostGoodRow.getCell(6).setCellStyle(stylePenilaian);
        almostGoodRow.getCell(7).setCellStyle(stylePenilaian);
        almostGoodRow.getCell(8).setCellStyle(styleManajemen);

        int satisfactoryCplus = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 16;
        Row satisfactoryCplusRow = sheet.createRow(satisfactoryCplus);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryCplus, satisfactoryCplus, 8, 10));
        satisfactoryCplusRow.createCell(6).setCellValue("C+");
        satisfactoryCplusRow.createCell(7).setCellValue("2,3");
        satisfactoryCplusRow.createCell(8).setCellValue("Cukup");
        satisfactoryCplusRow.getCell(6).setCellStyle(stylePenilaian);
        satisfactoryCplusRow.getCell(7).setCellStyle(stylePenilaian);
        satisfactoryCplusRow.getCell(8).setCellStyle(styleManajemen);

        int satisfactoryC = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 17;
        Row satisfactoryCRow = sheet.createRow(satisfactoryC);
        sheet.addMergedRegion(new CellRangeAddress(satisfactoryC, satisfactoryC, 8, 10));
        satisfactoryCRow.createCell(6).setCellValue("C");
        satisfactoryCRow.createCell(7).setCellValue("2");
        satisfactoryCRow.createCell(8).setCellValue("Cukup");
        satisfactoryCRow.getCell(6).setCellStyle(stylePenilaian);
        satisfactoryCRow.getCell(7).setCellStyle(stylePenilaian);
        satisfactoryCRow.getCell(8).setCellStyle(styleManajemen);

        int poor = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 18;
        Row poorRow = sheet.createRow(poor);
        sheet.addMergedRegion(new CellRangeAddress(poor, poor, 8, 10));
        poorRow.createCell(6).setCellValue("D");
        poorRow.createCell(7).setCellValue("1");
        poorRow.createCell(8).setCellValue("Kurang");
        poorRow.getCell(6).setCellStyle(stylePenilaian);
        poorRow.getCell(7).setCellStyle(stylePenilaian);
        poorRow.getCell(8).setCellStyle(styleManajemen);

        int fail = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 19;
        Row failRow = sheet.createRow(fail);
        sheet.addMergedRegion(new CellRangeAddress(fail, fail, 8, 10));
        failRow.createCell(6).setCellValue("E");
        failRow.createCell(7).setCellValue("0");
        failRow.createCell(8).setCellValue("Sangat Kurang");
        failRow.getCell(6).setCellStyle(stylePenilaian);
        failRow.getCell(7).setCellStyle(stylePenilaian);
        failRow.getCell(8).setCellStyle(styleManajemen);

        int createDate = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size() + semester6.size() + semester7.size() + semester8.size() + 22;
        Row createDateRow = sheet.createRow(createDate);
        HijrahDate islamicDate = HijrahDate
                .from(mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan());
        String namaBulanHijri = islamicDate.format(DateTimeFormatter.ofPattern("MMMM", new Locale("en")));
        String tanggalHijri = islamicDate.format(DateTimeFormatter.ofPattern("dd", new Locale("en")));
        String tahunHijri = islamicDate.format(DateTimeFormatter.ofPattern("yyyy", new Locale("en")));

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 1) {

            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Januari " + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Januari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Januari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Januari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Januari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 2) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Februari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Februari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Februari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Februari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Februari" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 3) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Maret" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Maret" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Maret" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Maret" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Maret" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 4) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " April" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " April" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " April" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " April" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " April" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 5) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Mei" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Mei" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Mei" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Mei" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Mei" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 6) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juni" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juni" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juni" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juni" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juni" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 7) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juli" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juli" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juli" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juli" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Juli" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 8) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Agustus" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Agustus" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Agustus" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Agustus" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Agustus" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 9) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " September" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " September" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " September" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " September" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " September" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 10) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Oktober" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Oktober" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Oktober" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Oktober" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Oktober" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 11) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " November" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " November" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " November" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " November" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor, "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " November" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        if (mahasiswaDao.cekTanggalDisahkan(mahasiswa.getAngkatan()).getTanggalDisahkan()
                .getMonthValue() == 12) {
            if (namaBulanHijri.equals("Jumada I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Desember" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Jumada II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Desember" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Jumadil Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ I")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Desember" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Awal " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else if (namaBulanHijri.equals("Rabiʻ II")) {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Desember" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan().getYear()
                                + " / " + tanggalHijri + " Rabi'ul Akhir " + tahunHijri
                                + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            } else {
                createDateRow.createCell(1)
                        .setCellValue("Transkrip ini dibuat dengan sebenarnya dan telah disahkan di Bogor,  "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getDayOfMonth()
                                + " Desember" + " "
                                + mahasiswaDao.cekTanggalDisahkan(
                                        mahasiswa.getAngkatan())
                                .getTanggalDisahkan()
                                .getYear()
                                + " / " + tanggalHijri + " " + namaBulanHijri + " "
                                + tahunHijri + " H");
                createDateRow.getCell(1).setCellStyle(styleData);
            }

        }

        int facultyy = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 24;
        Row facultyRow = sheet.createRow(facultyy);
        facultyRow.createCell(6).setCellValue("Dekan ");
        facultyRow.getCell(6).setCellStyle(styleData);

        int faculty2 = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 25;
        Row facultyRow2 = sheet.createRow(faculty2);
        facultyRow2.createCell(6)
                .setCellValue("Fakultas " + mahasiswa.getIdProdi().getFakultas().getNamaFakultas());
        facultyRow2.getCell(6).setCellStyle(styleData);

        int lecture = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 30;
        Row lectureRow = sheet.createRow(lecture);
        lectureRow.createCell(6)
                .setCellValue(mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNamaKaryawan());
        lectureRow.getCell(6).setCellStyle(styleDosen);

        int nik = 24 + semester1.size() + semester2.size() + semester3.size() + semester4.size()
                + semester5.size()
                + semester6.size() + semester7.size() + semester8.size() + 31;
        Row nikRow = sheet.createRow(nik);
        nikRow.createCell(6)
                .setCellValue("NIK : " + mahasiswa.getIdProdi().getFakultas().getDosen().getKaryawan()
                        .getNik());
        nikRow.getCell(6).setCellStyle(styleNik);

        String namaFile = "Transkript-" + mahasiswa.getNim() + "-" + mahasiswa.getNama();
        String extentionX = ".xlsx";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + namaFile + extentionX + "\"");
        workbook.write(response.getOutputStream());
        workbook.close();
    }

    @GetMapping("/check-presensi-dosen/{id}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse checkPresensiDosenHariIni(
            @PathVariable String id) {
        Jadwal jadwal = jadwalDao.findById(id).get();
        SesiKuliah sesiKuliah = sesiKuliahDao.cariSesiKuliahHariIni(jadwal);

        if (sesiKuliah == null) {
            id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = new id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse();
            response.setCode("404");
            response.setMessage("Sesi Kuliah Belum Dibuat");
            return response;
        } else {
            id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = new id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse();
            response.setCode("200");
            response.setMessage("Sesi Kuliah Telah Dibuat");
            return response;
        }

    }

    @GetMapping("/generate-sesikuliah/{id}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse generatePresensi(@PathVariable String id)
            throws Exception {
        Jadwal jadwal = jadwalDao.findById(id).get();
        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = presensiService.generateSesi(jadwal);

        return response;

    }

    @GetMapping("/generate-presensi-mahasiswa/{id}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse generatePresensiMahasiswa(@PathVariable String id)
            throws Exception {
        SesiKuliah sesiKuliah = sesiKuliahDao.findById(id).get();
        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = presensiService
                .generatePresensiMahasiswa(sesiKuliah);

        return response;

    }

    @GetMapping("/generate-sesikuliah")
    @ResponseBody
    public void generatePresensi()
            throws Exception {
        List<Jadwal> cekJadwal = jadwalDao.findByStatusAndTahunAkademikAndFinalPloting(StatusRecord.AKTIF,
                tahunAkademikDao.findByStatus(StatusRecord.AKTIF), "FINAL");
        for (Jadwal j : cekJadwal) {
            System.out.println(j.getId() + " : id jadwal");
            presensiService.generateSesi2(j);

        }

    }

    @GetMapping("/updateall-sesikuliah/{tahun}")
    @ResponseBody
    public void updatePresensiAll(@PathVariable String tahun)
            throws Exception {
        List<Jadwal> jadwals = jadwalDao.findByStatusAndTahunAkademikAndFinalPloting(StatusRecord.AKTIF,
                tahunAkademikDao.findById(tahun).get(), "FINISHED");
        for (Jadwal jadwal : jadwals) {
            presensiService.updateSesi1(jadwal);
        }
    }

    @GetMapping("/update-sesikuliah/{id}")
    @ResponseBody
    public void updatePresensi(@PathVariable String id)
            throws Exception {
        Jadwal jadwal = jadwalDao.findById(id).get();
        presensiService.updateSesi1(jadwal);

    }

    @PostMapping("/import-ipk-sks")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse importIpkSks() {

        List<Mahasiswa> mahasiswaAktif = mahasiswaDao.findByStatusAndStatusAktif(StatusRecord.AKTIF, "AKTIF");
        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = mahasiswaService
                .importIpkSks(mahasiswaAktif);

        return response;
    }

    @PostMapping("/import-ipk-sks/{tahun}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse importIpkSksTahun(@PathVariable String tahun) {

        TahunAkademik tahunAkademik = tahunAkademikDao.findById(tahun).get();
        List<Krs> krsAktif = krsDao.findByTahunAkademikAndStatus(tahunAkademik, StatusRecord.AKTIF);

        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = mahasiswaService
                .importIpkSks(krsAktif.stream().map(Krs::getMahasiswa)
                        .collect(Collectors.toList()));

        return response;
    }

    @PostMapping("/import-ipk-sks/angkatan/{angkatan}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse importIpkSksAngkatan(@PathVariable String angkatan) {

        List<Mahasiswa> mhs = mahasiswaDao.findByStatusAndStatusAktifAndAngkatan(StatusRecord.AKTIF, "AKTIF", angkatan);

        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = mahasiswaService.importIpkSks(mhs);

        return response;
    }

    @PostMapping("/import-ipk-krs/{angkatan}")
    @ResponseBody
    public id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse importIpkKrs(@PathVariable String angkatan) {

        List<Mahasiswa> mahasiswa = mahasiswaDao.findByStatusAndAngkatan(StatusRecord.AKTIF, angkatan);
        id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse response = krsService.updateIpKrsApi(mahasiswa);

        return response;
    }

    @GetMapping("/regenerate-sesikuliah/{tanggal}")
    @ResponseBody
    public void regeneratePresensiPerJadwal(@PathVariable String tanggal) throws Exception {
        List<Jadwal> cekJadwal = jadwalDao.findByStatusAndTahunAkademikAndFinalPloting(StatusRecord.AKTIF, tahunAkademikDao.findByStatus(StatusRecord.AKTIF), "REGENERATE");
        for (Jadwal j : cekJadwal) {
            System.out.println(j.getId() + " : id jadwal");
            presensiService.reGenerate(j, tanggal);
        }
    }


    @PostMapping("/studiesActivity/attendance/tambahfoto")
    public String tambahFilePerkuliahan(@RequestParam SesiKuliah sesiKuliah,
                                        @RequestParam("fotoKuliah") MultipartFile file,
                                        Authentication authentication,
                                        RedirectAttributes redirectAttributes) throws Exception {

        User user = currentUserService.currentUser(authentication);
//        Karyawan karyawan = karyawanDao.findByUser(user);
//        Dosen dosen = dosenDao.findByKaryawan(karyawan);


        String namaAsli = file.getOriginalFilename();
        if (namaAsli == null || !isImageFile(namaAsli)) {
            redirectAttributes.addFlashAttribute("error", "File yang diunggah bukan gambar");
            return "redirect:../attendance/detail?jadwal="+ sesiKuliah.getJadwal().getId();
        }

//        SesiKuliah sesiKuliah = sesiKuliahDao.findById(id).get();
        SesiKuliahFoto sesiKuliahFoto = new SesiKuliahFoto();
        sesiKuliahFoto.setJadwal(sesiKuliah.getJadwal());
        sesiKuliahFoto.setSesiKuliah(sesiKuliah);
        sesiKuliahFoto.setNamaFile(namaAsli);

        // Ekstrak ekstensi file
        String extension = namaAsli.substring(namaAsli.lastIndexOf('.') + 1);
        String idFile1 = UUID.randomUUID().toString();
        String namaFileSimpan = idFile1 + "." + extension;
        sesiKuliahFoto.setFile(namaFileSimpan);

        // Buat direktori jika belum ada
        new File(uploadFotoKuliah).mkdirs();
        File tujuan2 = new File(uploadFotoKuliah + File.separator + namaFileSimpan);


        // Konversi dan kompres gambar agar di bawah 100KB
        Thumbnails.of(file.getInputStream())
                .size(500, 500) // Mengurangi ukuran maksimal
                .outputFormat(extension)
                .outputQuality(0.2) // Menurunkan kualitas untuk kompresi lebih besar
                .toFile(tujuan2);

// Validasi ukuran file agar kurang dari 100KB
        if (tujuan2.length() > 50 * 1024) {
            Thumbnails.of(tujuan2)
                    .scale(1.0) // tidak mengubah ukuran
                    .outputQuality(0.2) // Kompres lebih lanjut jika masih terlalu besar
                    .toFile(tujuan2);
        }

        sesiKuliahFoto.setUserUpload(user.getUsername());
        sesiKuliahFoto.setStatus(StatusRecord.AKTIF);
        sesiKuliahFoto.setTanggalUpload(LocalDateTime.now());
        sesiKuliahFotoDao.save(sesiKuliahFoto);

        redirectAttributes.addFlashAttribute("success", "Foto perkuliahan berhasil ditambahkan");
        return "redirect:../attendance/detail?jadwal="+ sesiKuliah.getJadwal().getId();
    }


    @GetMapping("/studiesActivity/attendance/hapusfoto")
    public String lappranBkdPendidikanHapusFile(@RequestParam(required = true)SesiKuliahFoto sesiKuliahFoto,
                                                Authentication authentication,
                                                RedirectAttributes redirectAttributes){

        User user = currentUserService.currentUser(authentication);

        sesiKuliahFoto.setStatus(StatusRecord.HAPUS);
        sesiKuliahFoto.setUserDelete(user.getUsername());
        sesiKuliahFoto.setTanggalDelete(LocalDateTime.now());
        sesiKuliahFotoDao.save(sesiKuliahFoto);


        redirectAttributes.addFlashAttribute("success", "File berhasil dihapus");
        return "redirect:../attendance/detail?jadwal="+ sesiKuliahFoto.getSesiKuliah().getJadwal().getId();
    }

    // Fungsi untuk validasi ekstensi gambar
    private boolean isImageFile(String fileName) {
        String extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
        return extension.equals("png") || extension.equals("jpg") || extension.equals("jpeg");
    }


    @GetMapping("/studiesActivity/{sesiKuliahFoto}/fotoKuliah/")
    public ResponseEntity<byte[]> tampilkanSesiKuliahFoto(@PathVariable SesiKuliahFoto sesiKuliahFoto) throws Exception {
        String lokasiFile = uploadFotoKuliah + File.separator + sesiKuliahFoto.getFile();
        LOGGER.debug("Lokasi file bukti : {}", lokasiFile);

        try {
            HttpHeaders headers = new HttpHeaders();
            if (sesiKuliahFoto.getNamaFile().toLowerCase().endsWith("jpeg") || sesiKuliahFoto.getNamaFile().toLowerCase().endsWith("jpg")) {
                headers.setContentType(MediaType.IMAGE_JPEG);
            } else if (sesiKuliahFoto.getNamaFile().toLowerCase().endsWith("png")) {
                headers.setContentType(MediaType.IMAGE_PNG);
            } else if (sesiKuliahFoto.getNamaFile().toLowerCase().endsWith("pdf")) {
                headers.setContentType(MediaType.APPLICATION_PDF);
            } else {
                headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            }
            byte[] data = Files.readAllBytes(Paths.get(lokasiFile));
            return new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
        } catch (Exception err) {
            LOGGER.warn(err.getMessage(), err);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

}

