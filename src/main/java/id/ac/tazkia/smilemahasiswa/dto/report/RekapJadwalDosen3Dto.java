package id.ac.tazkia.smilemahasiswa.dto.report;

public class RekapJadwalDosen3Dto {
    private Long idJadwal;
    private Long idDosen;
    private String namaDosen;
    private String namaMatakuliah;
    private Integer sks;
    private String namaProdi;
    private String namaKelas;
    private String namaHari;
    private Integer sesi;
    private String jamMulai;
    private String jamSelesai;
    private String namaRuangan;
    private String namaGedung;
    private Integer jumlahKehadiran;

    // Default constructor
    public RekapJadwalDosen3Dto() {}

    // Setter & Getter (bisa auto-generate via IDE)
    // ...
}
