package id.ac.tazkia.smilemahasiswa.dto.tlc.attendance;

public interface GetDataPresensi {
    String getId();

    String getJadwal();

    String getDosen();

    String getBerita();

    Integer getPertemuanke();

    String getWaktu();

    String getLink();

    String getPertemuan();

    String getJenis();

    String getMulai();

    String getSelesai();
}
