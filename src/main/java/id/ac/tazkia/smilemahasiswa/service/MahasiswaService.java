package id.ac.tazkia.smilemahasiswa.service;


import id.ac.tazkia.smilemahasiswa.dao.*;
import id.ac.tazkia.smilemahasiswa.dto.ImportMahasiswaDto;
import id.ac.tazkia.smilemahasiswa.dto.response.BaseResponse;
import id.ac.tazkia.smilemahasiswa.dto.transkript.DataTranskript;
import id.ac.tazkia.smilemahasiswa.dto.user.MahasiswaDto;
import id.ac.tazkia.smilemahasiswa.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@Transactional @Slf4j
public class MahasiswaService {
    @Autowired
    private MahasiswaDao mahasiswaDao;

    @Autowired
    private AyahDao ayahDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private IbuDao ibuDao;

    @Autowired
    private WaliDao waliDao;

    @Autowired
    private ProgramDao programDao;

    @Autowired
    private ProdiDao prodiDao;

    @Autowired
    private AgamaDao agamaDao;

    @Autowired
    private DosenDao dosenDao;

    @Autowired
    private MahasiswaDetailKeluargaDao mahasiswaDetailKeluargaDao;

    @Autowired
    private KurikulumDao kurikulumDao;

    @Autowired
    private KrsDetailDao krsDetailDao;

    @Autowired
    private IpkDao ipkDao;

    @Autowired
    private JenjangDao jenjangDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(MahasiswaService.class);

    public Mahasiswa prosesMahasiswa(MahasiswaDto mahasiswaDto){
        Mahasiswa mahasiswa = new Mahasiswa();
                BeanUtils.copyProperties(mahasiswaDto, mahasiswa);
        mahasiswa.setTempatLahir(mahasiswaDto.getTempat());

        if (mahasiswa.getUser() == null) {
            createUser(mahasiswa);
        }

        mahasiswaDao.save(mahasiswa);

        return mahasiswa;
    }

    private void createUser(Mahasiswa mahasiswa) {
        Role rolePendaftar = roleDao.findById("mahasiswa").get();

        User user = new User();
        user.setUsername(mahasiswa.getNim());
        user.setActive(true);
        user.setRole(rolePendaftar);
        userDao.save(user);

        mahasiswa.setUser(user);
    }

    public Ayah prosesAyah(MahasiswaDto mahasiswaDto, Mahasiswa mahasiswa){
        Ayah ayah = new Ayah();

        BeanUtils.copyProperties(mahasiswaDto, ayah);
        ayah.setId(mahasiswaDto.getAyah());
        ayah.setTanggalLahir(mahasiswaDto.getTanggalLahirAyah());
        ayah.setTempatLahir(mahasiswaDto.getTempatLahirAyah());
        ayah.setStatusHidup(mahasiswaDto.getHidup());


        ayahDao.save(ayah);

        return ayah;
    }

    public Ibu prosesIbu(MahasiswaDto mahasiswaDto, Mahasiswa mahasiswa){
        Ibu ibu = new Ibu();
        ibu.setId(mahasiswaDto.getId());
        ibu.setNamaIbuKandung(mahasiswaDto.getNamaIbuKandung());
        ibu.setKebutuhanKhusus(mahasiswaDto.getKebutuhanKhususIbu());
        ibu.setTempatLahir(mahasiswaDto.getTempatLahirIbu());
        ibu.setTanggalLahir(mahasiswaDto.getTanggalLahirIbu());
        ibu.setIdJenjangPendidikan(mahasiswaDto.getIdJenjangPendidikanIbu());
        ibu.setIdPekerjaan(mahasiswaDto.getIdPekerjaanIbu());
        ibu.setPenghasilan(mahasiswaDto.getPenghasilanIbu());
        ibu.setAgama(mahasiswaDto.getAgamaIbu());
        ibu.setStatusHidup(mahasiswaDto.getStatusHidupIbu());



        ibuDao.save(ibu);

        return ibu;
    }

    public Wali prosesWali(MahasiswaDto mahasiswaDto, Mahasiswa mahasiswa){
        Wali wali = new Wali();

        wali.setNamaWali(mahasiswaDto.getNamaWali());
        wali.setId(mahasiswaDto.getWali());
        wali.setKebutuhanKhusus(mahasiswaDto.getKebutuhanKhususWali());
        wali.setTempatLahir(mahasiswaDto.getTempatLahirWali());
        wali.setTanggalLahir(mahasiswaDto.getTanggalLahirWali());
        wali.setIdJenjangPendidikan(mahasiswaDto.getIdJenjangPendidikanWali());
        wali.setIdPekerjaan(mahasiswaDto.getIdPekerjaanWali());
        wali.setIdPenghasilan(mahasiswaDto.getIdPenghasilanWali());
        wali.setAgama(mahasiswaDto.getAgamaWali());
        waliDao.save(wali);
        return wali;
    }

    public Mahasiswa importMahasiswa(ImportMahasiswaDto importMahasiswaDto){

        Ayah ayah = new Ayah();
        ayah.setNamaAyah(importMahasiswaDto.getAyah());
        ayah.setTanggalLahir(LocalDate.parse("1982-05-14"));
        ayah.setAgama(agamaDao.findById(importMahasiswaDto.getIdAgama()).get());
        ayah.setStatusHidup("H");
        ayahDao.save(ayah);

        Ibu ibu = new Ibu();
        ibu.setNamaIbuKandung(importMahasiswaDto.getIbu());
        ibu.setTanggalLahir(LocalDate.parse("1982-05-14"));
        ibu.setAgama(agamaDao.findById(importMahasiswaDto.getIdAgama()).get());
        ibu.setStatusHidup("H");
        ibuDao.save(ibu);

        Role rolePendaftar = roleDao.findById("mahasiswa").get();
        User user = new User();
        user.setUsername(importMahasiswaDto.getNim());
        user.setActive(true);
        user.setRole(rolePendaftar);
        userDao.save(user);


        Mahasiswa mahasiswa = new Mahasiswa();
        BeanUtils.copyProperties(importMahasiswaDto,mahasiswa);
        mahasiswa.setIdProdi(prodiDao.findByKodeSpmb(importMahasiswaDto.getProdi()));
        mahasiswa.setIdKotaKabupaten(importMahasiswaDto.getKabupaten());
        mahasiswa.setIdProvinsi(importMahasiswaDto.getProvinsi());
        mahasiswa.setIdNegara(importMahasiswaDto.getNegara());
        mahasiswa.setDosen(dosenDao.findById("''").get());
        mahasiswa.setEmailPribadi(importMahasiswaDto.getEmail());
        mahasiswa.setTeleponRumah(importMahasiswaDto.getTelepon());
        mahasiswa.setTeleponSeluler(importMahasiswaDto.getTelepon());
        mahasiswa.setIdAgama(agamaDao.findById(importMahasiswaDto.getIdAgama()).get());
        mahasiswa.setStatusMatrikulasi("N");
        mahasiswa.setStatusAktif("AKTIF");
        mahasiswa.setAyah(ayah);
        mahasiswa.setIbu(ibu);
        mahasiswa.setKurikulum(kurikulumDao.findByProdiAndStatus(mahasiswa.getIdProdi(),StatusRecord.AKTIF));
        mahasiswa.setNamaJalan(importMahasiswaDto.getAlamat());
        mahasiswa.setIdAbsen(mahasiswaDao.cariMaxAbsen()+1);
        mahasiswa.setUser(user);
        if (importMahasiswaDto.getJenjang().equals("S1")){
            if (importMahasiswaDto.getProgram().equalsIgnoreCase("Reguler")) {
                mahasiswa.setIdProgram(programDao.findById("01").get());
            }
            if (importMahasiswaDto.getProgram().equalsIgnoreCase("Karyawan")){
                mahasiswa.setIdProgram(programDao.findById("3abf660c-e801-4ca3-b6d6-e69f67b9f09c").get());
            }
            if (importMahasiswaDto.getProgram().equalsIgnoreCase("Hafiz Nomist")){
                mahasiswa.setIdProgram(programDao.findById("haf_si_prog").get());
            }
        }

        if (importMahasiswaDto.getJenjang().equals("S2")){
            if (importMahasiswaDto.getProgram().equals("Reguler")){
                mahasiswa.setIdProgram(programDao.findById("8ec26f2c-a48a-4948-be90-e03e9374c675").get());
            }

            if (importMahasiswaDto.getProgram().equals("Eksekutif")){
                mahasiswa.setIdProgram(programDao.findById("03").get());
            }
        }
        mahasiswaDao.save(mahasiswa);

        return mahasiswa;
    }

    public BaseResponse importIpkSks(List<Mahasiswa> mahasiswaAktif){
        BaseResponse response = null;
        for (Mahasiswa mhs : mahasiswaAktif){

            List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mhs);
            listTranskript.removeIf(e -> e.getGrade().equals("E"));

            int sks = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();
            System.out.println("sks " + mhs.getNim() + " : " + sks);

            if (sks != 0) {
                BigDecimal mutu = listTranskript.stream().map(DataTranskript::getMutu)
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                BigDecimal ipk = mutu.divide(new BigDecimal(sks), 2, BigDecimal.ROUND_HALF_UP);

                Ipk cek = ipkDao.findByMahasiswa(mhs);
                if (cek == null) {
                    Ipk inputIpk = new Ipk();
                    inputIpk.setMahasiswa(mhs);
                    inputIpk.setIpk(ipk);
                    inputIpk.setSksTotal(sks);
                    ipkDao.save(inputIpk);
                }else{
                    cek.setMahasiswa(mhs);
                    cek.setIpk(ipk);
                    cek.setSksTotal(sks);
                    ipkDao.save(cek);
                }
            }
        }
        response = BaseResponse.builder()
                .code("200").message("Berhasil di import")
                .build();

        return response;
    }

    @Scheduled(cron = "0 00 01 * * *", zone = "Asia/Jakarta")
    public void importIpkSksS1(){
        Jenjang jenjang = jenjangDao.findById("01").get();
        List<Mahasiswa> mahasiswaAktif = mahasiswaDao.findByIdProdiIdJenjangAndStatusAndStatusAktifIn(jenjang, StatusRecord.AKTIF, Arrays.asList("AKTIF", "LULUS"));
        for (Mahasiswa mhs : mahasiswaAktif){

            if (mhs.getStatusAktif().equals("LULUS")){
                log.info("status mahasiswa {} lulus", mhs.getNim());
                if (Integer.parseInt(mhs.getAngkatan()) < 2015) {
                    log.info("Mahasiswa {} tidak generate ipk karna angkatan {}", mhs.getNim(), mhs.getAngkatan());
                }else{
                    List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mhs);
                    listTranskript.removeIf(e -> e.getGrade().equals("E"));

                    int sks = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();
                    log.info("sks {} : {}",mhs.getNim(), sks);

                    if (sks != 0) {
                        BigDecimal mutu = listTranskript.stream().map(DataTranskript::getMutu)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);

                        BigDecimal ipk = mutu.divide(new BigDecimal(sks), 2, BigDecimal.ROUND_HALF_UP);

                        Ipk cek = ipkDao.findByMahasiswa(mhs);
                        if (cek == null) {
                            Ipk inputIpk = new Ipk();
                            inputIpk.setMahasiswa(mhs);
                            inputIpk.setIpk(ipk);
                            inputIpk.setSksTotal(sks);
                            ipkDao.save(inputIpk);
                        }else{
                            cek.setMahasiswa(mhs);
                            cek.setIpk(ipk);
                            cek.setSksTotal(sks);
                            ipkDao.save(cek);
                        }
                    }
                }
            }
        }
    }

    @Scheduled(cron = "0 00 03 * * *", zone = "Asia/Jakarta")
    public void importIpkSksS2(){
        Jenjang jenjang = jenjangDao.findById("02").get();
        List<Mahasiswa> mahasiswaAktif = mahasiswaDao.findByIdProdiIdJenjangAndStatusAndStatusAktifIn(jenjang, StatusRecord.AKTIF, Arrays.asList("AKTIF", "LULUS"));
        for (Mahasiswa mhs : mahasiswaAktif){

            if (mhs.getStatusAktif().equals("LULUS")){
                log.info("status mahasiswa {} lulus", mhs.getNim());
                if (Integer.parseInt(mhs.getAngkatan()) < 10) {
                    log.info("Mahasiswa {} tidak generate ipk karna angkatan {}", mhs.getNim(), mhs.getAngkatan());
                }else{
                    List<DataTranskript> listTranskript = krsDetailDao.listTranskript(mhs);
                    listTranskript.removeIf(e -> e.getGrade().equals("E"));

                    int sks = listTranskript.stream().map(DataTranskript::getSks).mapToInt(Integer::intValue).sum();
                    log.info("sks {} : {}",mhs.getNim(), sks);

                    if (sks != 0) {
                        BigDecimal mutu = listTranskript.stream().map(DataTranskript::getMutu)
                                .reduce(BigDecimal.ZERO, BigDecimal::add);

                        BigDecimal ipk = mutu.divide(new BigDecimal(sks), 2, BigDecimal.ROUND_HALF_UP);

                        Ipk cek = ipkDao.findByMahasiswa(mhs);
                        if (cek == null) {
                            Ipk inputIpk = new Ipk();
                            inputIpk.setMahasiswa(mhs);
                            inputIpk.setIpk(ipk);
                            inputIpk.setSksTotal(sks);
                            ipkDao.save(inputIpk);
                        }else{
                            cek.setMahasiswa(mhs);
                            cek.setIpk(ipk);
                            cek.setSksTotal(sks);
                            ipkDao.save(cek);
                        }
                    }
                }
            }
        }
    }

}
