package id.ac.tazkia.smilemahasiswa.constant;

public enum JenisTest {
    TOEFL, IELTS, LISTENING, STRUCTURE, READING, WRITING, SPEAKING;
}
