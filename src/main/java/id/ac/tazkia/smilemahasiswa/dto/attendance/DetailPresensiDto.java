package id.ac.tazkia.smilemahasiswa.dto.attendance;

public interface DetailPresensiDto {
    String getPresensi();

    String getMasuk();

    String getKeluar();

    String getBerita();

}
