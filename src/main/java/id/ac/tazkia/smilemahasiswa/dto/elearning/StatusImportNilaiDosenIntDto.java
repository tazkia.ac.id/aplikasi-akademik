package id.ac.tazkia.smilemahasiswa.dto.elearning;

public interface StatusImportNilaiDosenIntDto {

    String getDosen();
    String getIdNumberElearning();
    String getNamaProdi();
    String getKodeMatakuliah();
    String getNamaKelas();
    String getNamaMatakuliah();
    String getNamaMatakuliahEnglish();
    String getStatusImportTugas();
    String getStatusImportUts();
    String getStatusImportUas();

}
