package id.ac.tazkia.smilemahasiswa.dto.api;


import lombok.Data;

@Data
public class JumlahDosenAktifDto {

    private String kodeProdi;
    private Integer lb;
    private Integer hb;
    private Integer total;

}
