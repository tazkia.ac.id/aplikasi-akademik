package id.ac.tazkia.smilemahasiswa.dto.transkript;

public interface TranskriptSementara {
    String getId();
    Integer getSemester();
    Integer getSks();
    String getKode();
    String getMatakuliah();
    String getGrade();
}
