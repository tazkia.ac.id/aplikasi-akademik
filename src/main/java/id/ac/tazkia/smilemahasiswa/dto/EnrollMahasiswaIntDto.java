package id.ac.tazkia.smilemahasiswa.dto;

public interface EnrollMahasiswaIntDto {

    String getIdNumberElearning();
    String getIdKrs();
    String getIdKrsDetail();
    String getIdJadwal();
    String getIdMahasiswa();
    String getEmail();
    String getNim();
    String getStatusEnroll();


}
