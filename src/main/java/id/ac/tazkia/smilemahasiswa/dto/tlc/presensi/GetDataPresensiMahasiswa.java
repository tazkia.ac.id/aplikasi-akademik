package id.ac.tazkia.smilemahasiswa.dto.tlc.presensi;

public interface GetDataPresensiMahasiswa {
    String getId();
    String getNim();
    String getNama();
    String getPresensi();
}
