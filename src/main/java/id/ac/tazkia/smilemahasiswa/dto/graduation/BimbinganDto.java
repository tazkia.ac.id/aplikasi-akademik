package id.ac.tazkia.smilemahasiswa.dto.graduation;

public interface BimbinganDto {
    String getId();
    String getNim();
    String getNama();
}
