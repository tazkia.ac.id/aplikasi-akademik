package id.ac.tazkia.smilemahasiswa.dto.krs;

public interface TotalSksDto {

    String getId();
    String getNim();
    String getNama();
    String getNamaProdi();
    String getTotalSks();
    String getStatusKrs();

    String getStatusAktif();

}
