package id.ac.tazkia.smilemahasiswa.dto.elearning;

import lombok.Data;

@Data
public class StatusImportNilaiDosenDto {

    private String dosen;
    private String idNumberElearning;
    private String namaProdi;
    private String kodeMatakuliah;
    private String namaKelas;
    private String namaMatakuliah;
    private String namaMatakuliahEnglish;
    private String statusImportTugas;
    private String statusImportUts;
    private String statusImportUas;

}
