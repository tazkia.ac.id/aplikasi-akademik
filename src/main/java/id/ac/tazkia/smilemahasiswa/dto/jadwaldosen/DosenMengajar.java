package id.ac.tazkia.smilemahasiswa.dto.jadwaldosen;

public interface DosenMengajar {
    String getId();
    String getDosen();
    String getGender();
}
