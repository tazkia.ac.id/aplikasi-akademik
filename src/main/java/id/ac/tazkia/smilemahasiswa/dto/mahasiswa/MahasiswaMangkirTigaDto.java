package id.ac.tazkia.smilemahasiswa.dto.mahasiswa;

public interface MahasiswaMangkirTigaDto {

    String getId();
    String getIdTahunElearning();
    String getIdMahasiswa();
    String getIdJadwal();
    String getEmail();
    String getIdNumberElearning();

    String getNim();
}
