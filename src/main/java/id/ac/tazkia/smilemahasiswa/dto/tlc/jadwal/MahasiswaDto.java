package id.ac.tazkia.smilemahasiswa.dto.tlc.jadwal;

public interface MahasiswaDto {
    String getId();
    String getNim();
    String getNama();
    String getProdi();
}
