package id.ac.tazkia.smilemahasiswa.constant;

public enum Approval {
    HAPUS, WAITING, APPROVED_AKADEMIK, APPROVED_KEMAHASISWAAN, APPROVED_TQC, REJECTED, APPROVED_PRODI, APPROVED, REJECTED_AKADEMIK, REJECTED_KEMAHASISWAAN, REJECTED_TQC, REJECTED_PRODI, UNFINISHED;
}
