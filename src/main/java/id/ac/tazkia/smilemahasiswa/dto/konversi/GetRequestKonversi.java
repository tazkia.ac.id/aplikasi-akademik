package id.ac.tazkia.smilemahasiswa.dto.konversi;

public interface GetRequestKonversi {

    String getId();

    String getNim();

    String getNama();

    String getJenis();

    String getProdi();

    String getStatus();

    String getApproval();

}
