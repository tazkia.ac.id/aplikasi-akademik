package id.ac.tazkia.smilemahasiswa.dto.api;

public interface MahasiswaAktifDto {

    String getKodeProdi();

    Integer getPria();

    Integer getWanita();

    Integer getTotal();

}
