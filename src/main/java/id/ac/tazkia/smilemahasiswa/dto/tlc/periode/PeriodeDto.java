package id.ac.tazkia.smilemahasiswa.dto.tlc.periode;

public interface PeriodeDto {
    String getId();
    String getTahun();
}
