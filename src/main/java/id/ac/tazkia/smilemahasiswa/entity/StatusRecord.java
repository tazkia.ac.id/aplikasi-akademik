package id.ac.tazkia.smilemahasiswa.entity;

public enum StatusRecord {
    AKTIF, PRAAKTIF, NONAKTIF, HAPUS, Y, N, TANGGUHKAN, DITAGIH_SELANJUTNYA, LUNAS, BELUM_LUNAS, GANJIL, GENAP, PENDEK, UTS, deleteyogi, DELETEYOGI, PRESTASI, TAHFIZH,
    UAS, KRS, SKRIPSI, SEMPRO, TESIS, WISUDA, STUDI_KELAYAKAN, DONE, UNDONE, PREVIEW, CUTI, CICILAN, MAGANG, METOLIT, PERINGANAN, ON_PROCESS, FINISHED, WAITING, KELUAR, MENGUNDURKAN_DIRI, D, REQEUST_KONVERSI,
    REJECTED, ERROR, TAHFIDZ, KOMPRE, LULUS, TIDAK_LULUS, BELUM_DINILAI, NOT_YET_SCHEDULED, SCHEDULED, ASSESMENT, KONVERSI, MBKM, JURNAL, CANCELED, PREDIKSI_TEST, KONVERSI_JURNAL, TOEFL, IELTS, APPROVED, QOLLOQIUM
}
