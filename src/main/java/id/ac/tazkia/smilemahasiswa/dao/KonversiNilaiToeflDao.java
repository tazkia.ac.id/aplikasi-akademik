package id.ac.tazkia.smilemahasiswa.dao;

import org.springframework.data.repository.PagingAndSortingRepository;

import id.ac.tazkia.smilemahasiswa.entity.KonversiNilaiToefl;

public interface KonversiNilaiToeflDao extends PagingAndSortingRepository<KonversiNilaiToefl, Integer> {


}