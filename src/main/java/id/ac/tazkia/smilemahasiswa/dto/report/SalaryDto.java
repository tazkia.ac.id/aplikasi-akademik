package id.ac.tazkia.smilemahasiswa.dto.report;

public interface SalaryDto {
    String getNama();

    String getEmail();

    String getMatkul();

    String getKelas();

    Integer getSks();

    String getOffline();

    String getOnline();

    String getAbstain();

    String getProdi();

    String getSdosen();
}
