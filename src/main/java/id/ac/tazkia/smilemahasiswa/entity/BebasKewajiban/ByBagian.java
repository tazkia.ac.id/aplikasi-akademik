package id.ac.tazkia.smilemahasiswa.entity.BebasKewajiban;

public enum ByBagian {
    PERPUSTAKAAN, KABAG_PERPUS, KEUANGAN, TLC, TQC, AKADEMIK, DIKAMPUS, DILUAR, HAFIDZPRENEUR
}
