package id.ac.tazkia.smilemahasiswa.dto.elearning;

public interface KlasemenDosenIntDto {

    String getDosen();
    String getNamaProdi();
    String getSesiTepatWaktu();
    String getSemuaSesi();
    String getPersentase();


}
