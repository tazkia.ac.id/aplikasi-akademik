package id.ac.tazkia.smilemahasiswa.dto.api;

import java.math.BigDecimal;

public interface PerformanceMahasiswaDto {

    String getBulan();
    BigDecimal getRate();
}
