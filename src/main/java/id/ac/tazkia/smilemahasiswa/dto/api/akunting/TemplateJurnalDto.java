package id.ac.tazkia.smilemahasiswa.dto.api.akunting;

import lombok.Data;

@Data
public class TemplateJurnalDto {

    private String id;
    private String code;
    private String name;
    private String category;

}

