package id.ac.tazkia.smilemahasiswa.dto.api;

public interface DosenAktifSemesterDto {

    String getKodeProdi();
    Integer getLb();
    Integer getHb();
    Integer getTotal();

}
