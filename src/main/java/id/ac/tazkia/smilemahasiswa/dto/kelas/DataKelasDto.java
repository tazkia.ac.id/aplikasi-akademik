package id.ac.tazkia.smilemahasiswa.dto.kelas;

public interface DataKelasDto {
    String getId();
    String getIdProdi();
    String getNama();
    String getProdi();
    String getKeterangan();
    String getAngkatan();
    Integer getMahasiswa();
}
