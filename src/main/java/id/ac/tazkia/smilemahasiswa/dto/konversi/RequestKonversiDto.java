package id.ac.tazkia.smilemahasiswa.dto.konversi;

public interface RequestKonversiDto {

    String getId();

    String getIdKrsDetail();

    String getIdProdi();

    String getIdJadwal();

    String getNamaProdi();

    String getIdMahasiswa();

    String getNim();

    String getNama();

    String getNamaMatkul();

    String getGradeLama();

    String getJenisNilai();

    String getStatus();

    String getApproval();

    String getFinalization();

}
