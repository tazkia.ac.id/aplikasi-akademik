package id.ac.tazkia.smilemahasiswa.dto.krs;

public interface MatkulMengulangDto {

    String getIdKrs();

    String getIdMahasiswa();

    String getNamaMatakuliah();

    String getNilaiAkhir();

    String getBobot();

    String getGrade();

}
