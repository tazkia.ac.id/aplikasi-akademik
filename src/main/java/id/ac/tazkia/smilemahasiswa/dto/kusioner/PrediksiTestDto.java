package id.ac.tazkia.smilemahasiswa.dto.kusioner;

public interface PrediksiTestDto {
    String getId();
    String getNim();
    String getNama();
    String getProdi();
    Integer getUjian();
    String getStatus();
    String getFile();
    String getTanggal();
}
