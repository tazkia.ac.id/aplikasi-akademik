package id.ac.tazkia.smilemahasiswa.dto.tlc.jadwal;

public interface JadwalRequest {
    String getId();

    String getNama();

    String getKelas();

    String getJenis();
}
