package id.ac.tazkia.smilemahasiswa.dto.ujian;

public interface Tahfidz {
    String getId();
    String getNim();
    String getNama();
    String getUjian();
    String getTanggal();
    String getStatus();
    String getStatusUjian();

}
