package id.ac.tazkia.smilemahasiswa.dto.ujian;

public interface ListTahfidz {
    String getId();
    String getNim();
    String getNama();
    String getUjian();
    String getTanggal();
    String getStatus();
    String getSurat();
    String getUjianKe();

    String getStatusUjian();
}
