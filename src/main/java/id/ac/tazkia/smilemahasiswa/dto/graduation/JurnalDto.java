package id.ac.tazkia.smilemahasiswa.dto.graduation;

import lombok.Data;

@Data
public class JurnalDto {
    private String id;
    private String jurnalId;
    private String gradeId;
}
