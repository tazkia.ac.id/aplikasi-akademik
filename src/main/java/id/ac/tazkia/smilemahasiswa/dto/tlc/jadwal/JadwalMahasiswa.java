package id.ac.tazkia.smilemahasiswa.dto.tlc.jadwal;

public interface JadwalMahasiswa {
    String getJadwal();

    String getId();

    String getNim();
    
    String getNama();
}
